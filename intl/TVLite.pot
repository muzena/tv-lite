#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: TV-Lite 0.1.0\n"
"POT-Creation-Date: 2022-06-01 19:28+0200\n"
"PO-Revision-Date: 2020-06-06 15:34+0300\n"
"Language-Team: Cristian Burneci\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"X-Poedit-Basepath: ../src\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n==0 || (n!=1 && n%100>=1 && n%100<=19) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"Last-Translator: \n"
"Language: ro\n"
"X-Poedit-SearchPath-0: .\n"

#: TVLite.desktop:5 gui-windows.h:182 gui.h:179
msgid "TV-Lite"
msgstr ""

#: TVLite.desktop:8
msgid "TV online viewer"
msgstr ""

#: TVLite.desktop:10
msgid "TVLite"
msgstr ""

#: aceprotocol.cpp:47
msgid "Could not find the Acestream engine binary in the path."
msgstr ""

#: aceprotocol.cpp:47 aceprotocol.cpp:87 acestreamprotocolhandler.cpp:93
#: acestreamprotocolhandler.cpp:191 acestreamprotocolhandler.cpp:233
#: dlprogress.cpp:152 downloadclient.cpp:52 main.cpp:389 main.cpp:396
#: main.cpp:412 main.cpp:502 main.cpp:1929 main.cpp:2076 sopprotocol.cpp:86
#: sopprotocol.cpp:92 sopprotocol.cpp:108 subscription.cpp:284
#: subscription.cpp:292 subscription.cpp:353 subscriptionlistdialog.cpp:47
#: subscriptionlistdialog.cpp:54 subscriptionlistdialog.cpp:97
#: subscriptionlistdialog.cpp:158 subscriptionlistdialog.cpp:248
#: subscriptionlistdialog.cpp:255
msgid "Error"
msgstr ""

#: aceprotocol.cpp:87
msgid "Could not execute Acestream engine!"
msgstr ""

#: acestreamprotocolhandler.cpp:88
msgid "Requesting data from Acestream"
msgstr ""

#: acestreamprotocolhandler.cpp:93 acestreamprotocolhandler.cpp:233
msgid "Error retrieving data from Acestream engine"
msgstr ""

#: acestreamprotocolhandler.cpp:94 main.cpp:244 main.cpp:553
msgid "Ready"
msgstr ""

#: acestreamprotocolhandler.cpp:191
msgid "Error response from Acestream engine\n"
msgstr ""

#: channel.cpp:314
msgid "Unknown protocol"
msgstr ""

#: channel.cpp:322
msgid "Sopcast address"
msgstr ""

#: channel.cpp:330
msgid "Web address"
msgstr ""

#: channel.cpp:334
msgid "File "
msgstr ""

#: channel.cpp:338
msgid "Acestream id"
msgstr ""

#: channel.cpp:342
msgid "Unknown address"
msgstr ""

#: channeledit.cpp:12
msgid "Addresses"
msgstr ""

#: channeledit.cpp:13 gui-windows.cpp:629 gui-windows.cpp:689 gui.cpp:649
#: gui.cpp:709 subscriptionlistdialog.cpp:15
msgid "Name"
msgstr ""

#: channeledit.cpp:72 localdialog.cpp:35
msgid "Please input a valid name"
msgstr ""

#: channeledit.cpp:72 channeledit.cpp:98 channeledit.cpp:102 localdialog.cpp:35
#: prefdialog.cpp:109 prefdialog.cpp:116 prefdialog.cpp:135 prefdialog.cpp:143
#: prefdialog.cpp:151 prefdialog.cpp:161 vlcoptdlg.cpp:59
msgid "Validation error"
msgstr ""

#: channeledit.cpp:82 channeledit.cpp:118
msgid "Click here to input the address..."
msgstr ""

#: channeledit.cpp:98
msgid "Empty address detected "
msgstr ""

#: channeledit.cpp:102
msgid "Please, enter at least an address"
msgstr ""

#: channeledit.cpp:128
msgid "Do you really wish to delete the selected address?"
msgstr ""

#: channeledit.cpp:129 locallistdialog.cpp:92 main.cpp:1716
#: subscriptionlistdialog.cpp:185 vlcoptdlg.cpp:79
msgid "Question"
msgstr ""

#: channelinfo.cpp:24
msgid "Channel name: "
msgstr ""

#: channelinfo.cpp:25
msgid "Channel group: "
msgstr ""

#: dlprogress.cpp:152
msgid "Could not update subscription "
msgstr ""

#: downloadclient.cpp:52
msgid "Error downloading channel list"
msgstr ""

#: gui-windows.cpp:26 gui.cpp:21
msgid "Manage &Lists..."
msgstr ""

#: gui-windows.cpp:28 gui.cpp:23
msgid "Add Local List"
msgstr ""

#: gui-windows.cpp:34 gui.cpp:29
msgid "Save Whole Channel List As..."
msgstr ""

#: gui-windows.cpp:38 gui.cpp:33
msgid "Save Current View As..."
msgstr ""

#: gui-windows.cpp:44 gui.cpp:39
msgid "Save Subscription As Local List"
msgstr ""

#: gui-windows.cpp:49 gui.cpp:44
msgid "Manage Local Lists"
msgstr ""

#: gui-windows.cpp:52 gui.cpp:47
msgid "Manage Subscriptions"
msgstr ""

#: gui-windows.cpp:58 gui.cpp:53
msgid "Update All Subscriptions"
msgstr ""

#: gui-windows.cpp:66 gui.cpp:61
msgid "Preferences"
msgstr ""

#: gui-windows.cpp:72 gui.cpp:67
msgid "E&xit"
msgstr ""

#: gui-windows.cpp:75 gui.cpp:70
msgid "&TVLite"
msgstr ""

#: gui-windows.cpp:79 gui.cpp:74
msgid "Full Screem"
msgstr ""

#: gui-windows.cpp:82 gui.cpp:77
msgid "Channel List"
msgstr ""

#: gui-windows.cpp:86 gui.cpp:81
msgid "Mini Window"
msgstr ""

#: gui-windows.cpp:93 gui.cpp:88
msgid "Aspect Ratio"
msgstr ""

#: gui-windows.cpp:95 gui.cpp:90
msgid "Default"
msgstr ""

#: gui-windows.cpp:99 gui.cpp:94
msgid "16:9"
msgstr ""

#: gui-windows.cpp:103 gui.cpp:98
msgid "4:3"
msgstr ""

#: gui-windows.cpp:111 gui.cpp:106
msgid "Video Tracks"
msgstr ""

#: gui-windows.cpp:115 gui.cpp:110
msgid "Audio Tracks"
msgstr ""

#: gui-windows.cpp:119 gui.cpp:114
msgid "Subtitle Tracks"
msgstr ""

#: gui-windows.cpp:122 gui.cpp:117
msgid "&View"
msgstr ""

#: gui-windows.cpp:126 gui.cpp:121
msgid "About"
msgstr ""

#: gui-windows.cpp:129 gui.cpp:124
msgid "Help"
msgstr ""

#: gui-windows.cpp:141 gui.cpp:134
msgid "List Types"
msgstr ""

#: gui-windows.cpp:148 gui-windows.h:222 gui.cpp:141 gui.h:258
msgid "Local Lists"
msgstr ""

#: gui-windows.cpp:151 gui-windows.h:256 gui.cpp:144 gui.h:292
msgid "Subscriptions"
msgstr ""

#: gui-windows.cpp:155 gui.cpp:148
msgid "Favorites"
msgstr ""

#: gui-windows.cpp:161 gui.cpp:154
msgid "Channel List:"
msgstr ""

#: gui-windows.cpp:184 gui-windows.cpp:213 gui.cpp:177 gui.cpp:206
msgid "A-Z"
msgstr ""

#: gui-windows.cpp:187 gui-windows.cpp:216 gui.cpp:180 gui.cpp:209
msgid "Z-A"
msgstr ""

#: gui-windows.cpp:236 gui.cpp:229
msgid "Total:"
msgstr ""

#: gui-windows.cpp:505 gui-windows.cpp:561 gui.cpp:525
msgid "New"
msgstr ""

#: gui-windows.cpp:508 gui-windows.cpp:573 gui.cpp:528 gui.cpp:593
msgid "Edit"
msgstr ""

#: gui-windows.cpp:511 gui-windows.cpp:576 gui.cpp:531 gui.cpp:596
msgid "Delete"
msgstr ""

#: gui-windows.cpp:520 gui-windows.cpp:591 gui.cpp:540 gui.cpp:611
msgid "Close"
msgstr ""

#: gui-windows.cpp:564 gui.cpp:584
msgid "New Xtream"
msgstr ""

#: gui-windows.cpp:570 gui.cpp:590 subscriptionlistdialog.cpp:134
#: subscriptionlistdialog.cpp:164 subscriptionlistdialog.cpp:261
msgid "Update"
msgstr ""

#: gui-windows.cpp:582 gui.cpp:602
msgid "Update All"
msgstr ""

#: gui-windows.cpp:636 gui-windows.cpp:703 gui.cpp:656 gui.cpp:723
#: subscriptionlistdialog.cpp:16
msgid "URL"
msgstr ""

#: gui-windows.cpp:646 gui.cpp:666
msgid " ... "
msgstr ""

#: gui-windows.cpp:696 gui.cpp:716
msgid "Author"
msgstr ""

#: gui-windows.cpp:710 gui.cpp:730
msgid "Version"
msgstr ""

#: gui-windows.cpp:751 gui-windows.cpp:832 gui.cpp:483 gui.cpp:993
msgid "None"
msgstr ""

#: gui-windows.cpp:751 gui.cpp:483
msgid "Record"
msgstr ""

#: gui-windows.cpp:751 gui.cpp:483
msgid "Stream"
msgstr ""

#: gui-windows.cpp:753 gui.cpp:485
msgid "Recording Options"
msgstr ""

#: gui-windows.cpp:789 gui.cpp:950
msgid "Streaming port"
msgstr ""

#: gui-windows.cpp:799 gui.cpp:960
msgid "Recording path"
msgstr ""

#: gui-windows.cpp:809 gui-windows.cpp:870 gui.cpp:970 gui.cpp:1031
msgid "..."
msgstr ""

#: gui-windows.cpp:815 gui.cpp:976
msgid "Enable  song change notifications"
msgstr ""

#: gui-windows.cpp:819 gui.cpp:980
msgid "Update all subscriptions at program start"
msgstr ""

#: gui-windows.cpp:827 gui.cpp:988
msgid "General"
msgstr ""

#: gui-windows.cpp:832 gui.cpp:993
msgid "Any"
msgstr ""

#: gui-windows.cpp:832 gui.cpp:993
msgid "Custom"
msgstr ""

#: gui-windows.cpp:834 gui.cpp:995
msgid "Hardware acceleration type"
msgstr ""

#: gui-windows.cpp:841 gui.cpp:1002
msgid "Custom HW acceleration string"
msgstr ""

#: gui-windows.cpp:855 gui.cpp:1016
msgid "VLC"
msgstr ""

#: gui-windows.cpp:860 gui.cpp:1021
msgid "Acestream executable path"
msgstr ""

#: gui-windows.cpp:879 gui.cpp:1046
msgid "Maximum disk cache size"
msgstr ""

#: gui-windows.cpp:886 gui.cpp:1053
msgid "MB"
msgstr ""

#: gui-windows.cpp:893 gui.cpp:1060
msgid "The settings on this page will only be effective after program restart!"
msgstr ""

#: gui-windows.cpp:901 gui.cpp:1068
msgid "Acestream"
msgstr ""

#: gui-windows.cpp:906 gui.cpp:1073
msgid "Use Proxy"
msgstr ""

#: gui-windows.cpp:914 gui.cpp:1081
msgid "Type"
msgstr ""

#: gui-windows.cpp:918 gui.cpp:1085
msgid "Address"
msgstr ""

#: gui-windows.cpp:922 gui.cpp:1089
msgid "Port"
msgstr ""

#: gui-windows.cpp:926 gui.cpp:1093
msgid "HTTP"
msgstr ""

#: gui-windows.cpp:926 gui.cpp:1093
msgid "SOCKS"
msgstr ""

#: gui-windows.cpp:941 gui.cpp:1108
msgid "Use Authentication"
msgstr ""

#: gui-windows.cpp:949 gui-windows.cpp:1335 gui.cpp:1116 gui.cpp:1198
msgid "Username"
msgstr ""

#: gui-windows.cpp:956 gui-windows.cpp:1342 gui.cpp:1123 gui.cpp:1205
msgid "Password"
msgstr ""

#: gui-windows.cpp:970
msgid "Proxy"
msgstr ""

#: gui-windows.cpp:1076 gui.cpp:771 main.cpp:202
msgid "Channel Name"
msgstr ""

#: gui-windows.cpp:1083 gui.cpp:778
msgid "Group"
msgstr ""

#: gui-windows.cpp:1090 gui.cpp:785
msgid "Channel Addresses"
msgstr ""

#: gui-windows.cpp:1321 gui.cpp:1184
msgid "Subscription Name"
msgstr ""

#: gui-windows.cpp:1328 gui.cpp:1191
msgid "Host and port (ex. http://host.xyz:port)"
msgstr ""

#: gui-windows.cpp:1349 gui.cpp:1212
msgid "m3u_plus"
msgstr ""

#: gui-windows.cpp:1349 gui.cpp:1212
msgid "m3u"
msgstr ""

#: gui-windows.cpp:1351 gui.cpp:1214
msgid "Playlist Type"
msgstr ""

#: gui-windows.cpp:1355 gui.cpp:1218
msgid "mpeg-ts"
msgstr ""

#: gui-windows.cpp:1355 gui.cpp:1218
msgid "hls"
msgstr ""

#: gui-windows.cpp:1355 gui.cpp:1218
msgid "rtmp"
msgstr ""

#: gui-windows.cpp:1357 gui.cpp:1220
msgid "Output Type"
msgstr ""

#: gui-windows.h:287 gui.h:323
msgid "Path"
msgstr ""

#: gui-windows.h:319 gui.h:355
msgid "Edit Local List"
msgstr ""

#: gui-windows.h:404 gui.h:490
msgid "TV-Lite - Preferences"
msgstr ""

#: gui-windows.h:428 gui.h:544
msgid "Channel Information"
msgstr ""

#: gui-windows.h:492 gui.h:397
msgid "Channel Editor"
msgstr ""

#: gui-windows.h:521 gui.h:573
msgid "VLC options"
msgstr ""

#: gui-windows.h:549 gui.h:426
msgid "Download progress"
msgstr ""

#: gui-windows.h:579 gui.h:520
msgid "Xtream credentials"
msgstr ""

#: gui.cpp:581
msgid "New URL"
msgstr ""

#: gui.cpp:1037
msgid "Disk"
msgstr ""

#: gui.cpp:1037
msgid "Memory"
msgstr ""

#: gui.cpp:1039
msgid "Cache Type"
msgstr ""

#: gui.cpp:1137
msgid "Network"
msgstr ""

#: locallist.cpp:29
msgid "Loading local list"
msgstr ""

#: locallistdialog.cpp:91
msgid "Do you really wish to delete the selected list, "
msgstr ""

#: main.cpp:117
msgid "Another instance of TV-Lite is running. Aborting."
msgstr ""

#: main.cpp:135
msgid "Could not initialize program configuration"
msgstr ""

#: main.cpp:203
msgid "Groups"
msgstr ""

#: main.cpp:389
msgid "No Item selected"
msgstr ""

#: main.cpp:396 main.cpp:412
msgid "This item has no media sources"
msgstr ""

#: main.cpp:502
msgid "Acestream engine not found"
msgstr ""

#: main.cpp:987
msgid "Total: "
msgstr ""

#: main.cpp:987
msgid " channels and "
msgstr ""

#: main.cpp:987
msgid " sources."
msgstr ""

#: main.cpp:1027
msgid "Finding groups..."
msgstr ""

#: main.cpp:1166
msgid "<All Channels>"
msgstr ""

#: main.cpp:1215
msgid "Update channelList"
msgstr ""

#: main.cpp:1401 main.cpp:1448 main.cpp:1473 main.cpp:1499
msgid "New Item"
msgstr ""

#: main.cpp:1500
msgid "Edit Item"
msgstr ""

#: main.cpp:1501
msgid "Delete Item"
msgstr ""

#: main.cpp:1506
msgid "Information"
msgstr ""

#: main.cpp:1512
msgid "Add to favorites"
msgstr ""

#: main.cpp:1715
msgid "Do you really wish to delete the selected channel?"
msgstr ""

#: main.cpp:1857
msgid "The list was saved."
msgstr ""

#: main.cpp:1918
msgid "Save File As _?"
msgstr ""

#: main.cpp:1919
msgid "TV-Maxe lists (*.db)|*.db|All files (*)|*"
msgstr ""

#: main.cpp:1929
msgid "Error while trying to write file: "
msgstr ""

#: main.cpp:1934
msgid "The list has been saved."
msgstr ""

#: main.cpp:1934
msgid "Save list"
msgstr ""

#: main.cpp:1946
msgid ""
"Viewer for online TV Channels - TV-maxe like\n"
"Version "
msgstr ""

#: main.cpp:1989
msgid "Buffering"
msgstr ""

#: main.cpp:2027
msgid "Playing"
msgstr ""

#: main.cpp:2049
msgid "Paused"
msgstr ""

#: main.cpp:2052
msgid "Stopped"
msgstr ""

#: main.cpp:2076
msgid "The player encountered an error while trying to play URL"
msgstr ""

#: prefdialog.cpp:109
msgid "Please input a valid recording path"
msgstr ""

#: prefdialog.cpp:116
msgid "Please provide HW acceleration type"
msgstr ""

#: prefdialog.cpp:135 prefdialog.cpp:143
msgid "Please input a valid proxy port value"
msgstr ""

#: prefdialog.cpp:151
msgid "Please input a proxy address"
msgstr ""

#: prefdialog.cpp:161
msgid "Please input a proxy username"
msgstr ""

#: prefdialog.cpp:181
msgid "Choose Recording Folder"
msgstr ""

#: prefdialog.cpp:210
msgid "Choose Acestream file"
msgstr ""

#: sopprotocol.cpp:86
msgid ""
"Could not search for the Sopcast binary.\n"
" Check your installation!"
msgstr ""

#: sopprotocol.cpp:92
msgid "Could not find the Sopcast binary in the path."
msgstr ""

#: sopprotocol.cpp:108
msgid "Could not execute Sopcast!"
msgstr ""

#: sopprotocolhandler.cpp:40
msgid "Starting Sopcast"
msgstr ""

#: subscription.cpp:233
msgid "Parsing playlist"
msgstr ""

#: subscription.cpp:284
msgid "Malformed M3U list!"
msgstr ""

#: subscription.cpp:292
msgid "Format not recognized"
msgstr ""

#: subscription.cpp:353
msgid "Could not open playlist"
msgstr ""

#: subscription.cpp:554
msgid "Saving..."
msgstr ""

#: subscriptiondialog.cpp:70
msgid "Open XYZ file"
msgstr ""

#: subscriptionlistdialog.cpp:47 subscriptionlistdialog.cpp:248
msgid "Found another subscription with the same URL"
msgstr ""

#: subscriptionlistdialog.cpp:54 subscriptionlistdialog.cpp:255
msgid "Could not set up the selected subscription"
msgstr ""

#: subscriptionlistdialog.cpp:60 subscriptionlistdialog.cpp:261
msgid "Subscription set up"
msgstr ""

#: subscriptionlistdialog.cpp:60
msgid "New subscription"
msgstr ""

#: subscriptionlistdialog.cpp:97
msgid "Could not modify the selected subscription"
msgstr ""

#: subscriptionlistdialog.cpp:134
msgid "Subscription modified"
msgstr ""

#: subscriptionlistdialog.cpp:158
msgid "Could not update the selected subscription"
msgstr ""

#: subscriptionlistdialog.cpp:164
msgid "Subscription Updated"
msgstr ""

#: subscriptionlistdialog.cpp:184
msgid "Do you really wish to delete the selected subscription, "
msgstr ""

#: vlcoptdlg.cpp:9
msgid "Option"
msgstr ""

#: vlcoptdlg.cpp:23 vlcoptdlg.cpp:48 vlcoptdlg.cpp:68
msgid "Click here to add option..."
msgstr ""

#: vlcoptdlg.cpp:59
msgid "Invalid option (blank)"
msgstr ""

#: vlcoptdlg.cpp:78
msgid "Do you really wish to delete the selected option?"
msgstr ""

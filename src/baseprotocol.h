#ifndef CBASEPROTOCOL_H
#define CBASEPROTOCOL_H

#include <wx/string.h>
#include <wx/process.h>

namespace tvlite
{

class CBaseProtocol: public wxProcess
{
private:
   CBaseProtocol(const CBaseProtocol& rhs);
   CBaseProtocol& operator=(const CBaseProtocol& rhs);
   CBaseProtocol();

public:
   CBaseProtocol(wxEvtHandler *parent, wxString url, wxString cmd);
   virtual ~CBaseProtocol();

   wxString GetURL();
   void SetURL(wxString url);

   wxString GetName();
   void SetName(wxString name);

   wxString GetCmd();
   void SetCmd(wxString cmd);

   virtual int  StartProtocol(long &pid) = 0;
   virtual void StopProtocol();

   virtual int LoadConfig() = 0;
   virtual int SaveConfig() = 0;

   virtual void OnTerminate(int pid, int status);

protected:
   wxEvtHandler *m_pparent;
   wxString m_url;
   wxString m_name;
   wxString m_cmd;


};

} //namespace

#endif // CBASEPROTOCOL_H

#include <wx/debug.h>
#include "busyobject.h"
using namespace tvlite;

CBusyObject::CBusyObject()
{
   m_busy = false;
}

CBusyObject::~CBusyObject()
{
}

void CBusyObject::SetBusy()
{
   m_busy = true;
}


void CBusyObject::ClearBusy()
{
   m_busy = false;
}

bool CBusyObject::IsBusy()
{
   return m_busy;
}


CBusyGuard::CBusyGuard(CBusyObject* parent):m_parentobj(parent), m_isInitialized(false)
{
   
   wxASSERT(m_parentobj!=NULL);
   if (m_parentobj && !(m_parentobj->IsBusy()))
   {
      m_isInitialized = true;
      m_parentobj->SetBusy();
   }
}

CBusyGuard::~CBusyGuard()
{
   if (m_isInitialized)
   {
      m_parentobj->ClearBusy();
   }   
}

bool CBusyGuard::Allows()
{
   return m_isInitialized;
}
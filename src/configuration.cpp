#include "configuration.h"
#include <cstdlib> // NULL
#include <wx/string.h>
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include "debug.h"

#define TEMPDIR_NAME  "temp"
#define CACHEDIR_NAME "cache"
#define CFGFILE_NAME  "config"
#define LISTDIR_NAME  "lists"
#define FAVORITES_DIR_NAME "favorites"
#define FAVORITES_FILE_NAME "common.db"
#define LAST_PLAYED_DIR_NAME ""
#define LAST_PLAYED_FILE_NAME "lastplayed.db"

using namespace tvlite;

tvlite::CConfiguration* tvlite::CConfiguration::ms_instance = NULL;

tvlite::CConfiguration::CConfiguration()
{
}

tvlite::CConfiguration::~CConfiguration()
{
}

bool tvlite::CConfiguration::Init()
{
   bool rc = Init(GetConfigDir()) &&
             Init(GetTempDir()) &&
             Init(GetCacheDir()) &&
             Init(GetFavoritesDir());
   if (rc)
   {
      DBG_INFO("Creating GUI config");
      m_pconfig = new wxFileConfig(wxEmptyString, wxEmptyString, GetConfigFileName(), wxEmptyString, wxCONFIG_USE_LOCAL_FILE);
      if (m_pconfig)
      {
         wxConfigBase::Set((wxConfigBase *)m_pconfig);
      }
      else
      {
         DBG_ERROR("Could not create GUI config");
         rc = false;
      }
   }
   return rc;
}

bool tvlite::CConfiguration::Init(wxString dir)
{
   DBG_INFO("Checking %s", (const char*)dir.utf8_str());
   wxFileName basedir(dir,"");
   if (!basedir.DirExists())
   {
      if (!wxMkdir(dir))
      {
         DBG_ERROR("Could not make %s directory", (const char*)dir.utf8_str());
         return false;
      }
   }
   return true;
}

tvlite::CConfiguration* tvlite::CConfiguration::Instance()
{
   if (ms_instance == NULL) {
      ms_instance = new tvlite::CConfiguration();
   }
   return ms_instance;
}

void tvlite::CConfiguration::Release()
{
   if (ms_instance) {
      delete ms_instance;
   }
   ms_instance = NULL;
}

wxString tvlite::CConfiguration::GetConfigDir()
{
   wxStandardPaths stdPath = wxStandardPaths::Get();
   wxString pathtmp = stdPath.GetUserDataDir();
   return pathtmp;

}

wxString tvlite::CConfiguration::GetConfigFileName()
{
   wxFileName configFile(GetConfigDir(), "config");
   DBG_INFO("Config file is: %s", (const char*)configFile.GetFullPath().utf8_str());
   return configFile.GetFullPath();
}

wxString tvlite::CConfiguration::GetTempDir()
{
   wxFileName tempDir(GetConfigDir(), "");
   tempDir.AppendDir(TEMPDIR_NAME);
   return tempDir.GetFullPath();

}

wxString tvlite::CConfiguration::GetCacheDir()
{
   wxFileName cacheDir(GetConfigDir(), "");
   cacheDir.AppendDir(CACHEDIR_NAME);
   return cacheDir.GetFullPath();
}

wxString tvlite::CConfiguration::GetListDir()
{
   wxFileName tempDir(GetConfigDir(), "");
   tempDir.AppendDir(LISTDIR_NAME);
   return tempDir.GetFullPath();

}

wxString tvlite::CConfiguration::GetFavoritesDir()
{
   wxFileName tempDir(GetConfigDir(), "");
   tempDir.AppendDir(FAVORITES_DIR_NAME);
   return tempDir.GetFullPath();

}

wxString tvlite::CConfiguration::GetLastPlayedDir()
{
   wxFileName tempDir(GetConfigDir(), "");
   //tempDir.AppendDir(LAST_PLAYED_DIR_NAME);
   return tempDir.GetFullPath();

}

wxString tvlite::CConfiguration::GetFavoritesFileName()
{
   return FAVORITES_FILE_NAME;
}

wxString tvlite::CConfiguration::GetLastPlayedFileName()
{
   return LAST_PLAYED_FILE_NAME;
}

wxFileConfig *tvlite::CConfiguration::GetProgramConfig()
{
   return m_pconfig;
}

void tvlite::CConfiguration::Load()
{
   GetGeneralConfiguration().Load();
   GetVLCConfiguration().Load();
   GetAceConfiguration().Load();
   GetNetworkConfiguration().Load();
}

void tvlite::CConfiguration::Save()
{
   GetGeneralConfiguration().Save();
   GetVLCConfiguration().Save();
   GetAceConfiguration().Save();
   GetNetworkConfiguration().Save();
   bool rc = TVLITECONFIG->Flush();
   DBG_INFO ("flush config result = %d", rc);
}

////////////////////////////////////////////////////////////////////////////////////////////
//  Configuration helper classes
///////////////////////////////////////////////////////////////////////////////////////////

TGeneralConfiguration& tvlite::CConfiguration::GetGeneralConfiguration()
{
   return m_genConfig;
}

TVLCConfiguration& tvlite::CConfiguration::GetVLCConfiguration()
{
   return m_vlcConfig;
}

TAceConfiguration& tvlite::CConfiguration::GetAceConfiguration()
{
   return m_aceConfig;
}

TNetworkConfiguration& tvlite::CConfiguration::GetNetworkConfiguration()
{
   return m_networkConfig;
}

// General Configuration

void TGeneralConfiguration::Load()
{
   long port;
   wxString defaultRecDir = GetDefaultRecordDir();
   wxString recDir;
   bool notif;
   bool updateall;
   bool playlast;
   if (!(TVLITECONFIG->Read(GC_STREAM_PORT_KEY, &port, 50100)))
   {
      DBG_INFO("Streaming port: Used default value");
   }
   if (!(TVLITECONFIG->Read(GC_REC_DIR_KEY, &recDir, defaultRecDir)))
   {
      DBG_INFO("Record dir: Used default value");
   }
   if (!(TVLITECONFIG->Read(GC_NOTIF_KEY, &notif, true)))
   {
      DBG_INFO("Notifications: Used default value");
   }
   if (!(TVLITECONFIG->Read(GC_UPDATEALL_KEY, &updateall, true)))
   {
      DBG_INFO("UpdateAllAtStart: Used default value");
   }
   if (!(TVLITECONFIG->Read(GC_PLAY_LAST_KEY, &playlast, true)))
   {
      DBG_INFO("UpdateAllAtStart: Used default value");
   }
   m_recordDir = recDir;
   m_streamPort = (unsigned int)port;
   m_notif = notif;
   m_updateall = updateall;
   m_playlast = playlast;
}

void TGeneralConfiguration::Save()
{
   if (!(TVLITECONFIG->Write(GC_STREAM_PORT_KEY, m_streamPort)))
   {
      DBG_ERROR("Could not save streaming port");
   }
   if (!(TVLITECONFIG->Write(GC_REC_DIR_KEY,    m_recordDir)))
   {
      DBG_ERROR("Could not save record dir");
   }
   if (!(TVLITECONFIG->Write(GC_NOTIF_KEY,    m_notif)))
   {
      DBG_ERROR("Could not save notification state");
   }
   if (!(TVLITECONFIG->Write(GC_UPDATEALL_KEY,    m_updateall)))
   {
      DBG_ERROR("Could not save updateall state");
   }
   if (!(TVLITECONFIG->Write(GC_PLAY_LAST_KEY,    m_playlast)))
   {
      DBG_ERROR("Could not save updateall state");
   }
}

void TGeneralConfiguration::SetRecordDir(wxString path)
{
   m_recordDir = path;
}

wxString TGeneralConfiguration::GetRecordDir()
{
   return m_recordDir;
}

void TGeneralConfiguration::SetStreamPort(unsigned int port)
{
   m_streamPort = port;
}

unsigned int TGeneralConfiguration::GetStreamPort()
{
   return m_streamPort;
}

wxString TGeneralConfiguration::GetDefaultRecordDir()
{
   wxStandardPaths stdPath = wxStandardPaths::Get();
   return stdPath.GetDocumentsDir();
}

void TGeneralConfiguration::SetNotifEnabled(bool enabled)
{
   m_notif = enabled;
}

bool  TGeneralConfiguration::GetNotifEnabled()
{
   return m_notif;
}

void TGeneralConfiguration::SetUpdateAllEnabled(bool enabled)
{
   m_updateall = enabled;
}

bool TGeneralConfiguration::GetUpdateAllEnabled()
{
   return m_updateall;
}


void TGeneralConfiguration::SetPlayLastStreamEnabled(bool enabled)
{
   m_playlast = enabled;
}

bool TGeneralConfiguration::GetPlayLastStreamEnabled()
{
   return m_playlast;
}

//VLC configuration

void TVLCConfiguration::Load()
{
   wxString key;
   if (!(TVLITECONFIG->Read(VC_RENDER_KEY, &key, "any")))
   {
      DBG_INFO("Streaming port: Used default value");
   }
   m_render = key;
}

void TVLCConfiguration::Save()
{
   if (!(TVLITECONFIG->Write(VC_RENDER_KEY, m_render)))
   {
      DBG_ERROR("Could not save vlc render");
   }
}

void TVLCConfiguration::SetRender(wxString render)
{
   m_render = render;
}

wxString TVLCConfiguration::GetRender()
{
   return m_render;
}

//ACE configuration

void TAceConfiguration::Load()
{
   wxString key;
   long size;
   long cacheType;

   if (!(TVLITECONFIG->Read(AC_FULL_PATH_KEY, &key, "")))
   {
      DBG_INFO("Acestream path: Used void value");
   }
   m_acePath = key;
   if (!(TVLITECONFIG->Read(AC_CACHE_SIZE_KEY, &size, 1073741824L)))
   {
      DBG_INFO("Acestream cache size: Used default value");
   }
   m_diskCacheSize = (unsigned long)size;
   if (!(TVLITECONFIG->Read(AC_CACHE_TYPE_KEY, &cacheType, E_CACHE_MEM)))
   {
      DBG_INFO("Acestream cache type: Used default value");
   }
   m_cacheType =  (eAceCacheType)cacheType;
}

void TAceConfiguration::Save()
{
   if (!(TVLITECONFIG->Write(AC_CACHE_SIZE_KEY, m_diskCacheSize)))
   {
      DBG_ERROR("Could not write Acestream cache size");
   }
   if (!(TVLITECONFIG->Write(AC_FULL_PATH_KEY, m_acePath)))
   {
      DBG_ERROR("Could not write Acestream path");
   }
   if (!(TVLITECONFIG->Write(AC_CACHE_TYPE_KEY, (long)m_cacheType)))
   {
      DBG_ERROR("Could not write Acestream cache type");
   }
}

void TAceConfiguration::SetFullPath(wxString path)
{
   m_acePath = path;
}

wxString TAceConfiguration::GetFullPath()
{
   return m_acePath;
}

void TAceConfiguration::SetCacheSize(unsigned long size)
{
   m_diskCacheSize = size;
}

unsigned long TAceConfiguration::GetCacheSize()
{
   return m_diskCacheSize;
}

eAceCacheType TAceConfiguration::GetCacheType()
{
   return m_cacheType;
}

void TAceConfiguration::SetCacheType(eAceCacheType type)
{
   m_cacheType = type;
}

//Network, proxy configuration

void TNetworkConfiguration::Load()
{
   bool boolkey;
   wxString stringkey;
   long  intKey;

   if (!(TVLITECONFIG->Read(NET_PROXY_USED_KEY, &boolkey, false)))
   {
      DBG_INFO("Proxy used: Used void value (false)");
   }
   m_proxyUsed = boolkey;
   if (!(TVLITECONFIG->Read(NET_PROXY_AUTH_USED_KEY, &boolkey, false)))
   {
      DBG_INFO("Proxy auth: Used void value (false)");
   }
   m_proxyAuthUsed = boolkey;
   if (!(TVLITECONFIG->Read(NET_PROXY_URL_KEY, &stringkey, "")))
   {
      DBG_INFO("Proxy address: Used default value (void)");
   }
   m_proxyAddress = stringkey;
   if (!(TVLITECONFIG->Read(NET_PROXY_TYPE_KEY, &intKey, 0)))
   {
      DBG_INFO("Proxy type: Used default value (HTTP)");
   }
   m_proxyType = (eProxyType)intKey;
   if (!(TVLITECONFIG->Read(NET_PROXY_PORT_KEY, &intKey, 0)))
   {
      DBG_INFO("Proxy port: Used default value (0)");
   }
   m_proxyPort = (unsigned short)intKey;
   if (!(TVLITECONFIG->Read(NET_PROXY_USER, &stringkey, "")))
   {
      DBG_INFO("Proxy user: default value (void)");
   }
   m_proxyUser = stringkey;
   if (!(TVLITECONFIG->Read(NET_PROXY_PASS, &stringkey, "")))
   {
      DBG_INFO("Proxy password: default value (void)");
   }
   m_proxyPassword = stringkey;

}

void TNetworkConfiguration::Save()
{
   if (!(TVLITECONFIG->Write(NET_PROXY_USED_KEY, m_proxyUsed)))
   {
      DBG_INFO("Proxy used: could not write value");
   }
   if (!(TVLITECONFIG->Write(NET_PROXY_AUTH_USED_KEY, m_proxyAuthUsed)))
   {
      DBG_INFO("Proxy auth used: could not write value");
   }
   if (!(TVLITECONFIG->Write(NET_PROXY_TYPE_KEY, (int)m_proxyType)))
   {
      DBG_INFO("Proxy type: could not write value");
   }
   if (!(TVLITECONFIG->Write(NET_PROXY_URL_KEY, m_proxyAddress)))
   {
      DBG_INFO("Proxy address: could not write value");
   }
   if (!(TVLITECONFIG->Write(NET_PROXY_PORT_KEY, (long)m_proxyPort)))
   {
      DBG_INFO("Proxy port: could not write value");
   }
   if (!(TVLITECONFIG->Write(NET_PROXY_USER, m_proxyUser)))
   {
      DBG_INFO("Proxy user: could not write value");
   }
   if (!(TVLITECONFIG->Write(NET_PROXY_PASS, m_proxyPassword)))
   {
      DBG_INFO("Proxy password: could not write value");
   }

}


void TNetworkConfiguration::SetProxyType(eProxyType type)
{
   m_proxyType = type;
}

eProxyType TNetworkConfiguration::GetProxyType()
{
   return m_proxyType;
}

void TNetworkConfiguration::SetProxyUsed(bool used)
{
   m_proxyUsed = used;
}

bool TNetworkConfiguration::GetProxyUsed()
{
   return m_proxyUsed;
}

void TNetworkConfiguration::SetProxyAddress(wxString address)
{
   m_proxyAddress = address;
}

wxString TNetworkConfiguration::GetProxyAddress()
{
   return m_proxyAddress;
}

void TNetworkConfiguration::SetProxyPort(unsigned short port)
{
   m_proxyPort = port;
}

unsigned short TNetworkConfiguration::GetProxyPort()
{
   return m_proxyPort;
}

void TNetworkConfiguration::SetProxyAuthUsed(bool used)
{
   m_proxyAuthUsed = used;
}

bool TNetworkConfiguration::GetProxyAuthUsed()
{
   return m_proxyAuthUsed;
}

void TNetworkConfiguration::SetProxyUserName(wxString username)
{
   m_proxyUser = username;
}

wxString TNetworkConfiguration::GetProxyUserName()
{
   return m_proxyUser;
}

void TNetworkConfiguration::SetProxyPassword(wxString password)
{
   m_proxyPassword = password;
}

wxString TNetworkConfiguration::GetProxyPassword()
{
   return m_proxyPassword;
}

///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include "estogglebutton.h"
#include "channellistctrl.h"
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/tglbtn.h>
#include <wx/wrapsizer.h>
#include <wx/choice.h>
#include <wx/listctrl.h>
#include <wx/srchctrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/splitter.h>
#include <wx/bmpbuttn.h>
#include <wx/button.h>
#include <wx/slider.h>
#include <wx/statbmp.h>
#include <wx/statusbr.h>
#include <wx/timer.h>
#include <wx/frame.h>
#include <wx/radiobox.h>
#include <wx/dialog.h>
#include <wx/dataview.h>
#include <wx/textctrl.h>
#include <wx/combobox.h>
#include <wx/gauge.h>
#include <wx/spinctrl.h>
#include <wx/checkbox.h>
#include <wx/notebook.h>

///////////////////////////////////////////////////////////////////////////

#define TRTIMER_ID 1000
#define START_TIMER_ID 1001
#define QUERYTIMER_ID 1002
#define MOVETIMER_ID 1003

///////////////////////////////////////////////////////////////////////////////
/// Class MainFrameBase
///////////////////////////////////////////////////////////////////////////////
class MainFrameBase : public wxFrame
{
	private:

	protected:
		wxMenuBar* m_menuBar;
		wxMenu* m_menuFile;
		wxMenu* m_menuManage;
		wxMenuItem* m_menuLocalLists;
		wxMenuItem* m_menuSubscriptions;
		wxMenu* m_menuView;
		wxMenuItem* m_menuChannelList;
		wxMenu* m_videoTracks;
		wxMenu* m_audioTracks;
		wxMenu* m_subtitleTracks;
		wxMenu* m_helpMenu;
		wxBoxSizer* mainSizer;
		wxSplitterWindow* m_splitter1;
		wxPanel* m_panel3;
		wxStaticText* m_staticText31;
		CESToggleButton* m_toggleLocalLists;
		CESToggleButton* m_toggleSubscriptions;
		CESToggleButton* m_toggleFavorites;
		wxStaticText* m_staticText1;
		wxChoice* m_choice1;
		wxSplitterWindow* m_splitter2;
		wxPanel* m_panel7;
		wxListCtrl* m_GroupList;
		wxBoxSizer* bSizer38;
		CESToggleButton* m_grouptbZA;
		wxSearchCtrl* m_groupSearchCtrl;
		wxPanel* m_panel8;
		wxBoxSizer* bSizer381;
		CESToggleButton* m_listtbAZ;
		CESToggleButton* m_listtbZA;
		wxSearchCtrl* m_listSearchCtrl;
		wxStaticText* m_statistics;
		wxPanel* m_panel4;
		wxBoxSizer* bSizer3;
		wxPanel* m_tvView;
		wxBoxSizer* bSizer41;
		wxBitmapButton* m_bpButtonPlay;
		wxBitmapButton* m_bpButtonStop;
		wxBitmapButton* m_bpButtonPause;
		wxBitmapButton* m_bpButtonRecord;
		wxStaticText* m_currenttime;
		wxSlider* m_movieslider;
		wxStaticText* m_totaltime;
		wxStaticBitmap* m_bitmap1;
		wxSlider* m_slider1;
		wxBitmapButton* m_bpButtonFullScreen;
		wxBitmapToggleButton* m_bmToggleMini;
		wxStatusBar* m_statusBar;
		wxTimer m_trtimer;
		wxTimer m_metaTimer;
		wxTimer m_aceStartTimer;
		wxTimer m_queryTimer;
		wxTimer m_moveTimer;

		// Virtual event handlers, override them in your derived class
		virtual void OnCloseFrame( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnMouseLeaveMiniFrame( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnMaximizeFrame( wxMaximizeEvent& event ) { event.Skip(); }
		virtual void OnMove( wxMoveEvent& event ) { event.Skip(); }
		virtual void OnPaintEvent( wxPaintEvent& event ) { event.Skip(); }
		virtual void OnShowFrame( wxShowEvent& event ) { event.Skip(); }
		virtual void OnSize( wxSizeEvent& event ) { event.Skip(); }
		virtual void OnLocalListAdd( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveList( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveView( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveSubscriptionAsLocalList( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnLocalListClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSubscriptionClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateAllSubscriptions( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnShowPref( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnExitClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnFullScreen( wxCommandEvent& event ) { event.Skip(); }
		virtual void ToggleChannelList( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMiniWindow( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnARDefault( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAR169( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAR43( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAbout( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnToggleLocalLists( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnToggleSubscriptions( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnToggleFavorites( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSubscriptionSelected( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnGroupSelect( wxListEvent& event ) { event.Skip(); }
		virtual void OnGroupListPaint( wxSizeEvent& event ) { event.Skip(); }
		virtual void OnToggleGroupAZ( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnToggleGroupZA( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPopulateChannel( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnListPlay( wxListEvent& event ) { event.Skip(); }
		virtual void OnListContextMenu( wxListEvent& event ) { event.Skip(); }
		virtual void OnChannelListPaint( wxSizeEvent& event ) { event.Skip(); }
		virtual void OnToggleListAZ( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnToggleListZA( wxCommandEvent& event ) { event.Skip(); }
		virtual void KeyPressed( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnEraseBackground( wxEraseEvent& event ) { event.Skip(); }
		virtual void ToggleFullScreen( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnPlay( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnStop( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPause( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRecord( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSeekMouseChanged( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnSeekUsrChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMuteClick( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnVolumeChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnToggleMini( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnStatusBarPaint( wxPaintEvent& event ) { event.Skip(); }
		virtual void OnStatusBarSize( wxSizeEvent& event ) { event.Skip(); }
		virtual void HideBar( wxTimerEvent& event ) { event.Skip(); }
		virtual void OnMetaTimer( wxTimerEvent& event ) { event.Skip(); }
		virtual void StartAcePlaying( wxTimerEvent& event ) { event.Skip(); }
		virtual void OnQueryTimer( wxTimerEvent& event ) { event.Skip(); }
		virtual void OnMoveTimer( wxTimerEvent& event ) { event.Skip(); }


	public:
		wxBoxSizer* bSizer4;
		CESToggleButton* m_grouptbAZ;
		channelListCtrl* m_ChannelList;

		MainFrameBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("TV-Lite"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 1024,700 ), long style = wxCLOSE_BOX|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER|wxSYSTEM_MENU|wxTAB_TRAVERSAL );

		~MainFrameBase();

		void m_splitter2OnIdle( wxIdleEvent& )
		{
			m_splitter2->SetSashPosition( 200 );
			m_splitter2->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainFrameBase::m_splitter2OnIdle ), NULL, this );
		}

};

///////////////////////////////////////////////////////////////////////////////
/// Class ControlsFrameBase
///////////////////////////////////////////////////////////////////////////////
class ControlsFrameBase : public wxFrame
{
	private:

	protected:

		// Virtual event handlers, override them in your derived class
		virtual void OnMoveOver( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnLeave( wxMouseEvent& event ) { event.Skip(); }


	public:
		wxBoxSizer* bSizerControls;

		ControlsFrameBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,80 ), long style = wxFRAME_FLOAT_ON_PARENT|wxFRAME_NO_TASKBAR|wxBORDER_NONE );

		~ControlsFrameBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CRecordChoiceBase
///////////////////////////////////////////////////////////////////////////////
class CRecordChoiceBase : public wxDialog
{
	private:

	protected:
		wxRadioBox* m_recordChoiceBox;

		// Virtual event handlers, override them in your derived class
		virtual void OnLeave( wxFocusEvent& event ) { event.Skip(); }
		virtual void OnLeaveRadioBox( wxFocusEvent& event ) { event.Skip(); }


	public:

		CRecordChoiceBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0|wxBORDER_NONE );

		~CRecordChoiceBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CLocalListDialogBase
///////////////////////////////////////////////////////////////////////////////
class CLocalListDialogBase : public wxDialog
{
	private:

	protected:
		wxListCtrl* m_LocalDataList;
		wxButton* m_newButton;
		wxButton* m_editButton;
		wxButton* m_deleteButton;
		wxButton* m_closeButton;

		// Virtual event handlers, override them in your derived class
		virtual void OnNewClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		CLocalListDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Local Lists"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE );

		~CLocalListDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CSubscriptionListDialogBase
///////////////////////////////////////////////////////////////////////////////
class CSubscriptionListDialogBase : public wxDialog
{
	private:

	protected:
		wxDataViewListCtrl* m_SubscriptionDataList;
		wxButton* m_newButton;
		wxButton* m_xtreamButton;
		wxButton* m_updateButton;
		wxButton* m_editButton;
		wxButton* m_deleteButton;
		wxButton* m_updateAllButton;
		wxButton* m_close;

		// Virtual event handlers, override them in your derived class
		virtual void OnNewClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnXtreamButtonClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateAll( wxCommandEvent& event ) { event.Skip(); }


	public:

		CSubscriptionListDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Subscriptions"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE );

		~CSubscriptionListDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CSubscriptionDialogBase
///////////////////////////////////////////////////////////////////////////////
class CSubscriptionDialogBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_NameLabel;
		wxTextCtrl* m_Name;
		wxStaticText* m_staticText2;
		wxTextCtrl* m_URLText;
		wxButton* m_PathButton;
		wxStdDialogButtonSizer* m_sdbSizer2;
		wxButton* m_sdbSizer2OK;
		wxButton* m_sdbSizer2Cancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnBrowseButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCancelButtonClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOkButtonClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CSubscriptionDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Path"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 766,-1 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );

		~CSubscriptionDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CLocalDialogBase
///////////////////////////////////////////////////////////////////////////////
class CLocalDialogBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_NameLabel;
		wxTextCtrl* m_Name;
		wxStaticText* m_AuthorLabel;
		wxTextCtrl* m_Author;
		wxStaticText* m_URLLabel;
		wxTextCtrl* m_urlText;
		wxStaticText* m_VersionLabel;
		wxTextCtrl* m_versionText;
		wxStdDialogButtonSizer* m_sdbSizer2;
		wxButton* m_sdbSizer2OK;
		wxButton* m_sdbSizer2Cancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnOkClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CLocalDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Edit Local List"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );

		~CLocalDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CChannelEditBase
///////////////////////////////////////////////////////////////////////////////
class CChannelEditBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText8;
		wxTextCtrl* m_channelName;
		wxStaticText* m_staticText25;
		wxComboBox* m_GroupBox;
		wxStaticText* m_staticText9;
		wxDataViewListCtrl* m_addressList;
		wxBitmapButton* m_addButton;
		wxBitmapButton* m_deleteButton;
		wxBitmapButton* m_upButton;
		wxBitmapButton* m_downButton;
		wxBitmapButton* m_vlcoptButton;
		wxStdDialogButtonSizer* m_sdbSizer3;
		wxButton* m_sdbSizer3OK;
		wxButton* m_sdbSizer3Cancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnAddressEditDone( wxDataViewEvent& event ) { event.Skip(); }
		virtual void OnAddressEditStarted( wxDataViewEvent& event ) { event.Skip(); }
		virtual void OnAddAddress( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteAddress( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDownClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnVLCOptClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOkClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CChannelEditBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Channel Editor"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 663,524 ), long style = wxDEFAULT_DIALOG_STYLE );

		~CChannelEditBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CDlProgressBase
///////////////////////////////////////////////////////////////////////////////
class CDlProgressBase : public wxDialog
{
	private:

	protected:
		wxBoxSizer* bSizer17;
		wxStaticText* m_urlText;
		wxGauge* m_progressGauge;
		wxStdDialogButtonSizer* m_sdbSizer4;
		wxButton* m_sdbSizer4Cancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnCloseClicked( wxCloseEvent& event ) { event.Skip(); }
		virtual void Init( wxInitDialogEvent& event ) { event.Skip(); }
		virtual void OnShow( wxShowEvent& event ) { event.Skip(); }
		virtual void OnCancelClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CDlProgressBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Download progress"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );

		~CDlProgressBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CPrefDialogBase
///////////////////////////////////////////////////////////////////////////////
class CPrefDialogBase : public wxDialog
{
	private:

	protected:
		wxNotebook* m_notebook;
		wxPanel* m_genpanel;
		wxStaticText* m_staticText19;
		wxSpinCtrl* m_streamPortSpinCtrl;
		wxStaticText* m_staticText20;
		wxTextCtrl* m_recordPathTextCtrl;
		wxButton* m_recPathButton;
		wxCheckBox* m_notifCheck;
		wxCheckBox* m_updateallcheck;
		wxCheckBox* m_playlastcheck;
		wxPanel* m_vlcpanel;
		wxRadioBox* m_radioBox4;
		wxStaticText* m_staticText21;
		wxTextCtrl* m_textCtrl13;
		wxPanel* m_acepanel;
		wxStaticText* m_staticText18;
		wxTextCtrl* m_aceExecPathTextCtrl;
		wxButton* m_acePathButton;
		wxRadioBox* m_CacheTypeRB;
		wxStaticText* m_staticText16;
		wxSpinCtrl* m_aceMaxCacheSizeSpinCtrl;
		wxStaticText* m_staticText17;
		wxStaticText* m_staticText201;
		wxPanel* m_networkPanel;
		wxCheckBox* m_proxyCheck;
		wxStaticText* m_staticText26;
		wxStaticText* m_staticText27;
		wxStaticText* m_staticText28;
		wxChoice* m_proxyTypeChoice;
		wxTextCtrl* m_addressText;
		wxTextCtrl* m_portText;
		wxCheckBox* m_authCheck;
		wxStaticText* m_staticText30;
		wxTextCtrl* m_userText;
		wxStaticText* m_staticText31;
		wxTextCtrl* m_passwordText;
		wxStdDialogButtonSizer* m_sdbSizer5;
		wxButton* m_sdbSizer5OK;
		wxButton* m_sdbSizer5Cancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnRecordingPathClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnHwAccType( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAcestreamExecPathClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnProxyUseClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnProxyAuthCheck( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOkClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CPrefDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("TV-Lite - Preferences"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 688,426 ), long style = wxDEFAULT_DIALOG_STYLE );

		~CPrefDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CXtreamDlgBase
///////////////////////////////////////////////////////////////////////////////
class CXtreamDlgBase : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText24;
		wxTextCtrl* m_NameText;
		wxStaticText* m_HostPortLabel;
		wxTextCtrl* m_HostPortText;
		wxStaticText* m_usernameLabel;
		wxTextCtrl* m_usernameText;
		wxStaticText* m_passwordLabel;
		wxTextCtrl* m_passwordText;
		wxRadioBox* m_typeRadioBox;
		wxRadioBox* m_outputRadioBox;
		wxStdDialogButtonSizer* m_sdbSizer6;
		wxButton* m_sdbSizer6OK;
		wxButton* m_sdbSizer6Cancel;

	public:

		CXtreamDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Xtream credentials"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_DIALOG_STYLE );

		~CXtreamDlgBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CChannelInfoBase
///////////////////////////////////////////////////////////////////////////////
class CChannelInfoBase : public wxDialog
{
	private:

	protected:
		wxTextCtrl* m_InfoTextCtrl;
		wxStdDialogButtonSizer* m_sdbSizer7;
		wxButton* m_sdbSizer7OK;

		// Virtual event handlers, override them in your derived class
		virtual void OnOkClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CChannelInfoBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Channel Information"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~CChannelInfoBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class CVLCOptDlgBase
///////////////////////////////////////////////////////////////////////////////
class CVLCOptDlgBase : public wxDialog
{
	private:

	protected:
		wxDataViewListCtrl* m_optList;
		wxBitmapButton* m_addButton;
		wxBitmapButton* m_delButton;
		wxStdDialogButtonSizer* m_sdbSizer8;
		wxButton* m_sdbSizer8OK;
		wxButton* m_sdbSizer8Cancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnPlusClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMinusClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOkClicked( wxCommandEvent& event ) { event.Skip(); }


	public:

		CVLCOptDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("VLC options"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~CVLCOptDlgBase();

};


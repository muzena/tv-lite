#include "controlsframe.h"
#include "main.h"
#include "debug.h"

ControlsFrame::ControlsFrame(wxWindow *parent):ControlsFrameBase(parent)
{
}

ControlsFrame::~ControlsFrame()
{
}

void ControlsFrame::OnMoveOver( wxMouseEvent& event )
{
   MainFrame* mainFrame = (MainFrame*)wxGetApp().GetMainFrame();
   wxTimer* mouseTimer = mainFrame->GetMouseTimer();

   if (!mainFrame->isFullScreen() && !mainFrame->isMinSizeSet()) return;
   if (mouseTimer->IsRunning())
   {
       mouseTimer->Stop();
   }
}

void ControlsFrame::OnLeave( wxMouseEvent& event )
{  
    if (!wxGetApp().GetMainFrame()->isMinSizeSet())
    {
       return;
    }
    wxRect rect = GetClientRect();
    wxPoint pnt = event.GetPosition();
   // DBG_INFO("y = %d, bottom = %d", pnt.y, rect.GetHeight());
   if (pnt.y >= rect.GetHeight())
    {
       Hide();
    }
}
#include "dlprogress.h"
#include "debug.h"
#include "main.h"

#define ID_REFRESH 1000
DEFINE_EVENT_TYPE(DTREADNOTIFY_EVT)
DEFINE_EVENT_TYPE(DTPARSEDNOTIFY_EVT)

using namespace tvlite;


CDLProgress::CDLProgress(wxWindow *parent, TSubscriptionList *list) :
               CDlProgressBase(parent),
               m_subscriptionList(list),
               m_timer(this, ID_REFRESH),
               downloadIndex(0),
               m_canClose(true)
{


}
CDLProgress::~CDLProgress()
{
   m_timer.Stop();
   chanlist->Clear();
   delete chanlist;
}

void CDLProgress::Init( wxInitDialogEvent& event )
{
#ifdef __WXGTK20__

   wxSize size = m_progressGauge->GetSize();
   size.SetHeight(20);
   m_progressGauge->SetMaxSize(size);
   this->Layout();
   bSizer17->Fit(this);

#endif
   Connect( ID_REFRESH, wxEVT_TIMER, wxTimerEventHandler(CDLProgress::onTimer) );
   Connect(wxID_ANY, DTREADNOTIFY_EVT, wxCommandEventHandler(CDLProgress::OnDownloadFinished));
   Connect(wxID_ANY, DTPARSEDNOTIFY_EVT, wxCommandEventHandler(CDLProgress::OnParseFinished));
   chanlist = new TChannelList();
   m_currentSubscription = nullptr;
   m_progressGauge->SetRange(100);
   m_progressGauge->Pulse();
   m_timer.Start(100);
   StartDownload();
   
}

void CDLProgress::PrintSize()
{
   int x = GetSize().GetWidth();
   int y = GetSize().GetHeight();
   DBG_INFO("size %d, %d", x, y );
}

void CDLProgress::OnShow(wxShowEvent& event)
{
   PrintSize();
   event.Skip();
}

void CDLProgress::StartDownload()
{
   PrintSize();
   chanlist->Clear();
   m_currentSubscription = m_subscriptionList->Item(downloadIndex);
   m_currentSubscription->SetEventHandler(this);
   m_urlText->SetLabel(m_currentSubscription->GetSubscriptionInfo()->url);
   m_currentSubscription->ReadData(chanlist);
}

int CDLProgress::NextDownload()
{
   int rc;
   m_currentSubscription->SetEventHandler(nullptr);
   downloadIndex++;
   if (downloadIndex >= m_subscriptionList->GetCount())
   {
      Disconnect(DTREADNOTIFY_EVT, wxCommandEventHandler(CDLProgress::OnDownloadFinished));
      Disconnect(DTPARSEDNOTIFY_EVT, wxCommandEventHandler(CDLProgress::OnParseFinished));
      Disconnect(wxEVT_TIMER, wxTimerEventHandler(CDLProgress::onTimer));
      m_timer.Stop();
      rc = E_DLP_OK;
   }
   else
   {
      StartDownload();
      rc = E_DLP_NEXT;
   }
   return rc;
}

void CDLProgress::OnCancelClicked( wxCommandEvent& event )
{
   CDownloadClient *dlClient = nullptr;

   m_timer.Stop();
   if (m_currentSubscription)
   {
      dlClient = m_currentSubscription->GetDLClient();
      if (dlClient)
      {
         dlClient->SetCanceled(true);
         dlClient->SetEventHandler(NULL);
      }
   }

   if (NextDownload() == E_DLP_OK) //otherwise E_DLP_NEXT
   {
       m_currentSubscription->SetEventHandler(NULL);
       Disconnect(DTREADNOTIFY_EVT, wxCommandEventHandler(CDLProgress::OnDownloadFinished));
       Disconnect(DTPARSEDNOTIFY_EVT, wxCommandEventHandler(CDLProgress::OnParseFinished));
       Disconnect(wxEVT_TIMER, wxTimerEventHandler(CDLProgress::onTimer));
       EndModal(E_DLP_CANCELED);
   }
}

void CDLProgress::OnCloseClicked( wxCloseEvent& event )
{
   if (!m_canClose)
   {
       if (event.CanVeto()) event.Veto();
   }
   else
   {
      m_timer.Stop();
      CDownloadClient *dlClient = nullptr;
      if (m_currentSubscription)
      {
         dlClient = m_currentSubscription->GetDLClient();
         if (dlClient)
         {
            dlClient->SetCanceled(true);
            dlClient->SetEventHandler(NULL);
         }
      }
      m_currentSubscription->SetEventHandler(NULL);
      Disconnect(DTREADNOTIFY_EVT, wxCommandEventHandler(CDLProgress::OnDownloadFinished));
      Disconnect(wxEVT_TIMER, wxTimerEventHandler(CDLProgress::onTimer));
      EndModal(E_DLP_CANCELED);
   }
}

void CDLProgress::OnDownloadFinished( wxCommandEvent& event )
{

   if (event.GetInt() != E_DLP_OK)
   {
      wxMessageBox(_("Could not update subscription ") + m_currentSubscription->GetURI(), _("Error"), wxOK | wxICON_ERROR, NULL);

   }
   else
   {
       m_sdbSizer4Cancel->Disable();
       m_canClose = false;
   }
}

void CDLProgress::OnParseFinished( wxCommandEvent& event )
{
   m_sdbSizer4Cancel->Enable();
   m_canClose = true;
   if (NextDownload() == E_DLP_OK)
   {
       m_currentSubscription->SetEventHandler(NULL);
       EndModal(event.GetInt());
   }
}


void CDLProgress::onTimer(wxTimerEvent &event)
{
   CDownloadClient *dlClient = nullptr;
   if (m_currentSubscription)
   {
      dlClient = m_currentSubscription->GetDLClient();
      if (dlClient)
      {
         if (dlClient->GetDownloadSizeExists())
         {
            int val =(int)(dlClient->GetDownloadOffset()*100.0/dlClient->GetDownloadSize());
            if (val < 0 )
            {
               val = 0;
            }
            if (val > 100)
            {
               val = 100;
            }

            m_progressGauge->SetValue(val);
         }
         else
         {
             m_progressGauge->Pulse();
         }
      }
      else
      {
          m_progressGauge->Pulse();
      }
   }
   else
   {
      m_progressGauge->Pulse();
   }
   event.Skip();

}



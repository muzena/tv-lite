
#include <wx/filedlg.h>

#include "subscription.h"
#include "subscriptiondialog.h"
#include "subscriptionlistdialog.h"
#include "database.h"
#include "debug.h"

using namespace tvlite;

CSubscriptionDialog::CSubscriptionDialog(wxWindow* parent):CSubscriptionDialogBase(parent)
{
   m_sdbSizer2OK->SetDefault();
}

CSubscriptionDialog::~CSubscriptionDialog()
{
}

void CSubscriptionDialog::SetNameField(wxString param)
{
   m_Name->SetValue(param);
}

const wxString CSubscriptionDialog::GetNameField() const
{
   return m_Name->GetValue();
}


void CSubscriptionDialog::SetData(CSubscriptionInfo *info)
{
   m_Name->SetValue(info->name);
   m_URLText->SetValue(info->url);
}

void CSubscriptionDialog::GetData(CSubscriptionInfo *info)
{
   info->name = m_Name->GetValue().Trim().Trim(false);
   info->url  = m_URLText->GetValue().Trim().Trim(false);
}


void CSubscriptionDialog::SetURLField(wxString param)
{
   m_URLText->SetValue(param);
}

const wxString CSubscriptionDialog::GetURLField() const
{
   return m_URLText->GetValue();
}


void CSubscriptionDialog::OnCancelButtonClicked( wxCommandEvent& event )
{
   event.Skip();
   EndModal(wxID_CANCEL);
}

void CSubscriptionDialog::OnOkButtonClicked( wxCommandEvent& event )
{
    EndModal(wxID_OK);   
} 


void CSubscriptionDialog::OnBrowseButtonClick(wxCommandEvent& event)
{
    wxFileDialog openFileDialog(this, _("Open XYZ file"), "", "",
                       wxFileSelectorDefaultWildcardStr, wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL)
    {
       return;
    }
    m_URLText->SetValue(openFileDialog.GetPath());                      
}


int CSubscriptionDialog::ShowModal()
{
   int parentWidth, parentHeight, width, height;
   GetParent()->GetSize(&parentWidth, &parentHeight);
   Fit();
   GetSize(&width,&height);
   SetSize(parentWidth, height);
   m_subscriptionName = m_Name->GetValue();
   m_subscriptionURL  = m_URLText->GetValue();
   return CSubscriptionDialogBase::ShowModal();
}

int CSubscriptionDialog::ShowURLOnly()
{
   
   m_NameLabel->Hide();
   m_Name->Hide();
   return ShowModal();
}

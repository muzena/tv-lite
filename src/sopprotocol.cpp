#include <wx/msgdlg.h>
#include "main.h"
#include "debug.h"
#include "sopprotocol.h"

#include <sys/types.h>
#include <signal.h>

#define E_ERROR -1
#define E_OK 0

using namespace tvlite;

CSopProtocol::CSopProtocol(wxEvtHandler *parent, wxString url, wxString cmd):
   CBaseProtocol(parent,url, cmd),
   m_playerport(10321),
   m_protoport (30010)
   
{
    Redirect();
}

///////////////////////////////////////////////////////////////////////////

CSopProtocol::~CSopProtocol()
{
  DBG_INFO ("SOP protocol -> delete"); 
}

///////////////////////////////////////////////////////////////////////////

int CSopProtocol::LoadConfig()
{
   //TODO stub
   return 0;   
}

///////////////////////////////////////////////////////////////////////////

int CSopProtocol::SaveConfig()
{
   //TODO stub
   return 0;
}
  
/////////////////////////////////////////////////////////////////////////

int CSopProtocol::GetProtoport()
{
   return m_protoport;
};

///////////////////////////////////////////////////////////////////////////

void CSopProtocol::SetProtoPort(uint32_t port)
{
   m_protoport = port;
};

///////////////////////////////////////////////////////////////////////////

int CSopProtocol::GetPlayerPort()
{
   return m_playerport;
};

///////////////////////////////////////////////////////////////////////////

void CSopProtocol::SetPlayerPort(uint32_t port)
{
   m_playerport = port;
};

///////////////////////////////////////////////////////////////////////////
 
int  CSopProtocol::StartProtocol (long &pid)
{ 
   DBG_INFO("Starting fresh sopcast process");
   MainFrame *mainWindow = (MainFrame*)wxGetApp().GetTopWindow();
   pid = 0;
   int result = E_OK;
   if (m_cmd == "")
   {
      if (SearchSop(m_cmd) != 0)
      {
         wxMessageBox(_("Could not search for the Sopcast binary.\n Check your installation!"), _("Error"), wxOK|wxICON_EXCLAMATION, mainWindow);
      }
      else
      {
         if (m_cmd == "")
         {
            wxMessageBox(_("Could not find the Sopcast binary in the path."), _("Error"), wxOK|wxICON_EXCLAMATION, mainWindow);
            result = E_ERROR;
         }   
      }
         
   }
   
   if (result == E_OK)
   {
      wxString cmd = m_cmd << " " << m_url << " " << m_protoport << " " << m_playerport;
      DBG_INFO("Sopcast command is '%s'.", (const char*)cmd.utf8_str());
      long rc = wxExecute(cmd, wxEXEC_ASYNC, this);
      pid = rc;
      if (rc == 0L)
      {
         DBG_ERROR("Execution of '%s' failed.", (const char*)cmd.utf8_str());
         wxMessageBox(_("Could not execute Sopcast!"), _("Error"), wxOK|wxICON_EXCLAMATION, mainWindow);
         result = E_ERROR;
      }
   }
  
   return result;
}



//////////////////////////////////////////////////////////////////////////

int CSopProtocol::SearchSop(wxString &cmd)
{
   wxArrayString res, err;
   long rc = wxExecute("which sp-sc" , res, err);
   if (!rc)
   {
      if (res.IsEmpty())
      {
         rc = wxExecute("which sp-sc-auth", res, err);
      }
   }
   if (!rc)
   {
      if (!res.IsEmpty())
      {   
         cmd = res[0];
      }  
   } 
   return rc;
}
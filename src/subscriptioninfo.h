#ifndef SUBSCRIPTIONINFO_H
#define SUBSCRIPTIONINFO_H

#include <wx/string.h>

namespace tvlite
{
struct CSubscriptionInfo
{
   wxString name;
   wxString version;
   wxString author;
   wxString url;
   wxString epgurl;
};
}
#endif // SUBSCRIPTIONINFO_H

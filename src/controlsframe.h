#ifndef CONTROLSFRAME_H
#define CONTROLSFRAME_H


#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif 

class ControlsFrame : public ControlsFrameBase
{
protected:
   virtual void OnMoveOver( wxMouseEvent& event );
   virtual void OnLeave( wxMouseEvent& event );
   
public:
   ControlsFrame(wxWindow *parent);
   
   virtual ~ControlsFrame();

};

#endif // CONTROLSFRAME_H

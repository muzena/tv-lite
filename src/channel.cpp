
#include <rapidjson/document.h>     // rapidjson's DOM-style API
#include <rapidjson/prettywriter.h> // for stringify JSON
#include <cstdio>
#include <wx/uri.h>
#include <wx/intl.h>
#include "channel.h"
#include "debug.h"

using namespace rapidjson;
using namespace std;
using namespace tvlite;

#define E_OK 0
#define E_PARSE -1

//#include <wx/arrimpl.cpp>

CChannel::CChannel(wxString id, wxString name, wxArrayString streamurls, wxString iconfile):
      m_id(id), m_name(name), m_streamurls(streamurls)
{

}

CChannel::CChannel()
{

}

CChannel::~CChannel()
{
   m_streamurls.Clear();
   m_audiochannels.Clear();
   m_vlcoptrecords.clear();
   m_namerecords.clear();
}

wxString CChannel::GetName()
{
   return m_name;
}

void CChannel::SetName(wxString name)
{
   m_name = name;
}

wxString CChannel::GetId()
{
   return m_id;
}

void CChannel::SetId(wxString id)
{
   m_id = id;
}

wxArrayString CChannel::GetStreamURLs()
{
   return m_streamurls;
}

wxStringToStringHashMap CChannel::GetStreamNames()
{
   return m_namerecords;
}

CStringToArrayStringHashMap CChannel::GetVLCOptions()
{
   return m_vlcoptrecords;
}

wxArrayString CChannel::GetAudioChannels()
{
   return m_audiochannels;
}

void CChannel::SetStreamURLs(wxArrayString s)
{
   m_streamurls = s;
}

void CChannel::SetStreamNames(wxStringToStringHashMap s)
{
   m_namerecords = s;
}

void CChannel::SetVLCOptions(CStringToArrayStringHashMap s)
{
   m_vlcoptrecords = s;
}

void CChannel::SetAudioChannels(wxArrayString s)
{
   m_audiochannels = s;
}

void CChannel::SetGroup(wxString group)
{
   m_group = group;
}

wxString CChannel::GetGroup()
{
   return m_group;
}

int CChannel::ConvertFromJSONArray(wxString s)
{
   Document d;
   m_streamurls.Clear();
   int result = E_OK;
   const char* js = s.utf8_str();
   if (d.Parse(js).HasParseError())
   {
     result = E_PARSE;
   }
   if (result == E_OK)
   {
      if (!d.IsArray())
      {
        result = E_PARSE;
      }
   }
   if (result == E_OK)
   {
      const Value& refURLs = d;
      for (Value::ConstValueIterator itr = refURLs.Begin(); itr != refURLs.End(); ++itr)
      {
         m_streamurls.Add(wxString::FromUTF8(itr->GetString()));

      }
//      for (size_t i = 0; i < sURLs.GetCount(); i++)
//      {
//         //DBG_INFO("Found url: %s", (const char*)sURLs[i].utf8_str());
//      }
   }
   return result;
}


void CChannel::ConvertToJSONArray(wxArrayString sURLs, wxString& s)
{
   s = "[";
   for (size_t i = 0; i < sURLs.GetCount(); i++)
   {
      s = s + "\"" + sURLs[i] + "\"";
      if (i != sURLs.GetCount() - 1)
      {
         s = s + ", ";
      }
   }
   s = s + "]";
}

int CChannel::ConvertFromJSONParam(wxString s)
{
   Document d;
   int result = E_OK;
   const char* js = s.utf8_str();
   if (d.Parse(js).HasParseError())
   {
     result = E_PARSE;
   }
   if (result == E_OK)
   {
      if (!d.IsObject())
      {
         result = E_PARSE;
      }
   }
   if (result == E_OK)
   {
      for (Value::ConstMemberIterator itr = d.MemberBegin();
            itr != d.MemberEnd(); ++itr)
            {
               wxString itName = wxString::FromUTF8(itr->name.GetString());
               if (itName == "group" && itr->value.GetType() == kStringType)
               {
                  m_group = wxString::FromUTF8(itr->value.GetString());
               }
               else if (itName == "vlcoptions" && itr->value.GetType() == kObjectType)
               {
                    // iterate through URLs
                    for (Value::ConstMemberIterator itm = itr->value.MemberBegin();
                          itm != itr->value.MemberEnd(); ++itm)
                          {
                             wxString sUrl = wxString::FromUTF8(itm->name.GetString());
                             if (itm->value.GetType() == kArrayType)
                             {
                                 for (Value::ConstValueIterator itv = itm->value.Begin(); itv != itm->value.End(); ++itv)
                                 {
                                    m_vlcoptrecords[sUrl].Add(wxString::FromUTF8(itv->GetString()));
                                 }
                             }
                          }

               }
               else if (itr->value.GetType() == kStringType)
               {
                  m_namerecords[wxString::FromUTF8(itr->name.GetString())] = wxString::FromUTF8(itr->value.GetString());
               }
            }
   }

   return result;
}

wxString CChannel::ConvertToJSONParam()
{
   Document d;
   d.SetObject();
   if (m_namerecords.size() != 0)
   {
      wxStringToStringHashMap::iterator it;

      for (it = m_namerecords.begin(); it != m_namerecords.end(); ++it)
      {
        if (it->first == "group")
        {
           continue;
        }
        Value name;
        name.SetString((const char*)(it->first.utf8_str()), d.GetAllocator());
        Value value;
        value.SetString((const char*)(it->second.utf8_str()), d.GetAllocator());
        d.AddMember(name, value, d.GetAllocator());
      }
   }
   if (m_group != "")
   {
      Value group;
      group.SetString((const char*)(m_group.utf8_str()), d.GetAllocator());
      d.AddMember("group", group, d.GetAllocator());
   }

   if (m_vlcoptrecords.size() != 0)
   {
      CStringToArrayStringHashMap::iterator  it;
      Value vlcOptions(kObjectType);
      for (it = m_vlcoptrecords.begin(); it != m_vlcoptrecords.end(); ++it)
      {
         Value name;
         name.SetString((const char*)(it->first.utf8_str()), d.GetAllocator());
         Value optionArray(kArrayType);
         wxArrayString& optionsRef = it->second;
         for (size_t i = 0;  i < optionsRef.Count(); i++)
         {
            Value s;
            s.SetString((const char*)(optionsRef[i].utf8_str()), d.GetAllocator());
            optionArray.PushBack(s, d.GetAllocator());
         }
         vlcOptions.AddMember(name, optionArray, d.GetAllocator());
      }
      d.AddMember("vlcoptions", vlcOptions, d.GetAllocator());
   }

   StringBuffer buffer;
   Writer<StringBuffer> writer(buffer);
   d.Accept(writer);
   return wxString::FromUTF8(buffer.GetString());
}

int CChannel::CompareAsc( CChannel ***item1, CChannel ***item2)
{
  wxString s1 = (**item1)->GetName();
  wxString s2 = (**item2)->GetName();

  return strcmp((const char*)s1.utf8_str(), (const char*)s2.utf8_str());
}

int CChannel::CompareDesc( CChannel ***item1, CChannel ***item2)
{
  wxString s1 = (**item1)->GetName();
  wxString s2 = (**item2)->GetName();

  return (-1)*strcmp((const char*)s1.utf8_str(), (const char*)s2.utf8_str());
}

void CChannel::PopulateDefaultValues(wxArrayString& sURLs, wxStringToStringHashMap& sParams)
{
   for (size_t i = 0; i < sURLs.GetCount(); i++)
   {
      wxStringToStringHashMap::iterator it = sParams.find(sURLs[i]);
      if ( it == sParams.end() || it->second == "")
      {
         sParams[sURLs[i]] = AssignDefaultValue(sURLs[i]);
      }
   }
   wxStringToStringHashMap::iterator it = sParams.find("group");
   if ( it == sParams.end())
   {
      sParams["group"] = "";
   }


}

void CChannel::PopulateDefaultValues()
{
   PopulateDefaultValues(m_streamurls, m_namerecords);
}


wxString CChannel::AssignDefaultValue(wxString sUrl)
{
   int result = E_OK;
   wxURI uri;
   wxString rs;

   if (uri.Create(sUrl) != true)
   {
      DBG_ERROR("URI not recognized!");
      rs = _("Unknown protocol");
      result =  E_PARSE;
   }
   if (result == E_OK)
   {
      wxString scheme = uri.GetScheme();
      if (scheme == "sop")
      {
         rs = _("Sopcast address");
      }
      else if (scheme == "http"||
               scheme == "https"||
               scheme == "rtmp" ||
               scheme == "rtsp" ||
               scheme == "mms")
      {
          rs = _("Web address");
      }
      else if (scheme == "file")
      {
          rs  =  _("File ") ;
      }
      else if (scheme == "acestream")
      {
          rs = _("Acestream id");
      }
      else
      {
         rs = _("Unknown address");
      }
   }
   return rs;
}

wxArrayString CChannel::GetVLCOptions(wxString sUrl)
{
    wxArrayString vlcoptions;
    CStringToArrayStringHashMap::iterator it = m_vlcoptrecords.find(sUrl);
    if (it != m_vlcoptrecords.end())
    {
       vlcoptions = it->second;
    }
     return vlcoptions;
}

void CChannel::SetVLCOptions(wxString sURL, wxArrayString& options)
{
   //TODO:Check existence
   m_vlcoptrecords[sURL] = options;
}

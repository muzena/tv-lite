///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "gui.h"

#include "volume.xpm"

///////////////////////////////////////////////////////////////////////////

MainFrameBase::MainFrameBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	m_menuBar = new wxMenuBar( 0 );
	m_menuFile = new wxMenu();
	m_menuManage = new wxMenu();
	wxMenuItem* m_menuManageItem = new wxMenuItem( m_menuFile, wxID_ANY, _("Manage &Lists..."), wxEmptyString, wxITEM_NORMAL, m_menuManage );
	wxMenuItem* m_menuAddLocalList;
	m_menuAddLocalList = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Add Local List") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuAddLocalList );

	m_menuManage->AppendSeparator();

	wxMenuItem* m_menuSaveList;
	m_menuSaveList = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Save Whole Channel List As...") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSaveList );

	wxMenuItem* m_menuSaveView;
	m_menuSaveView = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Save Current View As...") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSaveView );

	m_menuManage->AppendSeparator();

	wxMenuItem* m_menuSaveSubscription;
	m_menuSaveSubscription = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Save Subscription As Local List") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSaveSubscription );

	m_menuManage->AppendSeparator();

	m_menuLocalLists = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Manage Local Lists") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuLocalLists );

	m_menuSubscriptions = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Manage Subscriptions") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuSubscriptions );

	m_menuManage->AppendSeparator();

	wxMenuItem* m_menuUpdateAll;
	m_menuUpdateAll = new wxMenuItem( m_menuManage, wxID_ANY, wxString( _("Update All Subscriptions") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuManage->Append( m_menuUpdateAll );

	m_menuFile->Append( m_menuManageItem );

	m_menuFile->AppendSeparator();

	wxMenuItem* m_menuPreferences;
	m_menuPreferences = new wxMenuItem( m_menuFile, wxID_ANY, wxString( _("Preferences") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( m_menuPreferences );

	m_menuFile->AppendSeparator();

	wxMenuItem* menuFileExit;
	menuFileExit = new wxMenuItem( m_menuFile, wxID_EXIT, wxString( _("E&xit") ) + wxT('\t') + wxT("Alt+X"), wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( menuFileExit );

	m_menuBar->Append( m_menuFile, _("&TVLite") );

	m_menuView = new wxMenu();
	wxMenuItem* m_menuFullScreen;
	m_menuFullScreen = new wxMenuItem( m_menuView, wxID_ANY, wxString( _("Full Screem") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuView->Append( m_menuFullScreen );

	m_menuChannelList = new wxMenuItem( m_menuView, wxID_ANY, wxString( _("Channel List") ) , wxEmptyString, wxITEM_CHECK );
	m_menuView->Append( m_menuChannelList );

	wxMenuItem* m_menuMiniItem;
	m_menuMiniItem = new wxMenuItem( m_menuView, wxID_ANY, wxString( _("Mini Window") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuView->Append( m_menuMiniItem );

	m_menuView->AppendSeparator();

	wxMenu* m_aspectRatio;
	m_aspectRatio = new wxMenu();
	wxMenuItem* m_aspectRatioItem = new wxMenuItem( m_menuView, wxID_ANY, _("Aspect Ratio"), wxEmptyString, wxITEM_NORMAL, m_aspectRatio );
	wxMenuItem* m_aspectRatioDefault;
	m_aspectRatioDefault = new wxMenuItem( m_aspectRatio, wxID_ANY, wxString( _("Default") ) , wxEmptyString, wxITEM_NORMAL );
	m_aspectRatio->Append( m_aspectRatioDefault );

	wxMenuItem* m_aspectRatio169;
	m_aspectRatio169 = new wxMenuItem( m_aspectRatio, wxID_ANY, wxString( _("16:9") ) , wxEmptyString, wxITEM_NORMAL );
	m_aspectRatio->Append( m_aspectRatio169 );

	wxMenuItem* m_aspectRatio43;
	m_aspectRatio43 = new wxMenuItem( m_aspectRatio, wxID_ANY, wxString( _("4:3") ) , wxEmptyString, wxITEM_NORMAL );
	m_aspectRatio->Append( m_aspectRatio43 );

	m_menuView->Append( m_aspectRatioItem );

	m_menuView->AppendSeparator();

	m_videoTracks = new wxMenu();
	wxMenuItem* m_videoTracksItem = new wxMenuItem( m_menuView, wxID_ANY, _("Video Tracks"), wxEmptyString, wxITEM_NORMAL, m_videoTracks );
	m_menuView->Append( m_videoTracksItem );

	m_audioTracks = new wxMenu();
	wxMenuItem* m_audioTracksItem = new wxMenuItem( m_menuView, wxID_ANY, _("Audio Tracks"), wxEmptyString, wxITEM_NORMAL, m_audioTracks );
	m_menuView->Append( m_audioTracksItem );

	m_subtitleTracks = new wxMenu();
	wxMenuItem* m_subtitleTracksItem = new wxMenuItem( m_menuView, wxID_ANY, _("Subtitle Tracks"), wxEmptyString, wxITEM_NORMAL, m_subtitleTracks );
	m_menuView->Append( m_subtitleTracksItem );

	m_menuBar->Append( m_menuView, _("&View") );

	m_helpMenu = new wxMenu();
	wxMenuItem* m_menuAbout;
	m_menuAbout = new wxMenuItem( m_helpMenu, wxID_ANY, wxString( _("About") ) , wxEmptyString, wxITEM_NORMAL );
	m_helpMenu->Append( m_menuAbout );

	m_menuBar->Append( m_helpMenu, _("Help") );

	this->SetMenuBar( m_menuBar );

	mainSizer = new wxBoxSizer( wxVERTICAL );

	m_splitter1 = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D|wxSP_3DSASH );
	m_panel3 = new wxPanel( m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	bSizer4 = new wxBoxSizer( wxVERTICAL );

	m_staticText31 = new wxStaticText( m_panel3, wxID_ANY, _("List Types"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	bSizer4->Add( m_staticText31, 0, wxALL, 5 );

	wxWrapSizer* wSizer1;
	wSizer1 = new wxWrapSizer( wxHORIZONTAL, wxWRAPSIZER_DEFAULT_FLAGS );

	m_toggleLocalLists = new CESToggleButton( m_panel3, wxID_ANY, _("Local Lists"), wxDefaultPosition, wxDefaultSize, 0 );
	wSizer1->Add( m_toggleLocalLists, 0, wxALL, 1 );

	m_toggleSubscriptions = new CESToggleButton( m_panel3, wxID_ANY, _("Subscriptions"), wxDefaultPosition, wxDefaultSize, 0 );
	m_toggleSubscriptions->SetValue( true );
	wSizer1->Add( m_toggleSubscriptions, 0, wxALL|wxEXPAND, 1 );

	m_toggleFavorites = new CESToggleButton( m_panel3, wxID_ANY, _("Favorites"), wxDefaultPosition, wxDefaultSize, 0 );
	wSizer1->Add( m_toggleFavorites, 0, wxALL, 1 );


	bSizer4->Add( wSizer1, 0, wxALL|wxEXPAND, 5 );

	m_staticText1 = new wxStaticText( m_panel3, wxID_ANY, _("Channel List:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizer4->Add( m_staticText1, 0, wxALL, 5 );

	wxArrayString m_choice1Choices;
	m_choice1 = new wxChoice( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice1Choices, 0 );
	m_choice1->SetSelection( 0 );
	bSizer4->Add( m_choice1, 0, wxALL|wxEXPAND, 5 );

	m_splitter2 = new wxSplitterWindow( m_panel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter2->Connect( wxEVT_IDLE, wxIdleEventHandler( MainFrameBase::m_splitter2OnIdle ), NULL, this );

	m_panel7 = new wxPanel( m_splitter2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer30;
	bSizer30 = new wxBoxSizer( wxVERTICAL );

	m_GroupList = new wxListCtrl( m_panel7, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxLC_REPORT|wxLC_SINGLE_SEL );
	m_GroupList->SetMinSize( wxSize( -1,85 ) );

	bSizer30->Add( m_GroupList, 1, wxALL|wxEXPAND, 5 );

	bSizer38 = new wxBoxSizer( wxHORIZONTAL );

	m_grouptbAZ = new CESToggleButton( m_panel7, wxID_ANY, _("A-Z"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer38->Add( m_grouptbAZ, 0, wxALL|wxEXPAND, 1 );

	m_grouptbZA = new CESToggleButton( m_panel7, wxID_ANY, _("Z-A"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer38->Add( m_grouptbZA, 0, wxALL|wxEXPAND, 1 );

	m_groupSearchCtrl = new wxSearchCtrl( m_panel7, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	#ifndef __WXMAC__
	m_groupSearchCtrl->ShowSearchButton( true );
	#endif
	m_groupSearchCtrl->ShowCancelButton( true );
	bSizer38->Add( m_groupSearchCtrl, 1, wxALL|wxEXPAND, 1 );


	bSizer30->Add( bSizer38, 0, wxEXPAND, 1 );


	m_panel7->SetSizer( bSizer30 );
	m_panel7->Layout();
	bSizer30->Fit( m_panel7 );
	m_panel8 = new wxPanel( m_splitter2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxVERTICAL );

	m_ChannelList = new channelListCtrl( m_panel8, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_SINGLE_SEL|wxLC_VIRTUAL );
	bSizer31->Add( m_ChannelList, 1, wxALL|wxEXPAND, 5 );

	bSizer381 = new wxBoxSizer( wxHORIZONTAL );

	m_listtbAZ = new CESToggleButton( m_panel8, wxID_ANY, _("A-Z"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer381->Add( m_listtbAZ, 0, wxALL|wxEXPAND, 1 );

	m_listtbZA = new CESToggleButton( m_panel8, wxID_ANY, _("Z-A"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer381->Add( m_listtbZA, 0, wxALL|wxEXPAND, 1 );

	m_listSearchCtrl = new wxSearchCtrl( m_panel8, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	#ifndef __WXMAC__
	m_listSearchCtrl->ShowSearchButton( true );
	#endif
	m_listSearchCtrl->ShowCancelButton( true );
	bSizer381->Add( m_listSearchCtrl, 1, wxALL|wxEXPAND, 1 );


	bSizer31->Add( bSizer381, 0, wxEXPAND, 5 );


	m_panel8->SetSizer( bSizer31 );
	m_panel8->Layout();
	bSizer31->Fit( m_panel8 );
	m_splitter2->SplitHorizontally( m_panel7, m_panel8, 200 );
	bSizer4->Add( m_splitter2, 1, wxEXPAND, 5 );

	m_statistics = new wxStaticText( m_panel3, wxID_ANY, _("Total:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_statistics->Wrap( -1 );
	bSizer4->Add( m_statistics, 0, wxALL, 5 );


	m_panel3->SetSizer( bSizer4 );
	m_panel3->Layout();
	bSizer4->Fit( m_panel3 );
	m_panel4 = new wxPanel( m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3 = new wxBoxSizer( wxVERTICAL );

	m_tvView = new wxPanel( m_panel4, wxID_ANY, wxDefaultPosition, wxSize( 640,-1 ), wxTAB_TRAVERSAL );
	m_tvView->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	m_tvView->SetMinSize( wxSize( 640,360 ) );

	bSizer3->Add( m_tvView, 1, wxEXPAND | wxALL, 0 );

	bSizer41 = new wxBoxSizer( wxHORIZONTAL );

	m_bpButtonPlay = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonPlay->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	m_bpButtonPlay->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-media-play"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonPlay, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonStop = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonStop->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-stop"), wxART_BUTTON ) );
	m_bpButtonStop->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-stop"), wxART_BUTTON ) );
	m_bpButtonStop->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-stop"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonStop, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonPause = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonPause->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	m_bpButtonPause->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-media-pause"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonPause, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonRecord = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonRecord->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	m_bpButtonRecord->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-media-record"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonRecord, 0, wxALIGN_CENTER|wxALL, 5 );

	m_currenttime = new wxStaticText( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_currenttime->Wrap( -1 );
	m_currenttime->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Sans") ) );

	bSizer41->Add( m_currenttime, 0, wxALIGN_CENTER_VERTICAL|wxALL|wxRIGHT, 5 );

	m_movieslider = new wxSlider( m_panel4, wxID_ANY, 0, 0, 10000, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxBORDER_NONE );
	bSizer41->Add( m_movieslider, 1, wxALIGN_CENTER|wxALL, 0 );

	m_totaltime = new wxStaticText( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_totaltime->Wrap( -1 );
	m_totaltime->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Sans") ) );

	bSizer41->Add( m_totaltime, 0, wxALIGN_CENTER_VERTICAL|wxALL|wxLEFT, 5 );

	m_bitmap1 = new wxStaticBitmap( m_panel4, wxID_ANY, wxBitmap( volume_xpm ), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer41->Add( m_bitmap1, 0, wxALIGN_CENTER|wxALL, 5 );

	m_slider1 = new wxSlider( m_panel4, wxID_ANY, 80, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer41->Add( m_slider1, 0, wxALIGN_CENTER|wxALL, 5 );

	m_bpButtonFullScreen = new wxBitmapButton( m_panel4, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_bpButtonFullScreen->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-fullscreen"), wxART_BUTTON ) );
	bSizer41->Add( m_bpButtonFullScreen, 0, wxALIGN_CENTER|wxALIGN_RIGHT|wxALL, 5 );

	m_bmToggleMini = new wxBitmapToggleButton( m_panel4, wxID_ANY, wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ), wxDefaultPosition, wxDefaultSize, wxBORDER_NONE );

	m_bmToggleMini->SetBitmap( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_bmToggleMini->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_bmToggleMini->SetBitmapPressed( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_bmToggleMini->SetBitmapFocus( wxNullBitmap );
	m_bmToggleMini->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	bSizer41->Add( m_bmToggleMini, 0, wxALL, 5 );


	bSizer3->Add( bSizer41, 0, wxEXPAND, 5 );


	m_panel4->SetSizer( bSizer3 );
	m_panel4->Layout();
	bSizer3->Fit( m_panel4 );
	m_splitter1->SplitVertically( m_panel3, m_panel4, -1 );
	mainSizer->Add( m_splitter1, 1, wxEXPAND, 5 );


	this->SetSizer( mainSizer );
	this->Layout();
	m_statusBar = this->CreateStatusBar( 2, wxSTB_SIZEGRIP|wxBORDER_SUNKEN, wxID_ANY );
	m_statusBar->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	m_trtimer.SetOwner( this, TRTIMER_ID );
	m_metaTimer.SetOwner( this, wxID_ANY );
	m_aceStartTimer.SetOwner( this, START_TIMER_ID );
	m_queryTimer.SetOwner( this, QUERYTIMER_ID );
	m_moveTimer.SetOwner( this, MOVETIMER_ID );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( MainFrameBase::OnMouseLeaveMiniFrame ) );
	this->Connect( wxEVT_MAXIMIZE, wxMaximizeEventHandler( MainFrameBase::OnMaximizeFrame ) );
	this->Connect( wxEVT_MOVE, wxMoveEventHandler( MainFrameBase::OnMove ) );
	this->Connect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnPaintEvent ) );
	this->Connect( wxEVT_SHOW, wxShowEventHandler( MainFrameBase::OnShowFrame ) );
	this->Connect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnSize ) );
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnLocalListAdd ), this, m_menuAddLocalList->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveList ), this, m_menuSaveList->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveView ), this, m_menuSaveView->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveSubscriptionAsLocalList ), this, m_menuSaveSubscription->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnLocalListClick ), this, m_menuLocalLists->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSubscriptionClick ), this, m_menuSubscriptions->GetId());
	m_menuManage->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnUpdateAllSubscriptions ), this, m_menuUpdateAll->GetId());
	m_menuFile->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnShowPref ), this, m_menuPreferences->GetId());
	m_menuFile->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnExitClick ), this, menuFileExit->GetId());
	m_menuView->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnFullScreen ), this, m_menuFullScreen->GetId());
	m_menuView->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::ToggleChannelList ), this, m_menuChannelList->GetId());
	m_menuView->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMiniWindow ), this, m_menuMiniItem->GetId());
	m_aspectRatio->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnARDefault ), this, m_aspectRatioDefault->GetId());
	m_aspectRatio->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAR169 ), this, m_aspectRatio169->GetId());
	m_aspectRatio->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAR43 ), this, m_aspectRatio43->GetId());
	m_helpMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAbout ), this, m_menuAbout->GetId());
	m_toggleLocalLists->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleLocalLists ), NULL, this );
	m_toggleSubscriptions->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleSubscriptions ), NULL, this );
	m_toggleFavorites->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleFavorites ), NULL, this );
	m_choice1->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( MainFrameBase::OnSubscriptionSelected ), NULL, this );
	m_GroupList->Connect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( MainFrameBase::OnGroupSelect ), NULL, this );
	m_GroupList->Connect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnGroupListPaint ), NULL, this );
	m_grouptbAZ->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleGroupAZ ), NULL, this );
	m_grouptbZA->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleGroupZA ), NULL, this );
	m_groupSearchCtrl->Connect( wxEVT_COMMAND_SEARCHCTRL_SEARCH_BTN, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_groupSearchCtrl->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_groupSearchCtrl->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_ChannelList->Connect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrameBase::OnListPlay ), NULL, this );
	m_ChannelList->Connect( wxEVT_COMMAND_LIST_ITEM_RIGHT_CLICK, wxListEventHandler( MainFrameBase::OnListContextMenu ), NULL, this );
	m_ChannelList->Connect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnChannelListPaint ), NULL, this );
	m_listtbAZ->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleListAZ ), NULL, this );
	m_listtbZA->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleListZA ), NULL, this );
	m_listSearchCtrl->Connect( wxEVT_COMMAND_SEARCHCTRL_SEARCH_BTN, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_listSearchCtrl->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_listSearchCtrl->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_tvView->Connect( wxEVT_CHAR, wxKeyEventHandler( MainFrameBase::KeyPressed ), NULL, this );
	m_tvView->Connect( wxEVT_ERASE_BACKGROUND, wxEraseEventHandler( MainFrameBase::OnEraseBackground ), NULL, this );
	m_tvView->Connect( wxEVT_LEFT_DCLICK, wxMouseEventHandler( MainFrameBase::ToggleFullScreen ), NULL, this );
	m_bpButtonPlay->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPlay ), NULL, this );
	m_bpButtonStop->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStop ), NULL, this );
	m_bpButtonPause->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPause ), NULL, this );
	m_bpButtonRecord->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnRecord ), NULL, this );
	m_movieslider->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( MainFrameBase::OnSeekMouseChanged ), NULL, this );
	m_movieslider->Connect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnSeekUsrChanged ), NULL, this );
	m_bitmap1->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( MainFrameBase::OnMuteClick ), NULL, this );
	m_slider1->Connect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnVolumeChanged ), NULL, this );
	m_bpButtonFullScreen->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnFullScreen ), NULL, this );
	m_bmToggleMini->Connect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleMini ), NULL, this );
	m_statusBar->Connect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnStatusBarPaint ), NULL, this );
	m_statusBar->Connect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnStatusBarSize ), NULL, this );
	this->Connect( TRTIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::HideBar ) );
	this->Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::OnMetaTimer ) );
	this->Connect( START_TIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::StartAcePlaying ) );
	this->Connect( QUERYTIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::OnQueryTimer ) );
	this->Connect( MOVETIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::OnMoveTimer ) );
}

MainFrameBase::~MainFrameBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Disconnect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( MainFrameBase::OnMouseLeaveMiniFrame ) );
	this->Disconnect( wxEVT_MAXIMIZE, wxMaximizeEventHandler( MainFrameBase::OnMaximizeFrame ) );
	this->Disconnect( wxEVT_MOVE, wxMoveEventHandler( MainFrameBase::OnMove ) );
	this->Disconnect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnPaintEvent ) );
	this->Disconnect( wxEVT_SHOW, wxShowEventHandler( MainFrameBase::OnShowFrame ) );
	this->Disconnect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnSize ) );
	m_toggleLocalLists->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleLocalLists ), NULL, this );
	m_toggleSubscriptions->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleSubscriptions ), NULL, this );
	m_toggleFavorites->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleFavorites ), NULL, this );
	m_choice1->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( MainFrameBase::OnSubscriptionSelected ), NULL, this );
	m_GroupList->Disconnect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( MainFrameBase::OnGroupSelect ), NULL, this );
	m_GroupList->Disconnect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnGroupListPaint ), NULL, this );
	m_grouptbAZ->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleGroupAZ ), NULL, this );
	m_grouptbZA->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleGroupZA ), NULL, this );
	m_groupSearchCtrl->Disconnect( wxEVT_COMMAND_SEARCHCTRL_SEARCH_BTN, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_groupSearchCtrl->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_groupSearchCtrl->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_ChannelList->Disconnect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrameBase::OnListPlay ), NULL, this );
	m_ChannelList->Disconnect( wxEVT_COMMAND_LIST_ITEM_RIGHT_CLICK, wxListEventHandler( MainFrameBase::OnListContextMenu ), NULL, this );
	m_ChannelList->Disconnect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnChannelListPaint ), NULL, this );
	m_listtbAZ->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleListAZ ), NULL, this );
	m_listtbZA->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleListZA ), NULL, this );
	m_listSearchCtrl->Disconnect( wxEVT_COMMAND_SEARCHCTRL_SEARCH_BTN, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_listSearchCtrl->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_listSearchCtrl->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( MainFrameBase::OnPopulateChannel ), NULL, this );
	m_tvView->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainFrameBase::KeyPressed ), NULL, this );
	m_tvView->Disconnect( wxEVT_ERASE_BACKGROUND, wxEraseEventHandler( MainFrameBase::OnEraseBackground ), NULL, this );
	m_tvView->Disconnect( wxEVT_LEFT_DCLICK, wxMouseEventHandler( MainFrameBase::ToggleFullScreen ), NULL, this );
	m_bpButtonPlay->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPlay ), NULL, this );
	m_bpButtonStop->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStop ), NULL, this );
	m_bpButtonPause->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnPause ), NULL, this );
	m_bpButtonRecord->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnRecord ), NULL, this );
	m_movieslider->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( MainFrameBase::OnSeekMouseChanged ), NULL, this );
	m_movieslider->Disconnect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnSeekUsrChanged ), NULL, this );
	m_bitmap1->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( MainFrameBase::OnMuteClick ), NULL, this );
	m_slider1->Disconnect( wxEVT_SLIDER, wxCommandEventHandler( MainFrameBase::OnVolumeChanged ), NULL, this );
	m_bpButtonFullScreen->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnFullScreen ), NULL, this );
	m_bmToggleMini->Disconnect( wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnToggleMini ), NULL, this );
	m_statusBar->Disconnect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnStatusBarPaint ), NULL, this );
	m_statusBar->Disconnect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnStatusBarSize ), NULL, this );
	this->Disconnect( TRTIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::HideBar ) );
	this->Disconnect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::OnMetaTimer ) );
	this->Disconnect( START_TIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::StartAcePlaying ) );
	this->Disconnect( QUERYTIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::OnQueryTimer ) );
	this->Disconnect( MOVETIMER_ID, wxEVT_TIMER, wxTimerEventHandler( MainFrameBase::OnMoveTimer ) );

}

ControlsFrameBase::ControlsFrameBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	bSizerControls = new wxBoxSizer( wxHORIZONTAL );


	this->SetSizer( bSizerControls );
	this->Layout();

	this->Centre( wxHORIZONTAL );

	// Connect Events
	this->Connect( wxEVT_ENTER_WINDOW, wxMouseEventHandler( ControlsFrameBase::OnMoveOver ) );
	this->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( ControlsFrameBase::OnLeave ) );
}

ControlsFrameBase::~ControlsFrameBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_ENTER_WINDOW, wxMouseEventHandler( ControlsFrameBase::OnMoveOver ) );
	this->Disconnect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( ControlsFrameBase::OnLeave ) );

}

CRecordChoiceBase::CRecordChoiceBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxVERTICAL );

	wxString m_recordChoiceBoxChoices[] = { _("None"), _("Record"), _("Stream") };
	int m_recordChoiceBoxNChoices = sizeof( m_recordChoiceBoxChoices ) / sizeof( wxString );
	m_recordChoiceBox = new wxRadioBox( this, wxID_ANY, _("Recording Options"), wxDefaultPosition, wxDefaultSize, m_recordChoiceBoxNChoices, m_recordChoiceBoxChoices, 1, wxRA_SPECIFY_COLS|wxBORDER_NONE );
	m_recordChoiceBox->SetSelection( 1 );
	bSizer18->Add( m_recordChoiceBox, 1, wxALIGN_CENTER|wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer18 );
	this->Layout();
	bSizer18->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeave ) );
	m_recordChoiceBox->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeaveRadioBox ), NULL, this );
}

CRecordChoiceBase::~CRecordChoiceBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeave ) );
	m_recordChoiceBox->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CRecordChoiceBase::OnLeaveRadioBox ), NULL, this );

}

CLocalListDialogBase::CLocalListDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	m_LocalDataList = new wxListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( 300,400 ), wxLC_NO_HEADER|wxLC_REPORT|wxLC_SINGLE_SEL );
	bSizer11->Add( m_LocalDataList, 0, wxALL, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxVERTICAL );

	m_newButton = new wxButton( this, wxID_ANY, _("New"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_newButton, 0, wxALL, 5 );

	m_editButton = new wxButton( this, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_editButton, 0, wxALL, 5 );

	m_deleteButton = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer12->Add( m_deleteButton, 0, wxALL, 5 );


	bSizer11->Add( bSizer12, 1, wxEXPAND, 5 );


	bSizer10->Add( bSizer11, 1, wxEXPAND, 5 );

	m_closeButton = new wxButton( this, wxID_OK, _("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer10->Add( m_closeButton, 0, wxALIGN_RIGHT|wxALL, 5 );


	this->SetSizer( bSizer10 );
	this->Layout();
	bSizer10->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_newButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnNewClick ), NULL, this );
	m_editButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnEditClick ), NULL, this );
	m_deleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnDeleteClick ), NULL, this );
}

CLocalListDialogBase::~CLocalListDialogBase()
{
	// Disconnect Events
	m_newButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnNewClick ), NULL, this );
	m_editButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnEditClick ), NULL, this );
	m_deleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalListDialogBase::OnDeleteClick ), NULL, this );

}

CSubscriptionListDialogBase::CSubscriptionListDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );

	m_SubscriptionDataList = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( 600,400 ), 0 );
	bSizer6->Add( m_SubscriptionDataList, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxVERTICAL );

	m_newButton = new wxButton( this, wxID_ANY, _("New URL"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_newButton, 0, wxALL|wxEXPAND, 5 );

	m_xtreamButton = new wxButton( this, wxID_ANY, _("New Xtream"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_xtreamButton, 0, wxALL|wxEXPAND, 5 );


	bSizer7->Add( 0, 20, 0, wxALL, 0 );

	m_updateButton = new wxButton( this, wxID_ANY, _("Update"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_updateButton, 0, wxALL|wxEXPAND, 5 );

	m_editButton = new wxButton( this, wxID_ANY, _("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_editButton, 0, wxALL|wxEXPAND, 5 );

	m_deleteButton = new wxButton( this, wxID_ANY, _("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_deleteButton, 0, wxALL|wxEXPAND, 5 );


	bSizer7->Add( 0, 20, 0, wxEXPAND, 5 );

	m_updateAllButton = new wxButton( this, wxID_ANY, _("Update All"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_updateAllButton, 0, wxALL|wxEXPAND, 5 );


	bSizer6->Add( bSizer7, 0, wxEXPAND, 5 );


	bSizer5->Add( bSizer6, 1, wxEXPAND, 5 );

	m_close = new wxButton( this, wxID_CANCEL, _("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer5->Add( m_close, 0, wxALIGN_RIGHT|wxALL, 5 );


	this->SetSizer( bSizer5 );
	this->Layout();
	bSizer5->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_newButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnNewClicked ), NULL, this );
	m_xtreamButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnXtreamButtonClicked ), NULL, this );
	m_updateButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnUpdateButtonClick ), NULL, this );
	m_editButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnEditClicked ), NULL, this );
	m_deleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnDeleteClicked ), NULL, this );
	m_updateAllButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnUpdateAll ), NULL, this );
}

CSubscriptionListDialogBase::~CSubscriptionListDialogBase()
{
	// Disconnect Events
	m_newButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnNewClicked ), NULL, this );
	m_xtreamButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnXtreamButtonClicked ), NULL, this );
	m_updateButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnUpdateButtonClick ), NULL, this );
	m_editButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnEditClicked ), NULL, this );
	m_deleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnDeleteClicked ), NULL, this );
	m_updateAllButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionListDialogBase::OnUpdateAll ), NULL, this );

}

CSubscriptionDialogBase::CSubscriptionDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxSize( -1,-1 ) );

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxVERTICAL );

	m_NameLabel = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_NameLabel->Wrap( -1 );
	bSizer8->Add( m_NameLabel, 0, wxALL, 5 );

	m_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer8->Add( m_Name, 0, wxALL|wxEXPAND, 5 );

	m_staticText2 = new wxStaticText( this, wxID_ANY, _("URL"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer8->Add( m_staticText2, 0, wxALL, 5 );

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );

	m_URLText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_URLText, 1, wxALL, 5 );

	m_PathButton = new wxButton( this, wxID_ANY, _(" ... "), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_PathButton, 0, wxALL, 5 );


	bSizer8->Add( bSizer9, 1, wxALL|wxEXPAND, 5 );

	m_sdbSizer2 = new wxStdDialogButtonSizer();
	m_sdbSizer2OK = new wxButton( this, wxID_OK );
	m_sdbSizer2->AddButton( m_sdbSizer2OK );
	m_sdbSizer2Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer2->AddButton( m_sdbSizer2Cancel );
	m_sdbSizer2->Realize();

	bSizer8->Add( m_sdbSizer2, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer8 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_PathButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnBrowseButtonClick ), NULL, this );
	m_sdbSizer2Cancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnCancelButtonClicked ), NULL, this );
	m_sdbSizer2OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnOkButtonClicked ), NULL, this );
}

CSubscriptionDialogBase::~CSubscriptionDialogBase()
{
	// Disconnect Events
	m_PathButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnBrowseButtonClick ), NULL, this );
	m_sdbSizer2Cancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnCancelButtonClicked ), NULL, this );
	m_sdbSizer2OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CSubscriptionDialogBase::OnOkButtonClicked ), NULL, this );

}

CLocalDialogBase::CLocalDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer( wxVERTICAL );

	m_NameLabel = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_NameLabel->Wrap( -1 );
	bSizer13->Add( m_NameLabel, 0, wxALL, 5 );

	m_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_Name, 0, wxALL|wxEXPAND, 5 );

	m_AuthorLabel = new wxStaticText( this, wxID_ANY, _("Author"), wxDefaultPosition, wxDefaultSize, 0 );
	m_AuthorLabel->Wrap( -1 );
	bSizer13->Add( m_AuthorLabel, 0, wxALL, 5 );

	m_Author = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_Author, 0, wxALL|wxEXPAND, 5 );

	m_URLLabel = new wxStaticText( this, wxID_ANY, _("URL"), wxDefaultPosition, wxDefaultSize, 0 );
	m_URLLabel->Wrap( -1 );
	bSizer13->Add( m_URLLabel, 0, wxALL, 5 );

	m_urlText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_urlText, 0, wxALL|wxEXPAND, 5 );

	m_VersionLabel = new wxStaticText( this, wxID_ANY, _("Version"), wxDefaultPosition, wxDefaultSize, 0 );
	m_VersionLabel->Wrap( -1 );
	bSizer13->Add( m_VersionLabel, 0, wxALL, 5 );

	m_versionText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), 0 );
	bSizer13->Add( m_versionText, 0, wxALL|wxEXPAND, 5 );

	m_sdbSizer2 = new wxStdDialogButtonSizer();
	m_sdbSizer2OK = new wxButton( this, wxID_OK );
	m_sdbSizer2->AddButton( m_sdbSizer2OK );
	m_sdbSizer2Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer2->AddButton( m_sdbSizer2Cancel );
	m_sdbSizer2->Realize();

	bSizer13->Add( m_sdbSizer2, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer13 );
	this->Layout();
	bSizer13->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_sdbSizer2OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalDialogBase::OnOkClicked ), NULL, this );
}

CLocalDialogBase::~CLocalDialogBase()
{
	// Disconnect Events
	m_sdbSizer2OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CLocalDialogBase::OnOkClicked ), NULL, this );

}

CChannelEditBase::CChannelEditBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxVERTICAL );

	m_staticText8 = new wxStaticText( this, wxID_ANY, _("Channel Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer14->Add( m_staticText8, 0, wxALL, 5 );

	m_channelName = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 600,-1 ), 0 );
	bSizer14->Add( m_channelName, 0, wxALL|wxEXPAND, 5 );

	m_staticText25 = new wxStaticText( this, wxID_ANY, _("Group"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText25->Wrap( -1 );
	bSizer14->Add( m_staticText25, 0, wxALL, 5 );

	m_GroupBox = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	bSizer14->Add( m_GroupBox, 0, wxALL|wxEXPAND, 5 );

	m_staticText9 = new wxStaticText( this, wxID_ANY, _("Channel Addresses"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	bSizer14->Add( m_staticText9, 0, wxALL, 5 );

	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxHORIZONTAL );

	m_addressList = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( 600,200 ), 0 );
	bSizer15->Add( m_addressList, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxVERTICAL );

	m_addButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_addButton->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	m_addButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-add"), wxART_BUTTON ) );
	bSizer16->Add( m_addButton, 0, wxALL, 5 );

	m_deleteButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_deleteButton->SetBitmap( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	m_deleteButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxT("gtk-remove"), wxART_BUTTON ) );
	bSizer16->Add( m_deleteButton, 0, wxALL, 5 );

	m_upButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_upButton->SetBitmap( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	m_upButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_GO_UP, wxART_BUTTON ) );
	bSizer16->Add( m_upButton, 0, wxALL, 5 );

	m_downButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_downButton->SetBitmap( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	m_downButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_GO_DOWN, wxART_BUTTON ) );
	bSizer16->Add( m_downButton, 0, wxALL, 5 );

	m_vlcoptButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_vlcoptButton->SetBitmap( wxArtProvider::GetBitmap( wxART_NEW, wxART_BUTTON ) );
	m_vlcoptButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_NEW, wxART_BUTTON ) );
	m_vlcoptButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxART_NEW, wxART_BUTTON ) );
	m_vlcoptButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_NEW, wxART_BUTTON ) );
	bSizer16->Add( m_vlcoptButton, 0, wxALL, 5 );


	bSizer15->Add( bSizer16, 0, wxEXPAND, 5 );


	bSizer14->Add( bSizer15, 1, wxEXPAND, 5 );

	m_sdbSizer3 = new wxStdDialogButtonSizer();
	m_sdbSizer3OK = new wxButton( this, wxID_OK );
	m_sdbSizer3->AddButton( m_sdbSizer3OK );
	m_sdbSizer3Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer3->AddButton( m_sdbSizer3Cancel );
	m_sdbSizer3->Realize();

	bSizer14->Add( m_sdbSizer3, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer14 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_addressList->Connect( wxEVT_COMMAND_DATAVIEW_ITEM_EDITING_DONE, wxDataViewEventHandler( CChannelEditBase::OnAddressEditDone ), NULL, this );
	m_addressList->Connect( wxEVT_COMMAND_DATAVIEW_ITEM_EDITING_STARTED, wxDataViewEventHandler( CChannelEditBase::OnAddressEditStarted ), NULL, this );
	m_addButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnAddAddress ), NULL, this );
	m_deleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDeleteAddress ), NULL, this );
	m_upButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnUpClicked ), NULL, this );
	m_downButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDownClicked ), NULL, this );
	m_vlcoptButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnVLCOptClicked ), NULL, this );
	m_sdbSizer3OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnOkClicked ), NULL, this );
}

CChannelEditBase::~CChannelEditBase()
{
	// Disconnect Events
	m_addressList->Disconnect( wxEVT_COMMAND_DATAVIEW_ITEM_EDITING_DONE, wxDataViewEventHandler( CChannelEditBase::OnAddressEditDone ), NULL, this );
	m_addressList->Disconnect( wxEVT_COMMAND_DATAVIEW_ITEM_EDITING_STARTED, wxDataViewEventHandler( CChannelEditBase::OnAddressEditStarted ), NULL, this );
	m_addButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnAddAddress ), NULL, this );
	m_deleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDeleteAddress ), NULL, this );
	m_upButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnUpClicked ), NULL, this );
	m_downButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnDownClicked ), NULL, this );
	m_vlcoptButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnVLCOptClicked ), NULL, this );
	m_sdbSizer3OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelEditBase::OnOkClicked ), NULL, this );

}

CDlProgressBase::CDlProgressBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxSize( -1,-1 ) );

	bSizer17 = new wxBoxSizer( wxVERTICAL );

	m_urlText = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT|wxST_ELLIPSIZE_END );
	m_urlText->Wrap( -1 );
	bSizer17->Add( m_urlText, 0, wxALIGN_BOTTOM|wxALL|wxEXPAND, 5 );

	m_progressGauge = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxSize( 390,-1 ), wxGA_HORIZONTAL|wxGA_SMOOTH );
	m_progressGauge->SetValue( 0 );
	m_progressGauge->SetMinSize( wxSize( 390,-1 ) );

	bSizer17->Add( m_progressGauge, 0, wxALL|wxEXPAND, 5 );

	m_sdbSizer4 = new wxStdDialogButtonSizer();
	m_sdbSizer4Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer4->AddButton( m_sdbSizer4Cancel );
	m_sdbSizer4->Realize();

	bSizer17->Add( m_sdbSizer4, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer17 );
	this->Layout();
	bSizer17->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( CDlProgressBase::OnCloseClicked ) );
	this->Connect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CDlProgressBase::Init ) );
	this->Connect( wxEVT_SHOW, wxShowEventHandler( CDlProgressBase::OnShow ) );
	m_sdbSizer4Cancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CDlProgressBase::OnCancelClicked ), NULL, this );
}

CDlProgressBase::~CDlProgressBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( CDlProgressBase::OnCloseClicked ) );
	this->Disconnect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( CDlProgressBase::Init ) );
	this->Disconnect( wxEVT_SHOW, wxShowEventHandler( CDlProgressBase::OnShow ) );
	m_sdbSizer4Cancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CDlProgressBase::OnCancelClicked ), NULL, this );

}

CPrefDialogBase::CPrefDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxVERTICAL );

	m_notebook = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_TOP );
	m_genpanel = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText19 = new wxStaticText( m_genpanel, wxID_ANY, _("Streaming port"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_staticText19->Wrap( -1 );
	bSizer26->Add( m_staticText19, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_streamPortSpinCtrl = new wxSpinCtrl( m_genpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1024, 65535, 0 );
	bSizer26->Add( m_streamPortSpinCtrl, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bSizer21->Add( bSizer26, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_staticText20 = new wxStaticText( m_genpanel, wxID_ANY, _("Recording path"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20->Wrap( -1 );
	bSizer21->Add( m_staticText20, 0, wxALL, 5 );

	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxHORIZONTAL );

	m_recordPathTextCtrl = new wxTextCtrl( m_genpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_recordPathTextCtrl, 1, wxALL|wxEXPAND, 5 );

	m_recPathButton = new wxButton( m_genpanel, wxID_ANY, _("..."), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_recPathButton, 0, wxALL, 5 );


	bSizer21->Add( bSizer29, 0, wxEXPAND, 5 );

	m_notifCheck = new wxCheckBox( m_genpanel, wxID_ANY, _("Enable  song change notifications"), wxDefaultPosition, wxDefaultSize, 0 );
	m_notifCheck->SetValue(true);
	bSizer21->Add( m_notifCheck, 0, wxALL, 5 );

	m_updateallcheck = new wxCheckBox( m_genpanel, wxID_ANY, _("Update all subscriptions at program start"), wxDefaultPosition, wxDefaultSize, 0 );
	m_updateallcheck->SetValue(true);
	bSizer21->Add( m_updateallcheck, 0, wxALL, 5 );

	m_playlastcheck = new wxCheckBox( m_genpanel, wxID_ANY, _("Play the last played stream at program start"), wxDefaultPosition, wxDefaultSize, 0 );
	m_playlastcheck->SetValue(true);
	bSizer21->Add( m_playlastcheck, 0, wxALL, 5 );


	m_genpanel->SetSizer( bSizer21 );
	m_genpanel->Layout();
	bSizer21->Fit( m_genpanel );
	m_notebook->AddPage( m_genpanel, _("General"), true );
	m_vlcpanel = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxVERTICAL );

	wxString m_radioBox4Choices[] = { _("None"), _("Any"), _("Custom") };
	int m_radioBox4NChoices = sizeof( m_radioBox4Choices ) / sizeof( wxString );
	m_radioBox4 = new wxRadioBox( m_vlcpanel, wxID_ANY, _("Hardware acceleration type"), wxDefaultPosition, wxDefaultSize, m_radioBox4NChoices, m_radioBox4Choices, 1, wxRA_SPECIFY_ROWS );
	m_radioBox4->SetSelection( 1 );
	bSizer22->Add( m_radioBox4, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText21 = new wxStaticText( m_vlcpanel, wxID_ANY, _("Custom HW acceleration string"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	bSizer28->Add( m_staticText21, 0, wxALL, 5 );

	m_textCtrl13 = new wxTextCtrl( m_vlcpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer28->Add( m_textCtrl13, 0, wxALL, 5 );


	bSizer22->Add( bSizer28, 0, wxEXPAND, 5 );


	m_vlcpanel->SetSizer( bSizer22 );
	m_vlcpanel->Layout();
	bSizer22->Fit( m_vlcpanel );
	m_notebook->AddPage( m_vlcpanel, _("VLC"), false );
	m_acepanel = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );

	m_staticText18 = new wxStaticText( m_acepanel, wxID_ANY, _("Acestream executable path"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	bSizer24->Add( m_staticText18, 0, wxALL, 5 );

	wxBoxSizer* bSizer27;
	bSizer27 = new wxBoxSizer( wxHORIZONTAL );

	m_aceExecPathTextCtrl = new wxTextCtrl( m_acepanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer27->Add( m_aceExecPathTextCtrl, 1, wxALL, 5 );

	m_acePathButton = new wxButton( m_acepanel, wxID_ANY, _("..."), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer27->Add( m_acePathButton, 0, wxALL, 5 );


	bSizer24->Add( bSizer27, 0, wxEXPAND, 5 );

	wxString m_CacheTypeRBChoices[] = { _("Disk"), _("Memory") };
	int m_CacheTypeRBNChoices = sizeof( m_CacheTypeRBChoices ) / sizeof( wxString );
	m_CacheTypeRB = new wxRadioBox( m_acepanel, wxID_ANY, _("Cache Type"), wxDefaultPosition, wxDefaultSize, m_CacheTypeRBNChoices, m_CacheTypeRBChoices, 2, wxRA_SPECIFY_COLS );
	m_CacheTypeRB->SetSelection( 0 );
	bSizer24->Add( m_CacheTypeRB, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText16 = new wxStaticText( m_acepanel, wxID_ANY, _("Maximum disk cache size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	bSizer25->Add( m_staticText16, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_aceMaxCacheSizeSpinCtrl = new wxSpinCtrl( m_acepanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 512, 20000, 0 );
	bSizer25->Add( m_aceMaxCacheSizeSpinCtrl, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_staticText17 = new wxStaticText( m_acepanel, wxID_ANY, _("MB"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	bSizer25->Add( m_staticText17, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	bSizer24->Add( bSizer25, 0, wxEXPAND, 5 );

	m_staticText201 = new wxStaticText( m_acepanel, wxID_ANY, _("The settings on this page will only be effective after program restart!"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText201->Wrap( -1 );
	bSizer24->Add( m_staticText201, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5 );


	m_acepanel->SetSizer( bSizer24 );
	m_acepanel->Layout();
	bSizer24->Fit( m_acepanel );
	m_notebook->AddPage( m_acepanel, _("Acestream"), false );
	m_networkPanel = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer38;
	bSizer38 = new wxBoxSizer( wxVERTICAL );

	m_proxyCheck = new wxCheckBox( m_networkPanel, wxID_ANY, _("Use Proxy"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer38->Add( m_proxyCheck, 0, wxALL, 5 );

	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 2, 3, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText26 = new wxStaticText( m_networkPanel, wxID_ANY, _("Type"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26->Wrap( -1 );
	fgSizer1->Add( m_staticText26, 0, wxALL, 5 );

	m_staticText27 = new wxStaticText( m_networkPanel, wxID_ANY, _("Address"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText27->Wrap( -1 );
	fgSizer1->Add( m_staticText27, 0, wxALL, 5 );

	m_staticText28 = new wxStaticText( m_networkPanel, wxID_ANY, _("Port"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28->Wrap( -1 );
	fgSizer1->Add( m_staticText28, 0, wxALL, 5 );

	wxString m_proxyTypeChoiceChoices[] = { _("HTTP"), _("SOCKS") };
	int m_proxyTypeChoiceNChoices = sizeof( m_proxyTypeChoiceChoices ) / sizeof( wxString );
	m_proxyTypeChoice = new wxChoice( m_networkPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_proxyTypeChoiceNChoices, m_proxyTypeChoiceChoices, 0 );
	m_proxyTypeChoice->SetSelection( 0 );
	fgSizer1->Add( m_proxyTypeChoice, 0, wxALL, 5 );

	m_addressText = new wxTextCtrl( m_networkPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 240,-1 ), 0 );
	fgSizer1->Add( m_addressText, 0, wxALL, 5 );

	m_portText = new wxTextCtrl( m_networkPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 80,-1 ), 0 );
	fgSizer1->Add( m_portText, 0, wxALL, 5 );


	bSizer38->Add( fgSizer1, 0, wxEXPAND, 5 );

	m_authCheck = new wxCheckBox( m_networkPanel, wxID_ANY, _("Use Authentication"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer38->Add( m_authCheck, 0, wxALL, 5 );

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 2, 2, 0, 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText30 = new wxStaticText( m_networkPanel, wxID_ANY, _("Username"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText30->Wrap( -1 );
	fgSizer2->Add( m_staticText30, 0, wxALL, 5 );

	m_userText = new wxTextCtrl( m_networkPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), 0 );
	fgSizer2->Add( m_userText, 0, wxALL, 5 );

	m_staticText31 = new wxStaticText( m_networkPanel, wxID_ANY, _("Password"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	fgSizer2->Add( m_staticText31, 0, wxALL, 5 );

	m_passwordText = new wxTextCtrl( m_networkPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), wxTE_PASSWORD );
	fgSizer2->Add( m_passwordText, 0, wxALL, 5 );


	bSizer38->Add( fgSizer2, 1, wxEXPAND, 5 );


	m_networkPanel->SetSizer( bSizer38 );
	m_networkPanel->Layout();
	bSizer38->Fit( m_networkPanel );
	m_notebook->AddPage( m_networkPanel, _("Network"), false );

	bSizer20->Add( m_notebook, 1, wxEXPAND | wxALL, 5 );

	m_sdbSizer5 = new wxStdDialogButtonSizer();
	m_sdbSizer5OK = new wxButton( this, wxID_OK );
	m_sdbSizer5->AddButton( m_sdbSizer5OK );
	m_sdbSizer5Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer5->AddButton( m_sdbSizer5Cancel );
	m_sdbSizer5->Realize();

	bSizer20->Add( m_sdbSizer5, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer20 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_recPathButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnRecordingPathClick ), NULL, this );
	m_radioBox4->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( CPrefDialogBase::OnHwAccType ), NULL, this );
	m_acePathButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnAcestreamExecPathClick ), NULL, this );
	m_proxyCheck->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnProxyUseClick ), NULL, this );
	m_authCheck->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnProxyAuthCheck ), NULL, this );
	m_sdbSizer5OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnOkClicked ), NULL, this );
}

CPrefDialogBase::~CPrefDialogBase()
{
	// Disconnect Events
	m_recPathButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnRecordingPathClick ), NULL, this );
	m_radioBox4->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( CPrefDialogBase::OnHwAccType ), NULL, this );
	m_acePathButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnAcestreamExecPathClick ), NULL, this );
	m_proxyCheck->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnProxyUseClick ), NULL, this );
	m_authCheck->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnProxyAuthCheck ), NULL, this );
	m_sdbSizer5OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CPrefDialogBase::OnOkClicked ), NULL, this );

}

CXtreamDlgBase::CXtreamDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxVERTICAL );

	m_staticText24 = new wxStaticText( this, wxID_ANY, _("Subscription Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText24->Wrap( -1 );
	bSizer29->Add( m_staticText24, 0, wxALL, 5 );

	m_NameText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_NameText, 0, wxALL|wxEXPAND, 5 );

	m_HostPortLabel = new wxStaticText( this, wxID_ANY, _("Host and port (ex. http://host.xyz:port)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_HostPortLabel->Wrap( -1 );
	bSizer29->Add( m_HostPortLabel, 0, wxALL|wxEXPAND, 5 );

	m_HostPortText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_HostPortText, 0, wxALL|wxEXPAND, 5 );

	m_usernameLabel = new wxStaticText( this, wxID_ANY, _("Username"), wxDefaultPosition, wxDefaultSize, 0 );
	m_usernameLabel->Wrap( -1 );
	bSizer29->Add( m_usernameLabel, 0, wxALL, 5 );

	m_usernameText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_usernameText, 0, wxALL|wxEXPAND, 5 );

	m_passwordLabel = new wxStaticText( this, wxID_ANY, _("Password"), wxDefaultPosition, wxDefaultSize, 0 );
	m_passwordLabel->Wrap( -1 );
	bSizer29->Add( m_passwordLabel, 0, wxALL, 5 );

	m_passwordText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_passwordText, 0, wxALL|wxEXPAND, 5 );

	wxString m_typeRadioBoxChoices[] = { _("m3u_plus"), _("m3u") };
	int m_typeRadioBoxNChoices = sizeof( m_typeRadioBoxChoices ) / sizeof( wxString );
	m_typeRadioBox = new wxRadioBox( this, wxID_ANY, _("Playlist Type"), wxDefaultPosition, wxDefaultSize, m_typeRadioBoxNChoices, m_typeRadioBoxChoices, 1, wxRA_SPECIFY_ROWS );
	m_typeRadioBox->SetSelection( 0 );
	bSizer29->Add( m_typeRadioBox, 0, wxALL|wxEXPAND, 5 );

	wxString m_outputRadioBoxChoices[] = { _("mpeg-ts"), _("hls"), _("rtmp") };
	int m_outputRadioBoxNChoices = sizeof( m_outputRadioBoxChoices ) / sizeof( wxString );
	m_outputRadioBox = new wxRadioBox( this, wxID_ANY, _("Output Type"), wxDefaultPosition, wxDefaultSize, m_outputRadioBoxNChoices, m_outputRadioBoxChoices, 1, wxRA_SPECIFY_ROWS );
	m_outputRadioBox->SetSelection( 0 );
	bSizer29->Add( m_outputRadioBox, 0, wxALL, 5 );

	m_sdbSizer6 = new wxStdDialogButtonSizer();
	m_sdbSizer6OK = new wxButton( this, wxID_OK );
	m_sdbSizer6->AddButton( m_sdbSizer6OK );
	m_sdbSizer6Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer6->AddButton( m_sdbSizer6Cancel );
	m_sdbSizer6->Realize();

	bSizer29->Add( m_sdbSizer6, 1, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer29 );
	this->Layout();
	bSizer29->Fit( this );

	this->Centre( wxBOTH );
}

CXtreamDlgBase::~CXtreamDlgBase()
{
}

CChannelInfoBase::CChannelInfoBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxVERTICAL );

	m_InfoTextCtrl = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 600,300 ), wxHSCROLL|wxTE_AUTO_URL|wxTE_DONTWRAP|wxTE_MULTILINE|wxTE_READONLY|wxTE_RICH );
	bSizer32->Add( m_InfoTextCtrl, 1, wxALL|wxEXPAND, 5 );

	m_sdbSizer7 = new wxStdDialogButtonSizer();
	m_sdbSizer7OK = new wxButton( this, wxID_OK );
	m_sdbSizer7->AddButton( m_sdbSizer7OK );
	m_sdbSizer7->Realize();

	bSizer32->Add( m_sdbSizer7, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer32 );
	this->Layout();
	bSizer32->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_sdbSizer7OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelInfoBase::OnOkClicked ), NULL, this );
}

CChannelInfoBase::~CChannelInfoBase()
{
	// Disconnect Events
	m_sdbSizer7OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CChannelInfoBase::OnOkClicked ), NULL, this );

}

CVLCOptDlgBase::CVLCOptDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer35;
	bSizer35 = new wxBoxSizer( wxHORIZONTAL );

	m_optList = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( 350,200 ), 0 );
	m_optList->SetMinSize( wxSize( 300,150 ) );

	bSizer35->Add( m_optList, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer36;
	bSizer36 = new wxBoxSizer( wxVERTICAL );

	m_addButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_addButton->SetBitmap( wxArtProvider::GetBitmap( wxART_PLUS, wxART_BUTTON ) );
	m_addButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_PLUS, wxART_BUTTON ) );
	m_addButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxART_PLUS, wxART_BUTTON ) );
	m_addButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxART_PLUS, wxART_BUTTON ) );
	m_addButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_PLUS, wxART_BUTTON ) );
	bSizer36->Add( m_addButton, 0, wxALL, 5 );

	m_delButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_delButton->SetBitmap( wxArtProvider::GetBitmap( wxART_MINUS, wxART_BUTTON ) );
	m_delButton->SetBitmapDisabled( wxArtProvider::GetBitmap( wxART_MINUS, wxART_BUTTON ) );
	m_delButton->SetBitmapPressed( wxArtProvider::GetBitmap( wxART_MINUS, wxART_BUTTON ) );
	m_delButton->SetBitmapFocus( wxArtProvider::GetBitmap( wxART_MINUS, wxART_BUTTON ) );
	m_delButton->SetBitmapCurrent( wxArtProvider::GetBitmap( wxART_MINUS, wxART_BUTTON ) );
	bSizer36->Add( m_delButton, 0, wxALL, 5 );


	bSizer35->Add( bSizer36, 0, wxEXPAND, 5 );


	bSizer33->Add( bSizer35, 1, wxEXPAND, 5 );

	m_sdbSizer8 = new wxStdDialogButtonSizer();
	m_sdbSizer8OK = new wxButton( this, wxID_OK );
	m_sdbSizer8->AddButton( m_sdbSizer8OK );
	m_sdbSizer8Cancel = new wxButton( this, wxID_CANCEL );
	m_sdbSizer8->AddButton( m_sdbSizer8Cancel );
	m_sdbSizer8->Realize();

	bSizer33->Add( m_sdbSizer8, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer33 );
	this->Layout();
	bSizer33->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	m_addButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CVLCOptDlgBase::OnPlusClicked ), NULL, this );
	m_delButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CVLCOptDlgBase::OnMinusClicked ), NULL, this );
	m_sdbSizer8OK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CVLCOptDlgBase::OnOkClicked ), NULL, this );
}

CVLCOptDlgBase::~CVLCOptDlgBase()
{
	// Disconnect Events
	m_addButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CVLCOptDlgBase::OnPlusClicked ), NULL, this );
	m_delButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CVLCOptDlgBase::OnMinusClicked ), NULL, this );
	m_sdbSizer8OK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CVLCOptDlgBase::OnOkClicked ), NULL, this );

}

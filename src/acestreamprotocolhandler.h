#ifndef CACESTREAMPROTOCOLHANDLER_H
#define CACESTREAMPROTOCOLHANDLER_H

#include "baseprotocolhandler.h"
#include "downloadclient.h"

namespace tvlite
{

class CAcestreamProtocolHandler : public CBaseProtocolHandler
{
private:
   CAcestreamProtocolHandler(const CAcestreamProtocolHandler& rhs);
   CAcestreamProtocolHandler& operator=(const CAcestreamProtocolHandler& rhs);
   CAcestreamProtocolHandler();
   int InitURLs();
   int SendStop();
   wxString m_transformed_url;
   wxString m_streamurl;
   wxString m_commandurl;
   CDownloadClient *m_initdlClient;
   CDownloadClient *m_stopdlClient;
   bool m_isLocked;
   wxTimer m_stoptimer;
   void StopPlayer();
   static int m_instanceNum;
   wxString m_InitURLFileName;


public:
   CAcestreamProtocolHandler(wxEvtHandler *parent, wxString url, wxString name, wxArrayString vlcoptions);
   virtual ~CAcestreamProtocolHandler();
   virtual void Start() ;
   virtual void Stop() ;
   wxString GetStreamURL();
   wxString GetCommandURL();
   void OnInitURLsResponse(wxThreadEvent &evt);
   void OnStopResponse(wxThreadEvent &evt);
   void OnTimer(wxTimerEvent &evt);


};

}

#endif // CACESTREAMPROTOCOLHANDLER_H

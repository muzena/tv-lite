#include "vlcoptdlg.h"
#include <wx/arrstr.h>
#include <wx/msgdlg.h>

CVLCOptDlg::CVLCOptDlg(wxWindow *parent):CVLCOptDlgBase(parent)
{
   int width, height;
   m_optList->GetSize(&width, &height);
   m_optList->AppendTextColumn(_("Option"), wxDATAVIEW_CELL_EDITABLE, width);
}

CVLCOptDlg::~CVLCOptDlg()
{
}


void CVLCOptDlg::GetData(wxArrayString& options)
{
   options.Clear();
   for (int i = 0; i < m_optList->GetItemCount(); i++)
   {
      wxString optString = m_optList->GetTextValue(i,0).Trim().Trim(false);
      if (optString!= "" && optString != _("Click here to add option..."))
      {
         options.Add(optString);
      }
   }

}

void CVLCOptDlg::SetData(wxArrayString& options)
{
   for (size_t i = 0; i < options.GetCount(); i++)
   {
      wxVector<wxVariant> data;
      data.push_back(options[i]);
      m_optList->AppendItem(data);
   }
}


bool CVLCOptDlg::ValidateData()
{
   bool valid = true;
   for (int i = 0; i < m_optList->GetItemCount(); i++)
   {
      wxString optString = m_optList->GetTextValue(i,0).Trim().Trim(false);
      if (optString =="" || optString == _("Click here to add option..."))
      {
         valid = false;
      }
      else
      {
         break;
      }
   }
   if (!valid)
   {
      wxMessageBox(_("Invalid option (blank)"), _("Validation error"), wxOK|wxICON_EXCLAMATION, this);
   }
   return valid;
}


void CVLCOptDlg::OnPlusClicked( wxCommandEvent& event )
{
      wxVector<wxVariant> data;
      data.push_back(_("Click here to add option..."));
      data.push_back("");
      m_optList->AppendItem(data);
      m_optList->SelectRow(m_optList->GetItemCount() - 1);
}

void CVLCOptDlg::OnMinusClicked( wxCommandEvent& event )
{
   if (m_optList->GetSelectedRow() != wxNOT_FOUND)
   {
      if (wxMessageBox(_("Do you really wish to delete the selected option?"),
                           _("Question"),
                             wxYES_NO|wxICON_QUESTION, NULL) == wxYES)
     {
         m_optList->DeleteItem(m_optList->GetSelectedRow());
     }

  }
}

void CVLCOptDlg::OnOkClicked( wxCommandEvent& event )
{
   if (ValidateData())
   {
      EndModal(wxID_OK);
   }
}

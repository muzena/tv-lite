#include "statusgauge.h"

using namespace tvlite;

#include <wx/arrimpl.cpp>
WX_DEFINE_OBJARRAY(TStatusGaugeList);   

TStatusGaugeList CStatusGauge::m_list;

CStatusGauge::CStatusGauge(int id, wxStatusBar* parent, int captionPane):
   m_id(id),
   m_parent(parent),
   m_captionPane(captionPane)
{
   m_gauge = new wxGauge(m_parent, wxID_ANY, 100, wxDefaultPosition, wxDefaultSize, wxGA_HORIZONTAL);
}

CStatusGauge::~CStatusGauge()
{
   if (m_gauge)
   {
      delete m_gauge;
   }
}

void CStatusGauge::CreateStatusGauge(wxStatusBar *parent, int id, int captionPane)
{
   CStatusGauge *pGauge = NULL;
   
   if (parent)
   {
      pGauge = new CStatusGauge(id, parent, captionPane);
   }
   if (pGauge)
   {
      m_list.Add(pGauge);
   }
}

void CStatusGauge::DestroyStatusGauges()
{
   for (size_t i = 0; i < m_list.Count(); i++)
   {
      if (m_list[i]) delete m_list[i];
   }
}

CStatusGauge* CStatusGauge::GetStatusGauge(int id)
{
   CStatusGauge* pRc = NULL;
   for (size_t i = 0; i < m_list.Count(); i++)
   {
      if (m_list[i]->m_id == id)
      {
         pRc = m_list[i];
      }
   }
   return pRc;
} 


void CStatusGauge::ShowStatusGauge(int id, wxString caption, bool visibleGauge /*=true*/)
{
   CStatusGauge* pGauge = GetStatusGauge(id);
   if (pGauge)
   {
      pGauge->m_parent->SetStatusText(caption, pGauge->m_captionPane);
      if (visibleGauge)
      {
         pGauge->m_gauge->Show(true);
         pGauge->m_gauge->Update();
         pGauge->m_gauge->Refresh();
      }
      else
      {
         if (pGauge->m_gauge->IsShown())
         {
            pGauge->m_gauge->Hide();
         }
      }
   }
}

void CStatusGauge::HideStatusGauge(int id)
{
  CStatusGauge* pGauge = GetStatusGauge(id);
  if (pGauge)
   {
      pGauge->m_gauge->Hide();
      pGauge->m_parent->SetStatusText("", pGauge->m_captionPane);
   }
}


void CStatusGauge::RedrawStatusGauges(wxStatusBar *parent)
{
   for (size_t i = 0; i < m_list.Count(); i++)
   {
      wxRect rectangle;
      CStatusGauge* pGauge = m_list[i];
      if (pGauge->m_parent == parent)
      {
         if (pGauge->m_gauge && pGauge->m_parent->GetFieldRect(pGauge->m_captionPane, rectangle))
         {
#ifdef __WXMSW__ 
               rectangle.y += 5;
               rectangle.height = rectangle.height - 10;
#else          
               rectangle.y += 10;
               rectangle.height = rectangle.height - 20;
#endif               
               wxSize captionsize = parent->GetTextExtent(parent->GetStatusText(pGauge->m_captionPane));
               rectangle.x = rectangle.x + captionsize.GetWidth() + 10;
               //rectangle.width = rectangle.width - captionsize.GetWidth() - 40;
               rectangle.width = 150;
                
               pGauge->m_gauge->SetSize(rectangle);
            
         }
      }
   }
}


void CStatusGauge::UpdateGauge(int id, int data)
{
   
   CStatusGauge* pGauge = GetStatusGauge(id);
   if (pGauge && pGauge->m_gauge)
   {
   
      if (data < 0 || data > 100)
      {
          pGauge->m_gauge->Pulse();
      }
      else
      {
          pGauge->m_gauge->SetValue(data);
      }
   }
}


bool CStatusGauge::IsVisible(int id)
{
   bool rv = false;
   CStatusGauge* pGauge = GetStatusGauge(id);
   if (pGauge && pGauge->m_gauge)
   {
      rv = pGauge->m_gauge->IsShown();
   }
   return rv;
}
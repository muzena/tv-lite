#ifndef CSUBSCRIPTION_H
#define CSUBSCRIPTION_H
#ifdef __WXMSW__
    #include <windows.h>
    #include <rpc.h>
    #include "wx/msw/ole/uuid.h"
#else
    #include <uuid/uuid.h>
    typedef unsigned char uuid_string_t[UUID_STR_LEN];
#endif
#include <wx/uri.h>
#include <wx/event.h>
#include "subscriptioninfo.h"
#include "channel.h"
#include "database.h"
#include "downloadclient.h"

DECLARE_EVENT_TYPE(DTREADNOTIFY_EVT, -1)
DECLARE_EVENT_TYPE(DTPARSEDNOTIFY_EVT, -1)
namespace tvlite
{
class CSubscription;

WX_DEFINE_ARRAY(CSubscription*, TSubscriptionList);


class CSubscription : public wxEvtHandler
{
   public:
    CSubscription(const CSubscription& rhs) = delete;
    CSubscription operator=(const CSubscription& rhs) = delete;
   CSubscription();
   CSubscription(wxString uri);
   virtual ~CSubscription();
   int ReadData(TChannelList *channelList);
   void SetChannelList(TChannelList *chanList);
   CSubscriptionInfo* GetSubscriptionInfo();
   void SetSubscriptionInfo(CSubscriptionInfo subscriptionInfo);
   static void GetCachedData(TSubscriptionList &subList);
   static wxString FindCachedData(wxString url);
   int SaveDataToCache();
   int SaveDataToCache(wxString &idStr);
   int SaveDataToListDir();
   int SaveDataToListDir(wxString &idStr);
   int SaveData(wxString path, bool keepuri = false);
   wxString GetURI();
   void SetURI(wxString uri);
   unsigned int GetNumberOfChannels();
   unsigned int GetNumberOfSources();
   static int Compare( CSubscription **item1, CSubscription **item2);
   void GetGroups(wxArrayString& grouparray);
   static int GetDBInfo(wxString dbFile, CSubscriptionInfo *rh);
   CDownloadClient* GetDLClient();
   void SetEventHandler(wxEvtHandler *evtHandler);
   int ReadDataFromCache(TChannelList *channelList);

protected:
   int  ParseData(TChannelList *channelList, wxString dbFile);
   int  ParseDbData(TChannelList *channelList, wxString dbFile);
   int  ParseM3UData(TChannelList *channelList, wxString dbFile, const wxMBConv &conv);
   bool ParseM3ULine(wxString &line, wxString &name, wxString &group);
   bool TestM3U(const char* testbuffer);
   void OnReadDataResponse (wxThreadEvent &event);
   int  CacheNewData(TChannelList *channelList, wxString importfile);
   int  ReadDataFromFile(TChannelList *channelList, wxString importfile);
   wxString GenUUID();
   wxEvtHandler* m_evtHandler;
   CSubscriptionInfo m_subscriptionInfo;
   TChannelList *m_chanlist;
   wxString m_uri;
   wxString m_cachedFile, m_importFile;
   wxString GenerateTempFileName();
   CDownloadClient *m_dlClient;

};

typedef  void (*TFillCallback)(int data) ;

} //namespace

#endif // CSUBSCRIPTION_H


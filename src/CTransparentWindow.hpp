#ifndef CTRANSPARENTWINDOW_HPP
#define CTRANSPARENTWINDOW_HPP
#include <wx/wx.h>
#include <wx/window.h>

namespace tvlite
{

class CTransparentWindow : public wxWindow
{
public:
    CTransparentWindow();
    virtual ~CTransparentWindow();

};

}

#endif // CTRANSPARENTWINDOW_HPP

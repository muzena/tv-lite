#ifndef CSUBSCRIPTIONDIALOG_H
#define CSUBSCRIPTIONDIALOG_H

#include <wx/wx.h>
#include <wx/window.h> 

#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif

#include "main.h"
#include "subscription.h"

namespace tvlite
{
class CSubscriptionDialog : public CSubscriptionDialogBase
{
private:
   CSubscriptionDialog(const CSubscriptionDialog& rhs) = delete;
   CSubscriptionDialog& operator=(const CSubscriptionDialog& rhs) = delete;
   wxString m_subscriptionName;
   wxString m_subscriptionURL;

public:
   CSubscriptionDialog() = delete;
   CSubscriptionDialog(wxWindow *parent);
   void SetNameField(wxString param);
   const wxString GetNameField() const;
   void SetURLField(wxString param);
   const wxString GetURLField() const;
   void SetData(CSubscriptionInfo *info);
   void GetData(CSubscriptionInfo *info);
   
   virtual int ShowModal();
   int ShowURLOnly();
   virtual void OnCancelButtonClicked( wxCommandEvent& event );
   virtual void OnOkButtonClicked( wxCommandEvent& event ); 
   virtual void OnBrowseButtonClick(wxCommandEvent& event);
   virtual ~CSubscriptionDialog();

};

} //namespace
#endif // CSUBSCRIPTIONDIALOG_H

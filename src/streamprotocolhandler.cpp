#include "streamprotocolhandler.h"
#include "debug.h"

using namespace tvlite;

CStreamProtocolHandler::CStreamProtocolHandler(wxEvtHandler *parent, wxString url, wxString name, wxArrayString vlcoptions) :
  CBaseProtocolHandler(parent, url, name, vlcoptions)

{

}

CStreamProtocolHandler::~CStreamProtocolHandler()
{
   DBG_INFO("Stream Protocol Handler -> delete")
}

void CStreamProtocolHandler::Start()
{
   m_vlcPlayer->Play(m_url, m_vlcoptions);

}

void CStreamProtocolHandler::Stop()
{
   m_vlcPlayer->Stop();
   m_stopped = true;
   OnStopAsync();
}

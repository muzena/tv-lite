#ifndef CM3UFILE_H
#define CM3UFILE_H

#include <wx/string.h>
#include <wx/file.h>

#define M3U_BUFFER_SIZE 1024

namespace tvlite
{

class CM3UFile : public wxFile
{
private:
   CM3UFile(const CM3UFile&) = delete;
   CM3UFile& operator=(const CM3UFile&) = delete;
   CM3UFile() = delete;
   bool ReadFromBuffer(wxString &s);
   char *m_buffer;
   wxString m_string;
   const wxMBConv &m_conv;
   wxString m_sep;
   bool m_isInitialized;


public:

   CM3UFile(	const wxString &filename,
                     const wxString &sep = " \t",
                     const wxMBConv &conv = wxConvAuto()
                     /*wxFile::OpenMode 	mode = wxFile::read*/
                  );
   bool Init();
   bool ReadLine(wxString &line);

   virtual ~CM3UFile();

};

}

#endif // CM3UFILE_H

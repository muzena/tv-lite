#ifndef CPROXY_H
#define CPROXY_H

#include "configuration.h"

namespace tvlite
{
class CProxy
{
private:
   eProxyType m_proxyType;
   bool m_isProxyAuthUsed;
   wxString m_URL;
   wxString m_user;
   wxString m_password;
   CProxy() {};
public:
   CProxy(eProxyType type, bool isProxyAuthUsed, wxString url, wxString username = "", wxString password = "");
   virtual ~CProxy();
   bool IsProxyAuthUsed();
   eProxyType GetProxyType();
   wxString GetVlcURL();
   wxString GetCurlURL();
   wxString GetProxyUserName();
   wxString GetProxyPassword();
   static CProxy *GetConfiguredProxy();
};
}
#endif // CPROXY_H

#include "textwrapper.h"

tvlite::CTextWrapper::CTextWrapper(wxWindow *win, int widthMax):m_win(win), m_widthMax(widthMax)
{
}

tvlite::CTextWrapper::~CTextWrapper()
{
}

void tvlite::CTextWrapper::OnOutputLine(const wxString& line)
{
   m_wrappedText += line;
}

void tvlite::CTextWrapper::OnNewLine()
{
   m_wrappedText += '\n';
}

wxString tvlite::CTextWrapper::Wrap(wxString &inputText)
{
   wxTextWrapper::Wrap(m_win, inputText, m_widthMax);
   return m_wrappedText;
}
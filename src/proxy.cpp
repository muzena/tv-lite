#include "proxy.h"

using namespace tvlite;

CProxy::CProxy(eProxyType type, bool isProxyAuthUsed, wxString url, wxString username /* = "" */, wxString password /* = "" */):
m_proxyType(type),
m_isProxyAuthUsed(isProxyAuthUsed),
m_URL(url),
m_user(username),
m_password(password)
{
}

CProxy::~CProxy()
{
}


eProxyType CProxy::GetProxyType()
{
   return m_proxyType;
}

bool CProxy::IsProxyAuthUsed()
{
   return m_isProxyAuthUsed;
}

wxString CProxy::GetProxyUserName()
{
   return m_user;
}

wxString CProxy::GetProxyPassword()
{
   return m_password;
}

wxString CProxy::GetVlcURL()
{
   wxString result;
  
   if (m_proxyType == E_PROXY_HTTP)
   {
      result = "http://";
      if (m_isProxyAuthUsed)
      {
         result = result + m_user + "@";
      }
      result = result + m_URL;
   }
   else if (m_proxyType == E_PROXY_SOCKS)
   {
      result = m_URL;
   }
  return result;
} 

wxString CProxy::GetCurlURL()
{
   wxString result;
   
   if (m_proxyType == E_PROXY_HTTP)
   {
      result  = "http://";
   }
   if (m_proxyType == E_PROXY_SOCKS)
   {
      result = "socks5://";
   }
   if (m_isProxyAuthUsed)
      {
         result = result + m_user;
         if (m_password != "")
         {
            result = result + ":" + m_password;
         }
         result = result +"@";
      }
   result = result + m_URL;
   return result;
}


CProxy* CProxy::GetConfiguredProxy()
{
   
   CProxy *result = nullptr;
   if (NETWORKCONFIG.GetProxyUsed())
   {
      wxString url ;
      url << NETWORKCONFIG.GetProxyAddress() << ":" << NETWORKCONFIG.GetProxyPort(); 
      result = new CProxy(NETWORKCONFIG.GetProxyType(), NETWORKCONFIG.GetProxyAuthUsed(),
                          url, NETWORKCONFIG.GetProxyUserName(),
                          NETWORKCONFIG.GetProxyPassword());
                          
   }
   return result;
}
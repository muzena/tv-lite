#ifndef CSOPTHREAD_H
#define CSOPTHREAD_H

#include <wx/thread.h>
#include "sopprotocol.h"

namespace tvlite
{

class CSopProtocol;
   
class CSopThread : public wxThread
{
    public:
    CSopThread(wxEvtHandler *handler, CSopProtocol *sopProcess); 
    virtual ~CSopThread();
    void SetRunning(bool run);
    bool GetRunning();

    // thread execution starts here
    virtual ExitCode Entry();
    
    protected:
    wxEvtHandler *m_sophandler;
    CSopProtocol  *m_sopprocess;
    bool m_running;
    wxMutex m_access;
   
};


}

#endif // CSOPTHREAD_H

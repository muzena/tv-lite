#ifndef CBASEPROTOCOLHANDLER_H
#define CBASEPROTOCOLHANDLER_H

#include <wx/event.h>
#include "vlcplayer.h"

DECLARE_EVENT_TYPE(vlcEVT_PLAY, -1)
DECLARE_EVENT_TYPE(protEVT_EXIT, -1)

namespace tvlite
{

class CBaseProtocolHandler : public wxEvtHandler
{
protected:
   CBaseProtocolHandler(const CBaseProtocolHandler& rhs) = delete;
   CBaseProtocolHandler& operator=(const CBaseProtocolHandler& rhs)= delete;
   CVlcPlayer *m_vlcPlayer;
   wxEvtHandler *m_pparent;
   wxString m_url;
   wxString m_name;
   wxArrayString m_vlcoptions;
   bool m_stopped;
   int m_type;
public:
   CBaseProtocolHandler() = delete;
   CBaseProtocolHandler(wxEvtHandler *parent);
   CBaseProtocolHandler(wxEvtHandler *parent, wxString url, wxString name, wxArrayString vlcoptions);
   void SetVLCPlayer(CVlcPlayer *m_vlcPlayer);
   CVlcPlayer * GetVLCPlayer();
   virtual ~CBaseProtocolHandler();
   virtual void Start() = 0 ;
   virtual void Stop()  = 0 ;
   virtual void OnStopAsync();

   void SetURL(wxString url);
   wxString GetURL();

   void SetName(wxString name);
   wxString GetName();

   void SetVLCOptions(wxArrayString vlcoptions);
   wxArrayString GetVLCOptions();

   bool WasStopped();
};

}

#endif // CBASEPROTOCOLHANDLER_H

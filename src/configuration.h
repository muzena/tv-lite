#ifndef CCONFIGURATION_H
#define CCONFIGURATION_H

#include <wx/string.h>
#include <wx/fileconf.h>

#define TVLITECONFIG   wxConfigBase::Get()
#define GENERALCONFIG  CConfiguration::Instance()->GetGeneralConfiguration()
#define VLCCONFIG      CConfiguration::Instance()->GetVLCConfiguration()
#define ACESTREAMCONFIG CConfiguration::Instance()->GetAceConfiguration()
#define NETWORKCONFIG   CConfiguration::Instance()->GetNetworkConfiguration()

#define GC_STREAM_PORT_KEY "GeneralConfiguration/StreamPort"
#define GC_REC_DIR_KEY     "GeneralConfiguration/RecordDir"
#define GC_NOTIF_KEY       "GeneralConfiguration/EnabledNotifications"
#define GC_UPDATEALL_KEY   "GeneralConfiguration/UpdateAllAtProgramStart"
#define GC_PLAY_LAST_KEY   "GeneralConfiguration/PlayLastStreamAtProgramStart"
#define VC_RENDER_KEY      "VLCConfiguration/RenderType"
#define AC_CACHE_SIZE_KEY  "AceConfiguration/DiskCacheSize"
#define AC_FULL_PATH_KEY   "AceConfiguration/AceFullPath"
#define AC_CACHE_TYPE_KEY  "AceConfiguration/AceCacheType"
#define NET_PROXY_USED_KEY "NetworkConfiguration/ProxyUsed"
#define NET_PROXY_TYPE_KEY "NetworkConfiguration/ProxyType"
#define NET_PROXY_URL_KEY  "NetworkConfiguration/ProxyURL"
#define NET_PROXY_PORT_KEY "NetworkConfiguration/ProxyPort"
#define NET_PROXY_AUTH_USED_KEY "NetworkConfiguration/ProxyAuthUsed"
#define NET_PROXY_USER     "NetworkConfiguration/ProxyUser"
#define NET_PROXY_PASS     "NetworkConfiguration/ProxyPass"


namespace tvlite
{
enum  eAceCacheType
{
   E_CACHE_DISK,
   E_CACHE_MEM
};

enum eProxyType
{
   E_PROXY_HTTP,
   E_PROXY_SOCKS,
};

struct TGeneralConfiguration
{
   wxString m_recordDir;
   unsigned int m_streamPort;
   bool m_notif;
   bool m_updateall;
   bool m_playlast;
   void Load();
   void Save();
   void SetRecordDir(wxString path);
   wxString GetRecordDir();
   void SetStreamPort(unsigned int port);
   unsigned int GetStreamPort();
   wxString GetDefaultRecordDir();
   void SetNotifEnabled(bool enabled);
   bool GetNotifEnabled();
   void SetUpdateAllEnabled(bool enabled);
   bool GetUpdateAllEnabled();
   void SetPlayLastStreamEnabled(bool enabled);
   bool GetPlayLastStreamEnabled();
};

struct TVLCConfiguration
{
   wxString m_render;
   void Load();
   void Save();
   void SetRender(wxString render);
   wxString GetRender();
};

struct TAceConfiguration
{
   wxString m_acePath;
   unsigned long m_diskCacheSize;
   eAceCacheType m_cacheType;
   wxString GetDefaultAcePath();
   void Load();
   void Save();
   void SetFullPath(wxString path);
   wxString GetFullPath();
   void SetCacheSize(unsigned long size);
   unsigned long GetCacheSize();
   eAceCacheType GetCacheType();
   void SetCacheType(eAceCacheType type);
};

struct TNetworkConfiguration
{
   bool m_proxyUsed;
   bool m_proxyAuthUsed;
   eProxyType m_proxyType;
   wxString m_proxyAddress;
   unsigned short m_proxyPort;
   wxString m_proxyUser;
   wxString m_proxyPassword;
   void Load();
   void Save();
   void SetProxyURL(wxString &url);
   wxString GetProxyURL();
   void SetProxyType(eProxyType type);
   eProxyType GetProxyType();
   void SetProxyUsed(bool used);
   bool GetProxyUsed();
   void SetProxyAddress(wxString address);
   wxString GetProxyAddress();
   void SetProxyPort(unsigned short port);
   unsigned short GetProxyPort();
   void SetProxyAuthUsed(bool used);
   bool GetProxyAuthUsed();
   void SetProxyUserName(wxString username);
   wxString GetProxyUserName();
   void SetProxyPassword(wxString password);
   wxString GetProxyPassword();
};

class CConfiguration
{
   static CConfiguration* ms_instance;
   const  wxString configName = "tvlite";

private:
   CConfiguration(const CConfiguration& rhs);
   CConfiguration& operator=(const CConfiguration& rhs);
   bool Init(wxString dir);
   TGeneralConfiguration m_genConfig;
   TVLCConfiguration m_vlcConfig;
   TAceConfiguration m_aceConfig;
   TNetworkConfiguration m_networkConfig;

public:
   static CConfiguration* Instance();
   bool Init();
   void Load();
   void Save();
   static void Release();
   static wxString GetConfigDir();
   static wxString GetConfigFileName();
   static wxString GetTempDir();
   static wxString GetCacheDir();
   static wxString GetListDir();
   static wxString GetFavoritesDir();
   static wxString GetLastPlayedDir();
   static wxString GetFavoritesFileName();
   static wxString GetLastPlayedFileName();
   wxFileConfig * GetProgramConfig();
   TGeneralConfiguration& GetGeneralConfiguration();
   TVLCConfiguration& GetVLCConfiguration();
   TAceConfiguration& GetAceConfiguration();
   TNetworkConfiguration& GetNetworkConfiguration();

private:
   CConfiguration();
   ~CConfiguration();
   wxFileConfig* m_pconfig;
};

} //namespace

#endif // CCONFIGURATION_H

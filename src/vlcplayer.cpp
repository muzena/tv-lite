#include "vlcplayer.h"
#include <wx/window.h>
#include <wx/stdpaths.h>
#include <wx/utils.h>
#include <wx/uri.h>
#include "debug.h"
#include "version.h"
#include "configuration.h"
#include "userdataobject.h"
#include "proxy.h"

using namespace tvlite;

CVlcPlayer::CVlcPlayer(wxEvtHandler *parent) :
   m_vlc_inst(NULL),
   m_media_player(NULL),
   m_vlc_evt_man(NULL),
#ifdef __WXGTK__
   m_embedded(NULL),
#endif
#ifdef __WXMSW__
   m_wnd(NULL),
#endif
   m_wasStopped(false),
   m_rState(rsNone),
   m_volume(80),
   m_pparent(parent)

{
   m_trackMap[etVideo] = -1;
   m_trackMap[etAudio] = -1;
   m_trackMap[etSubtitle] =-1;
}

CVlcPlayer::~CVlcPlayer()
{
   if (m_vlc_inst) libvlc_release(m_vlc_inst);
   if (m_media_player) libvlc_media_player_release(m_media_player);
}


void CVlcPlayer::Init(wxWindow *wnd)
{
    wxString userAgentOption;
    userAgentOption << "--http-user-agent="<< "'" << GetUserAgent() << "'";

    const char* szoptions[3];
    szoptions[0] = "--network-caching=1500";
    szoptions[1] = "--live-caching=1000";
    szoptions[2] = (const char*)userAgentOption.utf8_str();
#ifdef __WXMSW__
    wxStandardPaths stdPath = wxStandardPaths::Get();
    wxString sPluginPath = stdPath.GetPluginsDir() + "\\plugins";
    wxSetEnv("VLC_PLUGIN_PATH",sPluginPath);
    DBG_INFO("plugin path %s", (const char*)sPluginPath.utf8_str());
#endif
    DBG_INFO("VLC options %s, %s, %s", szoptions[0], szoptions[1], szoptions[2]);
    m_vlc_inst = libvlc_new(3, szoptions);
    m_media_player = libvlc_media_player_new(m_vlc_inst);
    m_vlc_evt_man = libvlc_media_player_event_manager(m_media_player);
    #ifdef __WXGTK__
        GtkWidget *gtk_player_widget = static_cast<GtkWidget *>(wnd->GetHandle());
        gtk_widget_realize(gtk_player_widget);
        m_embedded = gtk_widget_get_window(gtk_player_widget);
        libvlc_media_player_set_xwindow(m_media_player, GDK_WINDOW_XID(m_embedded));
    #else
        libvlc_media_player_set_hwnd(m_media_player, wnd->GetHandle());
        m_wnd = wnd;
    #endif
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerBuffering,   VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerPlaying,         VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerPaused,          VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerStopped,         VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerEndReached,      VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerPositionChanged, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerLengthChanged,   VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerEncounteredError,VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerVout,   VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerCorked, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerUncorked, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaParsedChanged, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaMetaChanged,  VlcEvtCallback, this );

    libvlc_event_attach( m_vlc_evt_man, libvlc_VlmMediaInstanceStatusError, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_VlmMediaInstanceStopped, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerEndReached, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerSeekableChanged, VlcEvtCallback, this );
    libvlc_event_attach( m_vlc_evt_man, libvlc_MediaPlayerAudioVolume, VlcEvtCallback, this );

    libvlc_audio_set_volume(m_media_player,80);

}

void CVlcPlayer::SetEmbedded()
{
        libvlc_set_fullscreen	(m_media_player, false);

}

void CVlcPlayer::SetFull()
{
        libvlc_set_fullscreen	(m_media_player, true);
}

void CVlcPlayer::Play(wxString location, wxArrayString options)
{
        DBG_INFO("Entering Play");
        m_wasStopped = false;
        if (location == "")
        {
           return;
        }
        m_location = location;
        m_vlcoptions = options;
        //TODO uri disseminate
        libvlc_media_t *media;
        media = libvlc_media_new_location(m_vlc_inst, (const char*)location.utf8_str());
        wxURI uri(location);
        wxString host = uri.GetServer();
        DBG_INFO("Host is %s", (const char*)host.utf8_str());
        bool islocalAddress = (host.Find("127.0.0.1") != wxNOT_FOUND ||
                               host.Find("localhost") != wxNOT_FOUND ||
                               host.Find("::1") != wxNOT_FOUND) ? true : false;

        CProxy *proxy = CProxy::GetConfiguredProxy();
        if (proxy && (!islocalAddress))
        {
           wxString vlcProxyOption;
           if (proxy->GetProxyType() == E_PROXY_HTTP)
           {
              vlcProxyOption = "http-proxy=" + proxy->GetVlcURL();
              DBG_INFO("Setting VLC option: %s", (const char*)vlcProxyOption.utf8_str());
              libvlc_media_add_option(media, (const char*)vlcProxyOption.utf8_str());
              if (proxy->IsProxyAuthUsed())
              {
                 vlcProxyOption = "http-proxy-pwd=" + proxy->GetProxyPassword();
                 DBG_INFO("Setting VLC option: %s", (const char*)vlcProxyOption.utf8_str());
                 libvlc_media_add_option(media, (const char*)vlcProxyOption.utf8_str());
              }
           }
           else if (proxy->GetProxyType() == E_PROXY_SOCKS)
           {
              vlcProxyOption = "socks=" + proxy->GetVlcURL();
              DBG_INFO("Setting VLC option: %s", (const char*)vlcProxyOption.utf8_str());
              libvlc_media_add_option(media, (const char*)vlcProxyOption.utf8_str());
              if (proxy->IsProxyAuthUsed())
              {
                 vlcProxyOption = "socks-user=" + proxy->GetProxyUserName();
                 DBG_INFO("Setting VLC option: %s", (const char*)vlcProxyOption.utf8_str());
                 libvlc_media_add_option(media, (const char*)vlcProxyOption.utf8_str());
                 vlcProxyOption = "socks-pwd=" + proxy->GetProxyPassword();
                 DBG_INFO("Setting VLC option: %s", (const char*)vlcProxyOption.utf8_str());
                 libvlc_media_add_option(media, (const char*)vlcProxyOption.utf8_str());
              }
           }
           delete proxy;
        }

        if (options.Count() != 0)
        {
           for (size_t i = 0; i < options.Count(); i++)
           {
              DBG_INFO("Setting VLC option: %s", (const char*)options[i].utf8_str());
              libvlc_media_add_option(media, (const char*)options[i].utf8_str());
           }
        }
        else
        {
          wxString userAgentOption;
          userAgentOption << "http-user-agent=" << GetUserAgent() ;
          libvlc_media_add_option(media, (const char*)userAgentOption.utf8_str());
        }

        libvlc_video_set_mouse_input(m_media_player, FALSE);
        libvlc_media_player_set_media(m_media_player, media);
        libvlc_media_release(media);
//for playing (rendering)
        {
           wxString renderer = CConfiguration::Instance()->GetVLCConfiguration().GetRender();
           wxString options = ":avcodec-hw=" + renderer;
           libvlc_media_add_option(media,(const char*)options.utf8_str());
        }
//for recording
        if (m_rState == rsFile)
        {
           wxString options = ":sout=#duplicate{dst=display,dst=std{access=file,mux=ts,dst="  + m_recordFileName + "}" ;
           libvlc_media_add_option(media,(const char*)options.utf8_str());
        }
//for transmitting
        else if (m_rState == rsStream)
        {
            wxString streamPort = wxString::Format("%d",m_streamPort);
            wxString options = ":sout=#duplicate{dst=display,dst=std{access=http,mux=ts,dst=:" + streamPort + "}";
            libvlc_media_add_option (media,(const char*)options.utf8_str());
        }
        libvlc_audio_set_volume(m_media_player,m_volume);
        int rv = libvlc_media_parse_with_options(media, libvlc_media_fetch_network , -1);
        DBG_INFO("Parse rv = %d", rv);
        libvlc_media_player_play(m_media_player);



}

void CVlcPlayer::Play()
{
   ResetTracks();
   Play(m_location, m_vlcoptions);
}

void CVlcPlayer::Stop()
{
       m_wasStopped = true;
       libvlc_media_player_pause(m_media_player);
       libvlc_media_player_stop (m_media_player);
       ResetTracks();
}

void CVlcPlayer::VlcEvtCallback(const struct libvlc_event_t *p_event, void *p_data)
{
       wxCommandEvent plevent(playerEVT);
       libvlc_event_t *evdata = new libvlc_event_t;
       *evdata = *p_event;
       plevent.SetClientObject((wxClientData*) new UserDataObject((void *)evdata));
       wxQueueEvent(static_cast<CVlcPlayer *>(p_data)->m_pparent, plevent.Clone());
       if (p_event->type == libvlc_MediaMetaChanged)
       {
          DBG_INFO("Metadata changed");
       }
       if (p_event->type == libvlc_MediaPlayerVout)
       {
//         DBG_INFO("Subtitle track is %d from a count of %d" ,libvlc_video_get_spu(static_cast<CVlcPlayer *>(p_data)->m_media_player),
//                        libvlc_video_get_spu_count(static_cast<CVlcPlayer *>(p_data)->m_media_player));
//         DBG_INFO("Video track is %d from a count of %d" ,libvlc_video_get_track(static_cast<CVlcPlayer *>(p_data)->m_media_player),
//                        libvlc_video_get_track_count(static_cast<CVlcPlayer *>(p_data)->m_media_player));
//         libvlc_video_set_spu(static_cast<CVlcPlayer *>(p_data)->m_media_player, static_cast<CVlcPlayer *>(p_data)->m_trackMap[etSubtitle]);
//         DBG_INFO("Number of video outputs = %d", libvlc_media_player_has_vout(static_cast<CVlcPlayer *>(p_data)->m_media_player));

       }
}

void CVlcPlayer::SetVolume(int value)
{
   m_volume = value;
   libvlc_audio_set_volume(m_media_player,value);
}

int CVlcPlayer::GetVolume()
{
   return m_volume;
}

void CVlcPlayer::SetAspectRatio(wxString aspectRatio)
{
   if (aspectRatio == "")
   {
       libvlc_video_set_aspect_ratio(m_media_player, NULL);
   }
   else
   {
      libvlc_video_set_aspect_ratio(m_media_player, (const char*)aspectRatio.utf8_str());
   }
   m_aspectRatio = aspectRatio;
}


wxString CVlcPlayer::GetAspectRatio()
{
   return m_aspectRatio;
}


bool CVlcPlayer::WasStopped()
{
   return m_wasStopped;
}

void CVlcPlayer::Pause()
{
   libvlc_media_player_pause(m_media_player);
}

void CVlcPlayer::Seek(float position)
{
   libvlc_media_player_set_position(m_media_player, position);
}

float CVlcPlayer::GetPosition()
{
   if (m_media_player)
   {
      return libvlc_media_player_get_position(m_media_player);
   }
   else
   {
      return 0.0;
   }
}

void CVlcPlayer::SetRecordState(EVLCRecordState state)
{
   m_rState = state;
}

EVLCRecordState CVlcPlayer::GetRecordState()
{
   return m_rState;
}

void CVlcPlayer::SetRecordFileName(wxString name)
{
   m_recordFileName = name;
}

wxString CVlcPlayer::GetRecordFileName()
{
   return m_recordFileName;
}

void CVlcPlayer::SetStreamPort(unsigned int port)
{
   m_streamPort = port;
}

#include <wx/arrimpl.cpp>
WX_DEFINE_OBJARRAY(TTrackInfoList);

int CVlcPlayer::GetTracks(TTrackInfoList &tracklist, ETrackType trackType)
{
   libvlc_track_description_t *p_tracks;
   switch (trackType)
   {
   case etVideo:
      p_tracks = libvlc_video_get_track_description(m_media_player);
      break;
   case  etAudio:
      p_tracks = libvlc_audio_get_track_description(m_media_player);
      break;
   case  etSubtitle:
      p_tracks = libvlc_video_get_spu_description(m_media_player);
      break;
    default:
      return -1;
      break;
   }
   libvlc_track_description_t * iter = p_tracks;
   while (iter)
   {
        CTrackInfo crt_track;
        crt_track.SetType(trackType);
        wxString info = iter->psz_name;
        if (info == "")
        {
             info << "Track " << iter->i_id;
        }
        crt_track.SetInfo(info);
        crt_track.SetIndex(iter->i_id);
        tracklist.Add(crt_track);
        iter = iter->p_next;
   }
   for (size_t i = 0;  i < tracklist.Count(); i++)
   {
       DBG_INFO("track %ld, index = %d, type = %d, info = %s", i,
               tracklist[i].GetIndex(),
               tracklist[i].GetType(),
               (const char*)tracklist[i].GetInfo().utf8_str());
   }
   libvlc_track_description_list_release(p_tracks);
   return 0;
}



void  CVlcPlayer::SetTrack(int trackid, ETrackType trackType)
{
   int result;
   if (trackType == etSubtitle)
   {
      DBG_INFO("setting subtitle track, %d", trackid);
      result = libvlc_video_set_spu(m_media_player, trackid);
      DBG_INFO("result = %d", result);
      if(result == 0)
      {
         m_trackMap[etSubtitle] = trackid;
      }
   }
   if (trackType == etAudio)
   {
      DBG_INFO("setting audio track, %d", trackid);
      result = libvlc_audio_set_track(m_media_player,trackid);
      DBG_INFO("result = %d", result);
      if (result == 0)
      {
         m_trackMap[etAudio] = trackid;
      }
   }
   if (trackType == etVideo)
   {
      DBG_INFO("setting video track, %d", trackid);
      result = libvlc_video_set_track(m_media_player,trackid);
      DBG_INFO("result = %d", result);
      if (result == 0)
      {
         m_trackMap[etVideo] = trackid;
      }
   }
}

void CVlcPlayer::RestoreTracks()
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
   int result;
   DBG_INFO("Restoring tracks");
   if (m_trackMap[etSubtitle] != -1)
   {
      DBG_INFO("Subtitle track %d ", m_trackMap[etSubtitle]);
      result = libvlc_video_set_spu(m_media_player, m_trackMap[etSubtitle]);
      DBG_INFO("result = %d", result);
   }
   if (m_trackMap[etAudio] != -1)
   {
      DBG_INFO("Audio track %d", m_trackMap[etAudio]);
      result = libvlc_audio_set_track(m_media_player, m_trackMap[etAudio]);
      DBG_INFO("result = %d", result);
   }
   if (m_trackMap[etVideo] != -1)
   {
      DBG_INFO("Video track, %d", m_trackMap[etVideo]);
      result = libvlc_video_set_track(m_media_player, m_trackMap[etVideo]);
      DBG_INFO("result = %d", result);
   }
#pragma GCC diagnostic pop
}

void CVlcPlayer::ResetTracks()
{
   m_trackMap[etVideo] = -1;
   m_trackMap[etAudio] = -1;
   m_trackMap[etSubtitle] =-1;
}

bool CVlcPlayer::IsValidTime()
{
   return libvlc_media_player_get_time(m_media_player) <= libvlc_media_player_get_length(m_media_player);
}

wxString CVlcPlayer::GetTime()
{
   libvlc_time_t ctime = libvlc_media_player_get_time(m_media_player);
   return FormatTime(ctime);
}

wxString CVlcPlayer::GetLength()
{
   libvlc_time_t ctime = libvlc_media_player_get_length(m_media_player);
   if (ctime == -1 || ctime == 0)
   {
      return wxEmptyString;
   }
   else
   {
      return FormatTime(ctime);
   }
}

wxString CVlcPlayer::FormatTime(libvlc_time_t ctime)
{
   if (ctime == -1)
   {
      return wxEmptyString;
   }
   wxTimeSpan span(0,0,0,ctime);
   return span.Format("%H:%M:%S");
}

wxString CVlcPlayer::GetUserAgent()
{
   wxString userAgent;
   userAgent << "TV-Lite/" << TV_LITE_VERSION_STRING << " (libVLC " << libvlc_get_version() << ")";
   return userAgent;
}

void CVlcPlayer::GetMeta(wxString &sArtist, wxString &sTitle)
{
   if (m_wasStopped) return;
   libvlc_media_t *p_media = libvlc_media_player_get_media(m_media_player);
   DBG_INFO("GetMeta called");
   sArtist = ""; sTitle = "";
   if (p_media != NULL)
   {
      char *title  = libvlc_media_get_meta(p_media, libvlc_meta_Title);
      char *artist = libvlc_media_get_meta(p_media, libvlc_meta_NowPlaying);
      if (title != NULL)
      {
         sTitle = wxString(title, wxConvUTF8);
         DBG_INFO("Title: %s", (const char*)sTitle.utf8_str());
      }
      if (artist != NULL)
      {
         sArtist = wxString(artist, wxConvUTF8);
         DBG_INFO("Artist: %s", (const char*)sArtist.utf8_str());
      }
   }
   else
   {
      DBG_INFO("NULL media found");
   }
}


TTrackMap &CVlcPlayer::GetTrackMap()
{
   return m_trackMap;
}

libvlc_media_player_t *CVlcPlayer::GetMediaPlayer()
{
   return m_media_player;
}

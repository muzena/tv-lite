#ifndef CDLPROGRESS_H
#define CDLPROGRESS_H
#include <wx/frame.h>
#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif
#include "downloadclient.h"
#include "subscription.h"
#include <wx/timer.h>

#define E_DLP_ERROR -1
#define E_DLP_OK     0
#define E_DLP_CANCELED -2
#define E_DLP_NEXT  1

namespace tvlite
{

class CDLProgress : public CDlProgressBase
{
private:
   CDLProgress(const CDLProgress& rhs);
   CDLProgress& operator=(const CDLProgress& rhs);
   TSubscriptionList *m_subscriptionList;
   CSubscription *m_currentSubscription;
   wxTimer m_timer;
   size_t downloadIndex;
   bool m_canClose;
   TChannelList *chanlist;

public:
   CDLProgress() = delete;
   CDLProgress(wxWindow *parent, TSubscriptionList *list = nullptr);
   ~CDLProgress();
   void StartDownload();
	virtual void OnCancelClicked( wxCommandEvent& event );
   virtual void Init( wxInitDialogEvent& event );
   void OnDownloadFinished(wxCommandEvent& event);
   void OnParseFinished(wxCommandEvent& event);
   void OnShow(wxShowEvent& event );
   virtual void OnCloseClicked( wxCloseEvent& event);
   void onTimer(wxTimerEvent &event);
   int NextDownload();
   void PrintSize();

};

}

#endif // CDLPROGRESS_H

#include "downloadclient.h"
#include "debug.h"
#include "proxy.h"
#include <wx/stdpaths.h>
#include <wx/utils.h>
#include <curl/curl.h>
#include <wx/ffile.h>
#include <wx/uri.h>

DEFINE_EVENT_TYPE(DLNOTIFY_EVT)
DEFINE_EVENT_TYPE(DLPROGRESS_EVT)

using namespace tvlite;

TDownloadInfo::TDownloadInfo():offset(0), size(0), have_size(false), size_checked(false)
{
   
}

CDownloadClient::CDownloadClient(wxEvtHandler *handler):
                wxThread(),
                m_curl_handle(NULL),
                m_evtHandler(handler),                
                m_canceled(false),
                m_writeFile(NULL)
                
{
   curl_global_init(CURL_GLOBAL_ALL);
}   

CDownloadClient::~CDownloadClient()
{
  DBG_INFO("Doing cleanup...");
  if (m_curl_handle)
  {
   curl_easy_cleanup(m_curl_handle);
   curl_global_cleanup();
  }
}

int CDownloadClient::StartClient(wxString url, wxString target)
{
   int rc = E_DL_OK;
   if (InitCurl())
   {
      rc =  E_DL_ERROR;
   }
   if (rc == E_DL_OK)  
   {
      if (Download(url, target))
      {
          wxMessageBox(_("Error downloading channel list"),_("Error"), wxOK|wxCENTER|wxICON_ERROR);
          rc =  E_DL_ERROR;
      }   
   }
   return rc;
}


void CDownloadClient::SetEventHandler(wxEvtHandler *evtHandler)
{
   wxMutexLocker lock(m_accessMutex);
   m_evtHandler = evtHandler;
}


int CDownloadClient::InitCurl()
{
   CURL *curl_handle;
   //const char* curl_url = m_url.c_str();
   int rc = E_DL_OK;
#ifdef __WXMSW__   
    wxStandardPaths &pathObj = wxStandardPaths::Get();
    wxString sCrtPath = pathObj.GetDataDir();
    sCrtPath = sCrtPath + "\\curl-ca-bundle.crt";
#endif    
   curl_handle = curl_easy_init();
   
   if (!curl_handle)
   {
      DBG_ERROR("Could not init CURL");
      rc = E_DL_FATAL;
   }
   else
   {   
      curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
      curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);
      curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36");
#ifdef __WXMSW__
      curl_easy_setopt(curl_handle, CURLOPT_CAINFO,(const char*)sCrtPath.c_str());
      DBG_INFO("cert path %s", (const char*)sCrtPath.c_str());
#endif 
      
      m_curl_handle = curl_handle;          
   }
   return rc;
}

int CDownloadClient::Download(wxString url, wxString target)
{
   m_url = url;
   m_target = target;
   wxURI uri(m_url);
   wxString host = uri.GetServer();
   DBG_INFO("Host is %s", (const char*)host.utf8_str());
   bool islocalAddress = (host.Find("127.0.0.1") != wxNOT_FOUND || 
                            host.Find("localhost") != wxNOT_FOUND ||
                            host.Find("::1") != wxNOT_FOUND) ? true : false;
   CProxy *proxy = CProxy::GetConfiguredProxy();
   
   if (proxy && !islocalAddress)
   {
      wxString proxyurl = proxy->GetCurlURL();
      DBG_INFO("Proxy URL is %s", (const char*)proxyurl.utf8_str());
      curl_easy_setopt(m_curl_handle, CURLOPT_PROXY, (const char*)proxyurl.utf8_str());
      delete proxy;
   }
   return Run();
}

wxThread::ExitCode CDownloadClient::Entry()
{
   int rc = E_DL_OK;
   DBG_INFO("Entering Download");
   wxFFile writeFile;
   if (m_curl_handle == NULL)
   {
      DBG_ERROR("CURL Download client not initialized");
      rc = InitCurl();
   }
   if (rc == E_DL_OK)
   {
      
      curl_easy_setopt(m_curl_handle, CURLOPT_URL, (const char *)m_url.c_str());
      DBG_INFO("Downloading from %s", (const char *)m_url.c_str());
      curl_easy_setopt(m_curl_handle, CURLOPT_WRITEFUNCTION, WriteData_cb);
      if (wxFileExists(m_target))
      {
         DBG_INFO("Remove stale target %s", (const char*)m_target.utf8_str());
         if (!wxRemoveFile(m_target))
         {
            DBG_ERROR("Error while removing stale download target: %s", strerror(errno));
            rc = E_FILE_FATAL;
         }
      }      
      writeFile.Open(m_target, "wb");
      if (!writeFile.IsOpened())
      {
         rc = E_FILE_FATAL;
         DBG_ERROR("Error while opening download target: %s", strerror(errno));
      }
   }
   if (rc == E_DL_OK)
   { 
      bool redirect = false;
      char *location; //placeholder
      m_writeFile = &writeFile;
      curl_easy_setopt(m_curl_handle, CURLOPT_WRITEDATA, (void *)this);
      do
      {
         CURLcode res = curl_easy_perform(m_curl_handle);
         if (res != CURLE_OK)
         {
               DBG_ERROR("An error has occured while downloading: %s", curl_easy_strerror(res));
               rc = (int)res;
               break;
         }
         long response_code;
         res = curl_easy_getinfo(m_curl_handle, CURLINFO_RESPONSE_CODE, &response_code);
         if (res != CURLE_OK)
         {
              DBG_ERROR("An error has occured while checking response: %s", curl_easy_strerror(res));
              rc = (int)res;
              break;
         }  
         if (response_code / 100 == 3)
         {
              redirect = true;
              res = curl_easy_getinfo(m_curl_handle, CURLINFO_REDIRECT_URL, &location);
              if (location == NULL)
              {
                 DBG_ERROR("Could not get redirect information: %s", curl_easy_strerror(res));
                 break;
              }
              curl_easy_setopt(m_curl_handle, CURLOPT_URL, location);
              writeFile.Close();
              writeFile.Open(m_target, "wb");
              if (!writeFile.IsOpened())
              {
                 rc = E_FILE_FATAL;
                 DBG_ERROR("Error while opening download target: %s", strerror(errno));
                 break;
              }
              
         }
         else
         {
            redirect = false;
         }
         
         if (response_code / 100 >= 4) 
         {
            rc = E_DL_FATAL;
            DBG_ERROR("Error %ld while downloading", response_code);
            break;
         }
         
         if (GetCanceled())
         {
            rc = E_DL_CANCELED;
            DBG_ERROR("Canceled by user");
            break;
         }
      }
      while (redirect);
   }
   
   writeFile.Close();
   Notify(rc);
  
   return NULL;
}
  
  
int CDownloadClient::WriteData_cb(void *ptr, size_t size, size_t nmemb, void *stream)  
{
   size_t bytecount = size * nmemb;
   curl_off_t dloffset, dlsize;
   CURLcode res;

   CDownloadClient *dlClient =  (CDownloadClient *)stream;
   if (dlClient == NULL)
   {
      return CURLE_CHUNK_FAILED;
   }
   wxFFile *file = dlClient->GetWriteFile();
   if (ptr == NULL)
   {
      return CURLE_CHUNK_FAILED;
   }
   if (dlClient->GetCanceled())
   {
      DBG_INFO("Aborted by user");
      return CURLE_ABORTED_BY_CALLBACK;
   }
   DBG_INFO("Writing %ld bytes", bytecount);
   
   if (!dlClient->GetSizeChecked())
   {
      res = curl_easy_getinfo(dlClient->GetCurlHandle(), CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &dlsize);
      if (res != CURLE_OK)
      {
         dlClient->SetDownloadSize(0);
         dlClient->SetDownloadSizeExists(false);
      }
      else
      {
         dlClient->SetDownloadSize(dlsize);
         dlClient->SetDownloadSizeExists(true);
      }
      dlClient->SetSizeChecked(true);
   }
   res = curl_easy_getinfo(dlClient->GetCurlHandle(), CURLINFO_SIZE_DOWNLOAD_T, &dloffset);
   if (res == CURLE_OK)
   {   
      dlClient->SetDownloadOffset(dloffset); 
      DBG_INFO("Downloaded %" CURL_FORMAT_CURL_OFF_T " from  %" CURL_FORMAT_CURL_OFF_T "\n", dlClient->GetDownloadOffset(), dlClient->GetDownloadSize());
   }
   else
   {
      dlClient->SetDownloadOffset(dlClient->GetDownloadOffset() + bytecount);
      DBG_INFO("%lu bytes written", dlClient->GetDownloadOffset());
   }
   return file->Write(ptr, bytecount);
}

void CDownloadClient::Notify(int result)
{
   wxMutexLocker lock(m_accessMutex);
   SetDownloadSizeExists(false);
   SetDownloadSize(100);
   if (m_evtHandler != NULL)
   {
      DBG_INFO("Notifying handler");
      wxThreadEvent dlNotifyEvent(DLNOTIFY_EVT);
      dlNotifyEvent.SetInt(result);
      wxQueueEvent(m_evtHandler, dlNotifyEvent.Clone());
   }
   else
   {
      DBG_WARNING("Handler has been destroyed. Not notifying");
      //delete file here, else the subscription will deal with it
      wxRemoveFile(m_writeFile->GetName());
   }
}

bool CDownloadClient::GetCanceled()
{
   wxMutexLocker lock(m_accessMutex);
   return m_canceled;
}


void CDownloadClient::SetCanceled(bool canceled)
{
   wxMutexLocker lock(m_accessMutex);
   m_canceled = canceled;
}

wxFFile* CDownloadClient::GetWriteFile()
{
   wxMutexLocker lock(m_accessMutex);
   return m_writeFile;
}

bool CDownloadClient::GetDownloadSizeExists()
{
   return m_dlInfo.have_size;
}

void CDownloadClient::SetDownloadSizeExists(bool exists)
{
   m_dlInfo.have_size = exists;
}


bool CDownloadClient::GetSizeChecked()
{
   return m_dlInfo.size_checked;
}

void CDownloadClient::SetSizeChecked(bool checked)
{
   m_dlInfo.size_checked = checked;
}


curl_off_t CDownloadClient::GetDownloadSize()
{
   return m_dlInfo.size;
}

void CDownloadClient::SetDownloadSize(curl_off_t size)
{
   m_dlInfo.size = size;
}

curl_off_t CDownloadClient::GetDownloadOffset()
{
   return m_dlInfo.offset;
}

void CDownloadClient::SetDownloadOffset(curl_off_t offset)
{
   m_dlInfo.offset = offset;
}

CURL * CDownloadClient::GetCurlHandle()
{
   return m_curl_handle;
}
#ifndef CSOPPROTOCOL_H
#define CSOPPROTOCOL_H

#include "baseprotocol.h"
#include "sopthread.h"

namespace tvlite
{

class CSopThread;

class CSopProtocol: public CBaseProtocol
{
private:
   CSopProtocol(const CSopProtocol& rhs) = delete;
   CSopProtocol& operator=(const CSopProtocol& rhs) = delete;
   CSopProtocol() = delete;
public:
   CSopProtocol(wxEvtHandler *parent, wxString url, wxString cmd = "");
   virtual ~CSopProtocol();

   virtual int  StartProtocol(long &pid);

   virtual int LoadConfig();
   virtual int SaveConfig();

   wxString GetUrl();
   void SetURL(wxString url);

   wxString GetCmd();
   void SetCmd(wxString cmd);

   int GetProtoport();
   void SetProtoPort(uint32_t port);

   int GetPlayerPort();
   void SetPlayerPort(uint32_t port);


private:
   uint32_t m_playerport;
   uint32_t m_protoport;
   long m_protopid;
   int SearchSop(wxString &cmd);

};

}

#endif // CSOPPROTOCOL_H

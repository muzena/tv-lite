#include "downloadclient.h"
#include <wx/filename.h>
#include <wx/dir.h>
#include "debug.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include "subscription.h"
#include "subscriptioninfo.h"
#include "configuration.h"
#include "dlprogress.h"
#include "channel.h"
#include "main.h"
#include "statusgauge.h"
#include "m3ufile.h"

#include <string.h>


#define E_SUB_OK      0
#define E_SUB_ERROR -1

using namespace tvlite;

CSubscription::CSubscription()
{
  m_chanlist   = NULL;
  m_evtHandler = NULL;
}


CSubscription::CSubscription(wxString uri) : m_evtHandler(nullptr), m_uri(uri), m_dlClient(nullptr)
{
   Connect(wxID_ANY, DLNOTIFY_EVT, wxThreadEventHandler(CSubscription::OnReadDataResponse));
}

CSubscription::~CSubscription()
{
   if (m_dlClient)
   {
      DBG_INFO("subscription handler -> still present!");
   }
}


#include <wx/arrimpl.cpp>
WX_DEFINE_OBJARRAY(TChannelList);
WX_DEFINE_OBJARRAY(TPChannelList);

void CSubscription::SetEventHandler(wxEvtHandler* evtHandler)
{
   m_evtHandler = evtHandler;
}

void CSubscription::SetChannelList(TChannelList *chanlist)
{
   m_chanlist = chanlist;
}

CSubscriptionInfo* CSubscription::GetSubscriptionInfo()
{
   return &m_subscriptionInfo;
}

void CSubscription::SetSubscriptionInfo(CSubscriptionInfo subscriptionInfo)
{
   m_subscriptionInfo = subscriptionInfo;
}

wxString CSubscription::GetURI()
{
   return m_uri;
}

void CSubscription::SetURI(wxString uri)
{
   m_uri = uri;
}

CDownloadClient* CSubscription::GetDLClient()
{
   return m_dlClient;
}

int CSubscription::ReadData(TChannelList *channelList)
{
   int rc = E_SUB_OK;
   m_chanlist = channelList;
   wxURI uri;
   DBG_INFO("[DEBUG] m_uri is %s", (const char*)m_uri.utf8_str());
   if (!uri.Create(m_uri))
   {
      DBG_ERROR("Invalid database URL")
      rc = E_SUB_ERROR;
   }
   if (rc == E_SUB_OK)
   {
      wxString scheme = uri.GetScheme();
      if (scheme == "http" || scheme == "https")
      {
         wxString uuidStr = GenUUID();
         m_importFile = wxFileName(CConfiguration::GetTempDir(), uuidStr).GetFullPath();
         m_dlClient = new CDownloadClient(this);
         if (m_dlClient)
         {
              if (rc == E_DL_OK)
              {
                 rc = m_dlClient->StartClient(m_uri, m_importFile);
              }
         }
         else
         {
            DBG_ERROR("Could not init dlclient!");
            rc = E_SUB_ERROR;
         }
      }
#ifdef __WXMSW__
      else if (scheme == "file" || scheme == "" || scheme.Length() == 1)
#else
      else if (scheme == "file" || scheme == "")
#endif
      {
         //Directly read and cache data
         if (m_evtHandler)
         {
            wxCommandEvent dtreadNotifyEvent(DTREADNOTIFY_EVT);
            dtreadNotifyEvent.SetInt(rc);
            wxQueueEvent(m_evtHandler, dtreadNotifyEvent.Clone());
         }
         wxString unescapedURI = uri.BuildUnescapedURI();
         rc = CacheNewData(m_chanlist, unescapedURI);
         if (m_evtHandler)
         {
            wxCommandEvent dtreadNotifyEvent(DTPARSEDNOTIFY_EVT);
            dtreadNotifyEvent.SetInt(rc);
            wxQueueEvent(m_evtHandler, dtreadNotifyEvent.Clone());
         }

      }
   }
   return rc;
}

void CSubscription::OnReadDataResponse (wxThreadEvent &event)
{
   int rc = E_SUB_OK;
   m_dlClient = NULL;
     DBG_INFO("CSubscription: Got response from dlclient");
   if (m_evtHandler)
   {
      wxCommandEvent dtreadNotifyEvent(DTREADNOTIFY_EVT);
      dtreadNotifyEvent.SetInt(rc);
      wxQueueEvent(m_evtHandler, dtreadNotifyEvent.Clone());
   }
   if (event.GetInt() == 0)
   {
      DBG_ERROR("CSubscription: Download OK");
      rc = CacheNewData(m_chanlist, m_importFile);
   }
   else
   {
       DBG_ERROR("CSubscription: Error from download client");
       rc = E_SUB_ERROR;
   }
   wxRemoveFile(m_importFile);
   //signal towards DLProgress window
   if (m_evtHandler)
   {
      wxCommandEvent dtreadNotifyEvent(DTPARSEDNOTIFY_EVT);
      dtreadNotifyEvent.SetInt(rc);
      wxQueueEvent(m_evtHandler, dtreadNotifyEvent.Clone());
   }
   //Disconnect(DLNOTIFY_EVT, wxThreadEventHandler(CSubscription::OnReadDataResponse));

}

int CSubscription::CacheNewData(TChannelList *channelList, wxString importfile)
{
   int rc = E_SUB_OK;
   wxString newCachedFile;
   channelList->Clear();
   rc = ReadDataFromFile(channelList, importfile );
   if (rc == E_SUB_OK)
   {
      m_cachedFile = FindCachedData(m_uri);
      DBG_INFO("[DEBUG] Cached file is %s ", (const char*)m_cachedFile.utf8_str());
      rc = SaveDataToCache(newCachedFile); //newCachedFile is given as reference here
   }
   if (rc == E_SUB_OK)
   {
      if (m_cachedFile != "")
      {
          DBG_INFO("[DEBUG]Removing old cached file")
          wxRemoveFile(m_cachedFile);
          m_cachedFile = newCachedFile;
      }
   }
   return rc;
}


int CSubscription::ReadDataFromCache(TChannelList *channelList)
{
   wxString readuri;
   wxString importfile;
   int rc = E_SUB_OK;
   wxBeginBusyCursor();
   DBG_INFO("[DEBUG] m_uri is %s", (const char*)m_uri.utf8_str());
   readuri = FindCachedData(m_subscriptionInfo.url);
   DBG_INFO("Reading from cache");
   channelList->Clear();
   if (readuri == "")
   {
      DBG_ERROR("Data file not found in cache!");
      rc =  E_DB_CREATE;
   }

   if (rc == E_SUB_OK)
   {
       rc = ReadDataFromFile(channelList, readuri);
   }
   else
   {
      rc = E_DB_CREATE;
   }
   wxEndBusyCursor();
   return rc;
}

int CSubscription::ReadDataFromFile(TChannelList *channelList, wxString importfile)
{
      DBG_INFO("Parse from %s",(const char*)importfile.utf8_str());
      channelList->Clear();
      wxString announceParse = _("Parsing playlist");
      CStatusGauge::ShowStatusGauge(E_SB_LISTPROGRESS, announceParse);
      int rc = ParseData(channelList, importfile);
      CStatusGauge::HideStatusGauge(E_SB_LISTPROGRESS);
      return rc;
}

bool CSubscription::TestM3U(const char* testbuffer)
{
  if (testbuffer)
  {
     wxString  testString(testbuffer);
     return  (testString.Trim(false).Left(4).MakeUpper() == "#EXT");
  }
  return false;

}

int CSubscription::ParseData(TChannelList *channelList, wxString dbFile)
{
   const unsigned int someLength = 1024;
   const unsigned int minLength  = 16;
   wxFileInputStream* stream = new wxFileInputStream(dbFile);
   if (stream == NULL || !stream->IsOk())
   {
      DBG_ERROR("File could not be opened");
      return -1;
   }
   unsigned char buffer[someLength];
   memset (buffer, 0, someLength);
   wxInputStream &current = stream->Read((void*)buffer,someLength - 1);
   size_t readBytes = current.LastRead();
   if (readBytes < minLength)
   {
      DBG_ERROR("File too small!")
      delete stream;
      return -1;
   }
   delete stream;

   if (!strncmp((const char*)buffer,"SQLite", strlen("SQLite")))
   {
      return ParseDbData(channelList, dbFile);
   }
   else if (TestM3U((const char *)buffer))
   {
      if (ParseM3UData(channelList, dbFile, wxConvUTF8))
      {
         wxMBConv *conv = new wxConvAuto();
         if (ParseM3UData(channelList, dbFile, *conv))
         {
            wxMessageBox(_("Malformed M3U list!"), _("Error"), wxOK|wxCENTER|wxICON_ERROR);
            return -1;
         }
         delete conv;
      }
   }
   else
   {
      wxMessageBox(_("Format not recognized"), _("Error"), wxOK|wxCENTER|wxICON_ERROR);
      DBG_ERROR("Format not recognized");
      return -1;
   }
   return 0;
}


int CSubscription::GetDBInfo(wxString dbFile, CSubscriptionInfo* rh)
{
   DBG_INFO("Getting info from db:  %s", (const char*) dbFile.utf8_str());
   CDataBase db(dbFile);
   int rc;
   rc = db.Init();
   if (rc == E_DB_OK)
   {
      rc = db.GetInfoTableData(rh);
   }
   return rc;
}

int CSubscription::ParseDbData(TChannelList *channelList, wxString dbFile)
{
   DBG_INFO("Parsing db data");
   CDataBase db(dbFile);
   channelList->Clear();
   int rc;
   rc = db.Init();
   wxString currentName = m_subscriptionInfo.name;
   if (rc == E_DB_OK)
   {
      CSubscriptionInfo rh;
      rc = db.GetInfoTableData(&rh);
      m_subscriptionInfo = rh;
      DBG_INFO("[DEBUG] Parse data name is %s ", (const char*)m_subscriptionInfo.name.utf8_str());
      DBG_INFO("[DEBUG] Parse data url is %s ", (const char*)m_subscriptionInfo.url.utf8_str());

      if (currentName != "")
      {
         m_subscriptionInfo.name = currentName;

      }
   }
   if (rc == E_DB_OK)
   {
      rc = db.GetTVTableData(channelList);
   }
   return 0;
}

int CSubscription::ParseM3UData(TChannelList *channelList, wxString dbFile, const wxMBConv &conv)
{
   int result = E_DB_OK;
   DBG_INFO("Parsing m3u data");
   wxFileName dbFileName(dbFile);
   channelList->Clear();

   //wxConvAuto::SetFallbackEncoding(wxFONTENCODING_SYSTEM);
   CM3UFile stream (dbFile, " \t", conv);
   if (!stream.Init())
   {
       wxMessageBox(_("Could not open playlist"), _("Error"), wxOK | wxICON_ERROR, NULL);
       result = E_DB_CREATE;
   }
   if (result == E_DB_OK)
   {
      wxFileOffset totallength = stream.Length();

      CChannel *item = nullptr;
      wxString rest;
      int count = 0;
      wxArrayString sVLCOpt;
      wxArrayString sURLs;
      wxString line;
      while (stream.ReadLine(line))
      {
         //DBG_INFO("%s", (const char*)line.utf8_str());
         line.Trim(false);
         line.Trim();
         if (line == "")
         {
            continue;
         }
         if (line[0] != '#')
         {

            sURLs.Add(line);
            if (!item)
            {
               item = new CChannel;
               item->SetName("No Name");
               item->SetId("NoId");
            }
            item->SetStreamURLs(sURLs);
            item->PopulateDefaultValues();
            if (sVLCOpt.Count() != 0)
            {
               CStringToArrayStringHashMap m;
               m[line] = sVLCOpt;
               item->SetVLCOptions(m);
            }
            channelList->Add(*item);
            sURLs.Clear();
            if (count++ > 1000)
            {
               count = 0;
               float percent = (float)stream.Tell()  / (float)totallength * 100.0;
               CStatusGauge::UpdateGauge(E_SB_LISTPROGRESS, (int)percent);
               wxYield();

            }
         }
         else if (line.StartsWith("#EXTINF:", &rest))
         {
            if (item != nullptr)
            {
               delete item;
            }
            sVLCOpt.Clear();
            item = new CChannel;
            sURLs.Clear();
            wxString name, group;
            if (ParseM3ULine(rest, name, group))
            {
               name.Trim(false);
               item->SetName(name);
               group.Trim(false);
               item->SetGroup(group);
               wxString id = name.BeforeFirst(wxUniChar(' '));
               wxString randVal;
               randVal.Printf("%3d", rand() % 1000);
               item->SetId(id + randVal);
            }
         }
         else if (line.StartsWith("#EXTVLCOPT:", &rest))
         {
            sVLCOpt.Add(rest);
         }

      }
      if (item != nullptr)
      {
         delete item;
      }
   }

   return 0;
}

bool CSubscription::ParseM3ULine(wxString &line, wxString &name, wxString &group)
{
   bool rc = true;
   group = "";
   name = line.AfterLast(wxUniChar(','));
   if (name == line)
   {
       DBG_ERROR("Parsing error! Name not found");
       rc = false;
   }
   if (rc)
   {
      int pos1 = line.Find("group-title");
      if (pos1 != wxNOT_FOUND)
      {
         wxString lgroup = line.Mid(pos1);
         wxString rgroup = lgroup.AfterFirst(wxUniChar('"'));
         if (rgroup == lgroup)
         {
            DBG_ERROR("Parsing error! Group begin not found");
         }
         else
         {
            group = rgroup.BeforeFirst(wxUniChar('"'));
            if (group == rgroup)
            {
               DBG_ERROR("Parsing error! Group end not found");
               group = "";
            }
         }
      }

   }
   return rc;
}


wxString CSubscription::GenUUID()
{
#ifdef __WXGTK__
    uuid_t guid;
    uuid_string_t uuidOut;
    uuid_generate(guid);
    uuid_unparse(guid, (char *)uuidOut);
    wxString uuidStr(uuidOut);
    wxString dash("-");
    wxString none;
    uuidStr.Replace(dash,none);
    return uuidStr;
#else
    Uuid *u = new Uuid();
    u->Create();
    UUID* s_uuid = (UUID*)u;
    wxString uuidStr; // In string form
    uuidStr.Printf( wxT("%08lX%04X%04X%02X%02X%02X%02X%02X%02X%02X%02X"),s_uuid->Data1, s_uuid->Data2, s_uuid->Data3,s_uuid->Data4[0], s_uuid->Data4[1], s_uuid->Data4[2], s_uuid->Data4[3], s_uuid->Data4[4], s_uuid->Data4[5], s_uuid->Data4[6],s_uuid->Data4[7]);
    delete u;
    return uuidStr;
#endif

}
int  CSubscription::SaveDataToCache()
{
   wxString path;
   return SaveDataToCache(path);
}

int  CSubscription::SaveDataToCache(wxString &idStr)
{
   int rc = E_SUB_OK;
   wxString uuidStr = GenUUID();
   wxString path = wxFileName(CConfiguration::GetCacheDir(), uuidStr).GetFullPath();
   rc = SaveData(path);
   if (rc == E_SUB_OK)
   {
      idStr = path;
   }
   return rc;
}

int  CSubscription::SaveDataToListDir()
{
   wxString path;
   return SaveDataToListDir(path);
}

int  CSubscription::SaveDataToListDir(wxString &idStr)
{
   int rc = E_SUB_OK;
   wxString uuidStr = GenUUID();
   wxString path = wxFileName(CConfiguration::GetListDir(), uuidStr).GetFullPath();
   rc = SaveData(path, true);
   if (rc == E_SUB_OK)
   {
      idStr = path;
   }
   return rc;
}


int  CSubscription::SaveData(wxString path, bool keepuri /* = false */)
{
    int rc = E_SUB_OK;
    CDataBase subDb(path);
    DBG_INFO("[DEBUG] m_uri is %s", (const char*)m_uri.utf8_str());

    if (subDb.Init() != E_DB_OK)
    {
       rc = E_SUB_ERROR;
       DBG_ERROR("Could not initialize database for cached subscription");
    }
    if (rc == E_SUB_OK)
    {
       //Update subscription info to the correct uri
       CStatusGauge::ShowStatusGauge(E_SB_LISTPROGRESS,  _("Saving..."), false);
       if (!keepuri)
       {
         m_subscriptionInfo.url = m_uri;
       }
       if (m_subscriptionInfo.name == "")
       {
          m_subscriptionInfo.name = "No Name";
       }
       DBG_INFO("[DEBUG] m_subscriptionInfo.url is %s", (const char*)m_subscriptionInfo.url.utf8_str());
       rc = subDb.SetInfoTableData(m_subscriptionInfo);
    }
    if (rc == E_DB_OK)
    {
       if (m_chanlist != NULL)
       {
         rc = subDb.SetTVTableData(*m_chanlist);
       }
    }
    CStatusGauge::HideStatusGauge(E_SB_LISTPROGRESS);
    return rc;
}

wxString CSubscription::GenerateTempFileName()
{
   wxString path = wxFileName(CConfiguration::GetTempDir(), "_dltemp_").GetFullPath();
   DBG_INFO("Temporary file path is %s", (const char*)path.utf8_str());
   return path;
}


void CSubscription::GetCachedData(TSubscriptionList &subList)
{
   wxArrayString cachedFilesArray;
   for (size_t i = 0; i < subList.Count(); i++)
   {
      if (subList[i] != nullptr)
      {
         delete subList[i];
         subList[i] = nullptr;
      }
   }
   subList.Clear();
   size_t numCachedFiles = wxDir::GetAllFiles(CConfiguration::GetCacheDir(), &cachedFilesArray, wxEmptyString, wxDIR_FILES);
   for (size_t index = 0; index < numCachedFiles; index++)
   {
      CSubscription *subscription = new CSubscription("file://" + cachedFilesArray[index]);
      subscription->GetDBInfo(cachedFilesArray[index], subscription->GetSubscriptionInfo());
      subscription->SetURI(subscription->GetSubscriptionInfo()->url);
      subList.Add(subscription);
   }
   subList.Sort(Compare);
   cachedFilesArray.Clear();
}


wxString CSubscription::FindCachedData(wxString url)
{
   wxArrayString cachedFilesArray;
   wxString rcString;
   CSubscriptionInfo info;
   DBG_INFO("[DEBUG] url is %s", (const char*)url.utf8_str());
   size_t numCachedFiles = wxDir::GetAllFiles(CConfiguration::GetCacheDir(), &cachedFilesArray, wxEmptyString, wxDIR_FILES);
   for (size_t index = 0; index < numCachedFiles; index++)
   {

      if (GetDBInfo(cachedFilesArray[index], &info) == E_DB_OK)
      {
         if (info.url == url)
         {
            rcString = cachedFilesArray[index];
            break;
         }
      }
   }
   cachedFilesArray.Clear();
   return rcString;
}

unsigned int CSubscription::GetNumberOfChannels()
{
   return m_chanlist->GetCount();
}

unsigned int CSubscription::GetNumberOfSources()
{
   unsigned int sources = 0;
   for (size_t i = 0; i < m_chanlist->GetCount(); i++)
   {
      wxArrayString s = m_chanlist->Item(i).GetStreamURLs();
      sources += s.GetCount();
      s.Clear();
   }
   return sources;
}

int CSubscription::Compare( CSubscription **item1, CSubscription **item2)
{
  wxString s1 = (*item1)->GetSubscriptionInfo()->name;
  wxString s2 = (*item2)->GetSubscriptionInfo()->name;

  return strcmp((const char*)s1.utf8_str(), (const char*)s2.utf8_str());
}


#include "sopprotocolhandler.h"
#include "debug.h"
#include "main.h"

#define E_ERROR -1
#define E_OK 0

DEFINE_EVENT_TYPE(vlcEVT_PLAY)
DEFINE_EVENT_TYPE(thrEVT_EXIT)

using namespace tvlite;

CSopProtocolHandler::CSopProtocolHandler(wxEvtHandler *parent, wxString url, wxString name, wxArrayString vlcoptions) :
  CBaseProtocolHandler(parent, url, name, vlcoptions),
  m_process(NULL),
  m_sopthread(NULL),
  m_protopid(0),
  m_timer(this, wxID_ANY)
{

   Connect(wxID_ANY, vlcEVT_PLAY, wxCommandEventHandler(CSopProtocolHandler::PlayVLC));
   Connect(wxID_ANY, thrEVT_EXIT, wxCommandEventHandler(CSopProtocolHandler::OnExitSOPThread));
   Connect(wxID_ANY, wxEVT_END_PROCESS, wxProcessEventHandler(CSopProtocolHandler::OnExitSOPProcess));
   Connect(wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(CSopProtocolHandler::OnTimerNotify));
}


CSopProtocolHandler::~CSopProtocolHandler()
{
   DBG_INFO("SOP Protocol Handler -> delete")
   if (m_process)
   {
      delete (m_process);
      m_process = NULL;
   }
}

void CSopProtocolHandler::Start()
{
   ((MainFrame*)wxGetApp().GetMainFrame())->SetStatusText(_("Starting Sopcast"), 0);
   LaunchSopProcess();
}

void CSopProtocolHandler::Stop()
{
   m_vlcPlayer->Stop();
   m_stopped = true;
   if (m_sopthread && !m_sopthread->GetRunning())
   {
      DBG_ERROR("Sop thread not running. Bail out");
      OnStopAsync();
   }
   else if (m_process)
   {
      m_process->StopProtocol();
   }
   else
   {
      OnStopAsync();
   }
}

int CSopProtocolHandler::LaunchSopProcess()
{
   int result = E_OK;
   m_process = new CSopProtocol(this, m_url);
   if (!m_process)
   {
      DBG_ERROR("Could not create sop protocol")
      result = E_ERROR;
   }
   if (result == E_OK)
   {
      result = m_process->StartProtocol(m_protopid);
   }
   else
   {
      DBG_ERROR("Could not start sopcast");
   }
   if (result == E_OK)
   {
      m_sopthread = new CSopThread(this, m_process);
      if (m_sopthread == NULL)
      {
          DBG_ERROR("Could not allocate thread");
          result = E_ERROR;
      }
   }
   if (result == E_OK)
   {
      if ( m_sopthread->Create() != wxTHREAD_NO_ERROR )
      {
          DBG_ERROR("Could not create thread");
          delete m_sopthread;
          m_sopthread = NULL;
          result = E_ERROR;
      }

   }
   if (result == E_OK)
   {
      if (m_sopthread->Run() != wxTHREAD_NO_ERROR)
      {
         DBG_ERROR("Could not run thread");
         result = E_ERROR;
      }

   }
   return result;
}

void CSopProtocolHandler::PlayVLC(wxCommandEvent &event)
{
   wxString addr;
   DBG_INFO("[Protocol handler]: Play VLC");
   addr << wxT("http://127.0.0.1:") << m_process->GetPlayerPort();
   m_vlcPlayer->Play(addr, m_vlcoptions);
#ifdef __WXMSW__
   //((MainFrame*)wxGetApp().GetTopWindow())->RaiseTransp();
#endif
}

void CSopProtocolHandler::OnExitSOPThread(wxCommandEvent &event)
{
   DBG_ERROR("Notified about thread exit. Waiting...");

   if (m_sopthread != NULL)
   {
         m_sopthread->Wait();
         delete m_sopthread;
         DBG_INFO("Finished wait!");
         m_sopthread = NULL;
         m_timer.Start(2);
   }
   else
   {
      DBG_INFO("Thread was already destroyed. Stopping");
      OnStopAsync();
   }
}

void CSopProtocolHandler::OnExitSOPProcess(wxProcessEvent &event)
{
   if ((long)event.GetPid() != m_protopid)
   {
      event.Skip();
      return;
   }

   DBG_ERROR("Sopcast process has exit.");
   if (m_sopthread != NULL)
   {
         DBG_INFO("Waiting for thread");
         m_sopthread->Wait();
         delete m_sopthread;
         DBG_INFO("Finished wait");
         m_sopthread = NULL;
   }
   else
   {
      DBG_INFO("Thread was already destroyed. Stopping");
      if (m_timer.IsRunning())
      {
         m_timer.Stop();
         OnStopAsync();
      }
   }
}

void CSopProtocolHandler::OnTimerNotify(wxTimerEvent &event)
{
   DBG_INFO("Timer passed: Stopping");
   OnStopAsync();
}
